#version 330

uniform mat4 T; // Translation matrix
uniform mat4 S; // Scale matrix
uniform mat4 R; // Rotation matrix
uniform mat4 camera; // camera matrix
uniform vec4 posRelParent; // the position relative to the parent
uniform vec4 clipPlane; //The clipping Plane
uniform int usePlane; //PLane bool

// Input vertex packet
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 textureCoord;


// Output vertex packet
out packet {
	vec2 textureCoord;
} outputVertex;

mat4 transform;
vec4 pos;


void main(void) {
	//Work out the transform matrix
	transform = T * R * S;

	pos = vec4(position.x, position.y, 0.0f, 1.0) + posRelParent;

	pos = transform * pos;


	if (usePlane == 1)
	{
		gl_ClipDistance[0] = dot(pos, clipPlane);
	}
	else
	{
		gl_ClipDistance[0] = 1;
	}
	pos = camera * pos;

	outputVertex.textureCoord = textureCoord;


	//Work out the final pos of the vertex
	gl_Position = pos;
}