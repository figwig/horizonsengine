#version 330

// Input vertex packet
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoords;


// Output vertex packet
out packet{
	vec2 textureCoord;
} outputVertex;



void main(void) {

	outputVertex.textureCoord = texCoords;
	// Setup local variable pos in case we want to modify it (since position is constant)
	vec4 pos = vec4(position.x, position.y, 0.0, 1.0);

	// Apply transformation to pos and store result in gl_Position
	gl_Position = pos;
}