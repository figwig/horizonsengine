#version 330
uniform sampler2D textureImage;
uniform vec4 color;
uniform float colStrength;
uniform int noTex;
// input packet

in packet{
	vec2 textureCoord;
} inputFragment;



// output packet
layout(location = 0) out vec4 fragmentColour;

float when_eq(float x, float y) {
	return 1.0 - abs(sign(x - y));
}
vec4 when_eq(vec4 x, vec4 y) {
	return 1.0 - abs(sign(x - y));
}
vec3 when_eq(vec3 x, vec3 y) {
	return 1.0 - abs(sign(x - y));
}


void main(void) {

	vec3 gamma = vec3(1.0 / 2.2);
	vec4 readInColor = texture2D(textureImage, inputFragment.textureCoord);
	vec4 tempFragmentColour = readInColor;

	float texA = tempFragmentColour.a;
	float tintA = color.a;

	tempFragmentColour = mix(tempFragmentColour, color, colStrength);

	tempFragmentColour.a = min(texA, tintA);

	tempFragmentColour.a += tintA  * when_eq(noTex, 1);

	fragmentColour = vec4(pow(tempFragmentColour.xyz, gamma), tempFragmentColour.a);
}