#include "LoadScreen.h"
#include <ImageRect.h>
#include <MenuController.h>
#include <Text.h>
#include <texture_loader.h>
#include <smallSpotLight.h>
#include <Scenery.h>
#include <SimpleCamera.h>
#include <Waterplane.h>
#include <Skybox.h>
#include <FPSCounter.h>
#include <MoonLight.h>
#include <BrightnessPP.h>
#include <Rotator.h>
#include <Button.h>
#include <ContrastPP.h>
#include <GLRenderer.h>
#include <HorizonsUI.h>
#include <AssimpContainer.h>
#include <ModelManager.h>

LoadScreen::LoadScreen()
{
	name = "Menu Choice";

}


LoadScreen::~LoadScreen()
{

}

void LoadScreen::run()
{
	Fade * f = new Fade(FADE_FROM_BLACK);
	addPublicBehaviour(f);
	f->startFade(1000);
	gameRefr->setCaptureMouse(false);
}

void LoadScreen::load(Game * thisGame)
{
	gameRefr = thisGame;

	//gameRefr->setRenderEngine(GLRenderer::singleton());
	
	loadInit();

	loadGUI();

	loadSphere();

	loadCamera();

	debugSettings->lightSphere = false;

	smallSpotLight * spot = new smallSpotLight(glm::vec3(0,0, 3.5), glm::vec3(0, 2, 10));
	smallSpotLight * spot2 = new smallSpotLight(glm::vec3(0,0, 3.5), glm::vec3(0, 2, -10));
	smallSpotLight * spot3 = new smallSpotLight(glm::vec3(0,0, 3.5), glm::vec3(10, 2, -10));

	spot->coneDirection = glm::vec3(0, -0.2, -1.0f);
	spot2->coneDirection = glm::vec3(0, -0.2, 1.0f);
	spot3->coneDirection = glm::vec3(0.3, -0.2, 1.0f);

	currLighting->addLight(spot);
	currLighting->addLight(spot2);
	currLighting->addLight(spot3);


	Moonlight * moonLight = new Moonlight(glm::vec3(0, 0.8f, 0.8f));
	currLighting->addLight(moonLight);

	Skybox * sky = new Skybox(1);

	sky->name = "Skybox";

	sky->setPos(glm::vec3(0,-200, 0));

	addGameObject(sky);

	gameRefr->setConfigAll();
}


void LoadScreen::loadGUI()
{
	TextObject* message = new TextObject(Game::singleton()->getConfig().codeFontCharacters);

	message->centred = true;

	message->setText("Welcome to Horizons Engine Demo");

	message->setPos(glm::vec3(0, 250, 0));

	message->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	addGUIObject(message);

	message->setScale(glm::vec3(0.5f));

	menuController = new MenuController(gameRefr);

	addPublicBehaviour(menuController);

	TextureExaminer* texExam = TextureExaminer::singleton();

	texExam->setDirectory("");

	Texture *t = texExam->getTexture("Textures\\Button.png");

	SceneOnButtonAction* waterAction = new SceneOnButtonAction("Water");
	SceneOnButtonAction* terrainAction = new SceneOnButtonAction("Terrain");
	SceneOnButtonAction* nightAction = new SceneOnButtonAction("Demo Scene");
	SceneOnButtonAction* physicsAction = new SceneOnButtonAction("Physics");

	Button* WaterScene = new Button(t, glm::vec3(0.42f), waterAction, "WaterButton");
	TextObject* waterText = new TextObject(gameRefr->getConfig().codeFontCharacters);
	

	Button* TerrainScene = new Button(t, glm::vec3(0.42f), terrainAction, "TerrainButton");
	TextObject* terrainText = new TextObject(gameRefr->getConfig().codeFontCharacters);

	Button* NightScene = new Button(t, glm::vec3(0.42f), nightAction, "NightButton");
	TextObject* nightText = new TextObject(gameRefr->getConfig().codeFontCharacters);

	Button* PhysicsScene = new Button(t, glm::vec3(0.42f), physicsAction, "Physics Button");
	TextObject* physicsText = new TextObject(gameRefr->getConfig().codeFontCharacters);


	WaterScene->setPos(glm::vec3(0, 105, 2));
	waterText->setPos(glm::vec3(0, 0, 3));

	TerrainScene->setPos(glm::vec3(0, 35, 2));
	terrainText->setPos(glm::vec3(0,0,3));

	NightScene->setPos(glm::vec3(0, -35, 2));
	nightText->setPos(glm::vec3(0,0,3));

	PhysicsScene->setPos(glm::vec3(0, -105, 2));
	physicsText->setPos(glm::vec3(0,0,3));

	WaterScene->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	waterText->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	TerrainScene->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	terrainText->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	NightScene->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	nightText->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	PhysicsScene->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	physicsText->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	waterText->setParent(WaterScene);
	waterText->centred = true;
	waterText->setColour(glm::vec3(0));

	nightText->setParent(NightScene);
	nightText->centred = true;
	nightText->setColour(glm::vec3(0));

	terrainText->setParent(TerrainScene);
	terrainText->centred = true;
	terrainText->setColour(glm::vec3(0));

	physicsText->setParent(PhysicsScene);
	physicsText->centred = true;
	physicsText->setColour(glm::vec3(0));

	Panel* panel = new Panel(glm::vec2(300, 300));

	panel->getComponent<RenderConditions>()->tintColor = glm::vec4(0.1f, 0.1f, 0.15f, 0.4f);
	panel->getComponent<RenderConditions>()->tintStrength = 1.0f;
	panel->transform->anchorType = GUI_TRANSFORM_ANCHOR_TL;

	panel->transform->setPos(glm::vec3(0, 180, 0));

	WaterScene->setParent(panel);
	NightScene->setParent(panel);
	TerrainScene->setParent(panel);
	PhysicsScene->setParent(panel);

	physicsText->setText("Physics Demo Scene");
	terrainText->setText("TerrainScene");
	nightText->setText("NightScene");
	waterText->setText("Water Scene");

  	addGUIObject(panel);

	//AnimationComponent* animator = new AnimationComponent(panel);

	//panel->addComponent(animator);


	addGUIObject(WaterScene);
	addGUIObject(waterText);

	addGUIObject(TerrainScene);
	addGUIObject(terrainText);

	addGUIObject(NightScene);
	addGUIObject(nightText);

	addGUIObject(PhysicsScene);
	addGUIObject(physicsText);
}

void LoadScreen::loadSphere()
{
	Model* sphere = ModelManager::getModel("Objects/HiResSphere.obj");

	Custom* sphereGO = new Custom(sphere, glm::vec3(0, 2, 0), glm::vec3(0.05f), glm::vec3(0, -1.57f ,0));

	sphereGO->name = "That Sphere";

	addGameObject(sphereGO);

	sphereGO->addPrivateBehaviour(new yRotator(sphereGO, 0.0002f));

	GLuint waterDudv = fiLoadTextureNormal("Textures/waterDUDV.png");
	GLuint waterNormal = fiLoadTextureNormal("Textures/waterNormalMap.png");

	WaterPlane * water = new WaterPlane(glm::vec2(40,80));

	water->name = "Water";

	water->setPos(glm::vec3(0, 0, 0));

	addReflectiveGameObject(water);

	water->setDudv(waterDudv);

	water->setNormal(waterNormal);
}

void LoadScreen::loadCamera()
{
	RotatingCamera * camera = new RotatingCamera();

	gameRefr->setCamera(camera);

	//BrightnessPP * bright = new BrightnessPP(1.5f);
	//gameRefr->addPostProccessor(bright);
}

