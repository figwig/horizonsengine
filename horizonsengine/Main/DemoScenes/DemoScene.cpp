#include "DemoScene.h"

#include <ImageRect.h>
#include <MenuController.h>
#include <Text.h>
#include <texture_loader.h>
#include <smallSpotLight.h>
#include <Scenery.h>
#include <SimpleCamera.h>
#include <Waterplane.h>
#include <Skybox.h>
#include <FPSCounter.h>
#include <MoonLight.h>
#include <BrightnessPP.h>
#include <Rotator.h>
#include <Button.h>
#include <ContrastPP.h>
#include <GLRenderer.h>
#include <HorizonsUI.h>
#include <AssimpContainer.h>
#include <ModelManager.h>
#include <ThirdPersonCamera.h>

DemoScene::DemoScene()
{
	name = "Demo Scene";

}


DemoScene::~DemoScene()
{
	delete menuController;
}

void DemoScene::run()
{
	Fade* f = new Fade(FADE_FROM_BLACK);
	addPublicBehaviour(f);
	f->startFade(1000);
	gameRefr->setCaptureMouse(false);
}

void DemoScene::load(Game* thisGame)
{
	gameRefr = thisGame;

	loadInit();

	loadGUI();

	loadPlayer();

	loadCamera();

	debugSettings->lightSphere = false;

	Moonlight* moonLight = new Moonlight(glm::vec3(0, 0.8f, 0.8f));
	currLighting->addLight(moonLight);

	Skybox* sky = new Skybox(0);

	sky->name = "Skybox";

	sky->setPos(glm::vec3(0, -200, 0));

	addGameObject(sky);

	gameRefr->setConfigAll();
}


void DemoScene::loadGUI()
{
	
}

void DemoScene::loadPlayer()
{
	//Model* sphere = ModelManager::getModel("Objects/spaceship.obj");
	Model* sphere = ModelManager::getModel("Objects/Transport Shuttle_obj.obj");
	//Model* sphere = ModelManager::getModel("Objects/cube.obj");
	
	player= new Custom(sphere, glm::vec3(0, 5, 0), glm::vec3(0.45f), glm::vec3(0, 0, 0));

	player->name = "PlayerShip";

	addGameObject(player);



	//Water

	GLuint waterDudv = fiLoadTextureNormal("Textures/waterDUDV.png");
	GLuint waterNormal = fiLoadTextureNormal("Textures/waterNormalMap.png");

	WaterPlane* water = new WaterPlane(glm::vec2(40, 80));

	water->name = "Water";

	water->setPos(glm::vec3(0, 0, 0));

	addReflectiveGameObject(water);

	water->setDudv(waterDudv);

	water->setNormal(waterNormal);
}
#include <FirstPersonCamera.h>
void DemoScene::loadCamera()
{
	ThirdPersonCamera* tpc = new ThirdPersonCamera();


	tpc->setRadius(15.0f);

	tpc->setTarget(player);
	
	gameRefr->setCamera(tpc);



	//BrightnessPP * bright = new BrightnessPP(1.5f);
	//gameRefr->addPostProccessor(bright);
}

