#include "WaterScene.h"
#include <Game.h>
#include <VAOData.h>
#include <Scenery.h>
#include <MoonLight.h>
#include <FirstPersonCamera.h>
#include <ThirdPersonCamera.h>
#include <Skybox.h>
#include <smallSpotLight.h>
#include <PlayerController.h>
#include <Rotator.h>
#include <PointLight.h>
#include <Volume.h>
#include <WaterPlane.h>
#include <ImageRect.h>
#include <FPSCounter.h>
#include <GrassyTerrainTextureData.h>
#include <Terrain.h>
#include <ContrastPP.h>
#include <PrintPositions.h>
#include <ModelManager.h>
#include <ObjectFactory.h>
#include "DeleteOnClick.h"
#include <PhysicsComponent.h>
#include <RebuildShader.h>
#include <random>
#include <ShaderManager.h>

WaterScene::WaterScene()
{
	name = "Water";
}

WaterScene::~WaterScene()
{
	delete currLighting;
}

void WaterScene::run()
{
	gameRefr->captureMouse = true;
	gameRefr->setPlayer(player);
	gameRefr->setPlaying(true);
}

void WaterScene::loadTextures()
{
	shipSpec = fiLoadTexture("Textures/shipSpec.png");

	waterDudv = fiLoadTextureNormal("Textures/waterDUDV.png");
	waterNormal = fiLoadTextureNormal("Textures/waterNormalMap.png");
}

void WaterScene::loadInit()
{
	Scene::loadInit();

	loadTextures();

	GameObject * skyBox = new Skybox(0);

	addGameObject(skyBox);

	RebuildShader* rs = new RebuildShader(DefaultShaders::getInstance());

	gameRefr->addGlobalBehaviour(rs);

	//debugSettings->wireFrame = true;
}

std::vector<Material*> WaterScene::genMaterials()
{
	std::vector<Material*> vec;

	Material* asphaltMat = MaterialHandler::singleton()->makeMaterial("Textures/Asphalt.png", "Textures/AsphaltSpec.png", "Textures/AsphaltNormal.png");

	Material* grassMat1 = MaterialHandler::singleton()->makeMaterial("Textures/grass01.jpg", "Textures/grass01_s.jpg", "Textures/grass01_n.jpg");

	Material* grassMat2 = MaterialHandler::singleton()->makeMaterial("Textures/grass02.jpg", "Textures/grass02_s.jpg", "Textures/grass02_n.jpg");

	Material * bark = MaterialHandler::singleton()->makeMaterial("Textures/bark 012 - LOOP.jpg", "Textures/grass02_s.jpg", "Textures/bark 012 - LOOP NORMAL.jpg");

	vec.push_back(asphaltMat);
	vec.push_back(grassMat1);
	vec.push_back(grassMat2);
	vec.push_back(bark);

	return vec;
}

void WaterScene::loadCubeGrid(int max, bool stressy)
{

	DeleteOnClick* doc = new DeleteOnClick();

	addPublicBehaviour(doc);

	Model* cube = ModelManager::getModel("Objects/cube.obj");

	cubeMaterial = cube->getMeshByIndex(0)->getDefaultMaterial();

	Model* sphere = ModelManager::getModel("Objects/HiResSphere.obj");

	float height = 20.0f;

	Material* sphereMat = sphere->getMeshByIndex(0)->getDefaultMaterial();
	
	int distance = 25;

	std::vector<Material*> materials = genMaterials();

	materials.push_back(sphereMat);
	materials.push_back(cubeMaterial);

	GameObject* rotator = new GameObject();
	rotator->setPos(glm::vec3(0, 0, 0));
	rotator->addPrivateBehaviour(new yRotator(rotator, 0.0001f));


	for (int count = -max; count < max; count++)
	{
		for (int count2 = -max; count2 < max; count2++)
		{
			if (count2 % 2 == 0)
			{
				Custom* cubeInGrid = new Custom(cube, glm::vec3(count * distance, height, count2 * distance), glm::vec3(0.5f), glm::vec3(0));
				cubeInGrid->setup();

				cubeInGrid->name = "Cube In Grid";

				int random = rand() % materials.size();

				cubeInGrid->getComponent<RenderConditions>()->setAllMaterials(materials[random]);

				cubeInGrid->e = cube->getExtents(0);

				if (stressy) {
					xRotator* rotator = new xRotator(cubeInGrid, 0.0003f);
					cubeInGrid->addPrivateBehaviour(rotator);

					btCollisionShape* shape = ObjectFactory::getInstance()->makeBoxCollider(cubeInGrid);

					cubeInGrid->setCollisionShape(shape);

					cubeInGrid->addComponent(new PhysicsComponent(cubeInGrid));
				}

				cubeInGrid->setParent(rotator);

				//loadCubeCircle(cubeInGrid, 8.0f);

				addGameObject(cubeInGrid);
				
			}
			else
			{
				Custom* sphereInGrid = new Custom(sphere, glm::vec3(count * distance, height, count2 * distance), glm::vec3(0.2f), glm::vec3(0));

				int random = rand() % materials.size();

				sphereInGrid->getComponent<RenderConditions>()->setAllMaterials(materials[random]);

				sphereInGrid->e = sphere->getExtents(0);

				if (stressy)
				{
					yRotator* rotator = new yRotator(sphereInGrid, 0.0003f);
					sphereInGrid->addPrivateBehaviour(rotator);

					btCollisionShape* shape = ObjectFactory::getInstance()->makeBoxCollider(sphereInGrid);

					sphereInGrid->setCollisionShape(shape);

					sphereInGrid->addComponent(new PhysicsComponent(sphereInGrid));
				}
				sphereInGrid->setParent(rotator);

				//loadCubeCircle(sphereInGrid, 8.0f);

				addGameObject(sphereInGrid);
			}

		}
	}
}

void WaterScene::loadCubeCircle(GameObject * parent, float radius)
{	
	//Set the file

	//Create a VAO data from this
	Model* cube = ModelManager::getModel("Objects/cube.obj");

	int cubeCount = 8;

	float interval = (2.0f * M_PI) / cubeCount;

	float x, z;
	float height = 5.0f;
	int count = 0;

	for (float angle = 0; angle < 2.0f* M_PI; angle += interval)
	{
		x = radius * cos(angle);
		z = radius * sin(angle);

		Custom  * cubeInCircle = new Custom(cube, glm::vec3(x, height, z), glm::vec3(0.05f), glm::vec3(0));
		cubeInCircle->setup();

		cubeInCircle->getComponent<RenderConditions>()->setAllMaterials(cubeMaterial);

		yRotator * rotator = new yRotator(cubeInCircle, 0.0003f);
		cubeInCircle->addPrivateBehaviour(rotator);

		cubeInCircle->setParent(parent);

		addGameObject(cubeInCircle);

	}
}

void WaterScene::loadPlayer()
{
	//Add a Cube to the scene
	//Set the file
	Model* cube = ModelManager::getModel("Objects/cube.obj");

	Custom *cube1 = new Custom(cube, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.1f), glm::vec3(0.0f, 0.0f, 0.0f));

	addGameObject(cube1);

	player = cube1;

	//Make a player controller
	PlayerController * controller = new PlayerController(gameRefr);

	player->addPrivateBehaviour(controller);
}

void WaterScene::loadCamera()
{
	//Setup the game camera
	Camera * newCamera = new FirstPersonCamera(player);
	newCamera->setTarget(player);
	gameRefr->setCamera(newCamera);
}

void WaterScene::loadWaterPlane()
{
	WaterPlane * water = new WaterPlane(glm::vec2(12));

	water->setPos(glm::vec3(0, -6, 0));
	
	addReflectiveGameObject(water);

	water->setDudv(waterDudv);

	water->setNormal(waterNormal);
}

void WaterScene::loadTree001()
{
	Model* tree001data = ModelManager::getModel("Objects/tree01b.obj");

	Custom * tree001 = new Custom(tree001data, glm::vec3(0, 30, 20), glm::vec3(1), glm::vec3(0));
	addGameObject(tree001);
}

void WaterScene::loadLighting()
{
	//////
	//LIGHTING
	//////
	
	//Add a basic moonlight to the scene
	currLighting->addLight(new Daylight(glm::vec3(-0.5f, 1.0f, -0.7f), false));

	//debugSettings->lightSphere = true;

	static const int lightCount = 8;
	float height = 30.0f;
	float radius = 150.0f;
	float x, z;

	float interval = (2.0f * M_PI) / lightCount;

	for (float angle = 0; angle < 2.0f* M_PI; angle += interval)
	{
		x = radius * cos(angle);
		z = radius * sin(angle);

		smallSpotLight * spotLight = new smallSpotLight(glm::vec3(0, 0, 4), glm::vec3(x, height + 10, z));
		currLighting->addLight(spotLight);
	}

	smallSpotLight * spotLight = new smallSpotLight(glm::vec3(0, 0, 4), glm::vec3(0, 7, 0));
	currLighting->addLight(spotLight);

}

void WaterScene::load(Game* thisGame)
{
	gameRefr = thisGame;

	loadInit();

	loadWaterPlane();

	//loadCubeCircle();

	loadCubeGrid(9, true);

	Model* cube = ModelManager::getModel("Objects/cube.obj");


	Custom* cubeObj = new Custom(cube, glm::vec3(0), glm::vec3(0.1), glm::vec3(0));

	cubeObj->addPrivateBehaviour(new yRotator(cubeObj, 0.001f));


	Custom* cubeObj2 = new Custom(cube, glm::vec3(20), glm::vec3(1), glm::vec3(0));

	Custom* cubeObj3 = new Custom(cube, glm::vec3(20), glm::vec3(0.5f), glm::vec3(0));

	cubeObj2->addPrivateBehaviour(new xRotator(cubeObj2, 0.002f));

	cubeObj2->setParent(cubeObj);
	cubeObj3->setParent(cubeObj2);

	addGameObject(cubeObj);
	addGameObject(cubeObj2);
	addGameObject(cubeObj3);

	loadPlayer();

	loadLighting();

	loadCamera();

	loadWorld();
	
	setupPostProcessing();

	gameRefr->setConfigAll();
}

void WaterScene::loadIndexedCube()
{
	Model* cubeData = ModelManager::getModel("Objects/cubeSimple.obj");

	Custom* cubeIndexed = new Custom(cubeData, glm::vec3(0,70,0), glm::vec3(1), glm::vec3(0));

	addGameObject(cubeIndexed);
}

void WaterScene::loadWorld()
{
	Model* terrain = ModelManager::getModel("Objects/WaterWorld.obj");

	Terrain * terrainObject = new Terrain(terrain , new GrassyTerrainTextureData());
	terrainObject->setScale(glm::vec3(0.035f));
	terrainObject->setTilingFactor(1);
	addGameObject(terrainObject);
}

void WaterScene::setupPostProcessing()
{
	gameRefr->addPostProccessor(new ContrastPP(0.12f));
}