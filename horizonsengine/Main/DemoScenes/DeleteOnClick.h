#pragma once
#include <PublicBehaviour.h>
#include <MouseRaycaster.h>
#include <InputListener.h>

class DeleteOnClick : public PublicBehaviour, public InputListener
{
public:
	DeleteOnClick()
	{
		contained = false;
		mrc = new MouseRaycaster();
	}
	void OnClick(int key, float x, float y) 
	{
		GameObject* gm = mrc->getObjectUnderMouse();

		if (gm != nullptr)
		{
			Game::singleton()->currentScene()->removeGameObject(gm);
		}
	}

	void update(double)
	{

	}

private:
	MouseRaycaster* mrc = nullptr;

};

