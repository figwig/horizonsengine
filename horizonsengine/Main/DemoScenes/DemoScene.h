#pragma once
#include <Scene.h>
#include <MenuController.h>
#include <Fade.h>
#include <ImageRect.h>

class Game;

class DemoScene :
	public Scene
{
public:
	DemoScene();
	~DemoScene();

	void run();
	void load(Game* gameRefr);
	void loadTextures() {}
protected:
	void loadPlayer();

	void loadGUI();

	void loadCamera();

	MenuController* menuController;

	Fade* fader;

	GameObject* player = nullptr;
};

