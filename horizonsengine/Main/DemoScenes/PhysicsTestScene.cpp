#include "PhysicsTestScene.h"
#include <Skybox.h>
#include <Game.h>
#include <VAOData.h>
#include <Scenery.h>
#include <FPSCounter.h>
#include <FirstPersonCamera.h>
#include <MoonLight.h>
#include <PlayerController.h>
#include <GenerateCube.h>
#include <WorldGUI.h>
#include <FloatingHealthBar.h>
#include <CheckPlayerHeight.h>
#include <PhysicsComponent.h>
#include <PrintPositions.h>
#include <SimpleCamera.h>
#include <ModelManager.h>
#include <ObjectFactory.h>

PhysicsTestScene::PhysicsTestScene()
{
	name = "Physics";
}


PhysicsTestScene::~PhysicsTestScene()
{
}

void PhysicsTestScene::run()
{
	gameRefr->captureMouse = true;
	gameRefr->setPlayer(player);
	gameRefr->setPlaying(true);
}

void PhysicsTestScene::load(Game * g) 
{
	gameRefr = g;

	loadInit();

	loadBase();

	loadPlayer();

	loadCubes();

	loadCamera();

	loadGUI();

	currLighting->addLight(new Daylight(glm::vec3(0, 1, 0.3)));
	currLighting->lights[0]->castShadows = true;

	gameRefr->setConfigAll();
}

void PhysicsTestScene::loadInit()
{
	Scene::loadInit();

	GameObject * skyBox = new Skybox(0);

	addGameObject(skyBox);
}

void PhysicsTestScene::loadBase()
{
	Model* cubeData = ModelManager::getModel("Objects//Cube.obj");

	Custom *  base = new Custom(cubeData, glm::vec3(0,0,0), glm::vec3(11), glm::vec3(0));

	base->e = cubeData->getExtents(0);

	base->addComponent(new PhysicsComponent(base, 0));
	Extents scaledExtents = base->getScaledExtents();

	float x = scaledExtents.maxExtent.x - scaledExtents.minExtent.x;
	float y = scaledExtents.maxExtent.y - scaledExtents.minExtent.y;
	float z = scaledExtents.maxExtent.z - scaledExtents.minExtent.z;

	btVector3 halfBox = btVector3(x / 2.0f, y / 2.0f, z / 2.0f);
	base->setCollisionShape(new btBoxShape(halfBox));
	addGameObject(base);
}

void PhysicsTestScene::loadCamera()
{
	//Setup the game camera
	Camera * newCamera = new FirstPersonCamera(player);

	//Camera* newCamera = new StaticLookAtCamera(glm::vec3(0,140,0), player);
	newCamera->setTarget(player);
	gameRefr->setCamera(newCamera);

}

void PhysicsTestScene::loadPlayer()
{
	Model *cube = ModelManager::getModel("Objects//Cube.obj");

	Custom *cube1 = new Custom(cube, glm::vec3(0.0f, 120.0f, 0.0f), glm::vec3(0.08f), glm::vec3(0.0f, 0.0f, 0.0f));

	cube1->addComponent( new PhysicsComponent(cube1, 10000));
	cube1->getComponent<PhysicsComponent>()->bulletIndependant = true;
	cube1->e = cube->getExtents(0);

	Extents scaledExtents = cube1->getScaledExtents();


	float x = scaledExtents.maxExtent.x - scaledExtents.minExtent.x;
	float y = scaledExtents.maxExtent.y - scaledExtents.minExtent.y;
	float z = scaledExtents.maxExtent.z - scaledExtents.minExtent.z;

	btVector3 halfBox = btVector3(x / 2.0f, y / 2.0f, z / 2.0f);

	cube1->setCollisionShape(new btBoxShape(halfBox));

	addGameObject(cube1);

	player = cube1;

	//Make a player controller
	PlayerController * controller = new PlayerController(gameRefr);

	player->addPrivateBehaviour(controller);

	GenerateCube * cubeGen = new GenerateCube(cube, player, this);

	player->addPrivateBehaviour(cubeGen);
}

void PhysicsTestScene::loadCubes()
{
	Model *cube = ModelManager::getModel("Objects//Cube.obj");

	Custom *cube1 = new Custom(cube, glm::vec3(15.0f, 18.5f, 10.0f), glm::vec3(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
	Custom *cube2 = new Custom(cube, glm::vec3(0.0f, 18.5f, 10.0f), glm::vec3(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));

	cube1->addComponent(new PhysicsComponent(cube1, 50.0f));
	cube1->e = cube->getExtents(0);

	cube1->setCollisionShape(ObjectFactory::getInstance()->makeBoxCollider(cube1));

	cube2->addComponent(new PhysicsComponent(cube2, 50.0f));
	cube2->e = cube->getExtents(0);

	cube2->setCollisionShape(ObjectFactory::getInstance()->makeComplexCollider(cube));

	addGameObject(cube1);
	addGameObject(cube2);
}


void PhysicsTestScene::loadGUI()
{

}