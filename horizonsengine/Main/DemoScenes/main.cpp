#include <GameObject.h>
#include <EngineWrapper.h>
#include "WaterScene.h"
#include "LoadScreen.h"
#include "DemoScene.h"
#include "PhysicsTestScene.h"
#include "TerrainScene.h"
#include <glfw/glfw3.h>


int main(int argc, char* argv[]) {

	EngineWrapper * horizonsInstance = new EngineWrapper();


	horizonsInstance->initGL(argc, argv, "Horizons Engine Demo Scenes");

	horizonsInstance->sortConfig();

	Scene * levelOne = new LoadScreen();

	horizonsInstance->initializeGame(levelOne);

	horizonsInstance->addScene(new WaterScene());
	horizonsInstance->addScene(new TerrainScene());
	horizonsInstance->addScene(new PhysicsTestScene());
	horizonsInstance->addScene(new DemoScene());

	horizonsInstance->startGame();

	return 0;
}
