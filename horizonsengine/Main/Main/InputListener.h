#pragma once
#include "Game.h"
#define INPUT_LEFT_MOUSE_DOWN 0
#define INPUT_RIGHT_MOUSE_DOWN 1
#define INPUT_LEFT_MOUSE_UP 2
#define INPUT_RIGHT_MOUSE_UP 3
#define INPUT_MIDDLE_MOUSE_DOWN 4
#define INPUT_MIDDLE_MOUSE_UP 5


#define HE_INPUT_LEFT_MOUSE_DOWN 0
#define HE_INPUT_RIGHT_MOUSE_DOWN 1
#define HE_INPUT_LEFT_MOUSE_UP 2
#define HE_INPUT_RIGHT_MOUSE_UP 3
#define HE_INPUT_MIDDLE_MOUSE_DOWN 4
#define HE_INPUT_MIDDLE_MOUSE_UP 5

class InputListener
{
public:
	InputListener() 
	{
		Game::singleton()->currentScene()->registerInputListener(this);
	}
	~InputListener()
	{
		Game::singleton()->currentScene()->removeInputListener(this);
	}
	virtual void OnClick(int key, float x, float y) {}
	virtual void mouseMovePos(float x, float y) {}
	virtual void scrollWheel(double x, double y) {}
};