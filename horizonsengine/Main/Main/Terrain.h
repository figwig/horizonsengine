#pragma once
#include "Object3D.h"
#include "TerrainTextureData.h"
#include "TerrainShaders.h"

class TerrainRenderingComponent;


class Terrain :
	public Object3D
{
public:
	Terrain(Model * ,TerrainTextureData * t);
	~Terrain();

	void update(double);

	void draw(RenderSettings*);

	void setTilingFactor(int x);
protected:

	TerrainRenderingComponent* trc = nullptr;


	TerrainTextureData * texData =nullptr;

	TerrainShaders * defaultShadersAsTerr = nullptr;
};

class TerrainRenderingComponent : public Component
{
public:
	TerrainRenderingComponent(Terrain* terr) : Component(terr)
	{

	}

	int tilingFactor = 20;

};
