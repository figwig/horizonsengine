#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Object3D.h"


class Custom :
	public virtual Object3D
{
public:
	Custom(Model* shape, glm::vec3 pos, glm::vec3 scale, glm::vec3 rot);

	
	~Custom();


	void setup();

protected:

};

