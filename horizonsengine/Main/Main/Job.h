#pragma once
#include <functional>


struct CpuJob
{
public:
	std::function<void()> function;

	uint32_t jobID;

	bool reportDone;
};


