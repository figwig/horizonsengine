#include "LogicalColliderFactory.h"


LogicalColliderFactory * LogicalColliderFactory::thisPointer = nullptr;
LogicalColliderFactory::LogicalColliderFactory()
{
}


LogicalColliderFactory::~LogicalColliderFactory()
{
}

void LogicalColliderFactory::assignVolumeToObject(Extents volumeDetails, std::string typeAndHostName, Scene * scene, SceneConfig scMain)
{
	int type = -1;
	int boxKeyLen = scMain.boxVolumeKeyword.length();

	int cylKeyLen = scMain.cylinderVolumeKeyword.length();
	int trckKeyLen = scMain.trackKeyword.length();

	std::string hostname;

	if (typeAndHostName.substr(0, boxKeyLen).find(scMain.boxVolumeKeyword) != string::npos)
	{
		type = CF_VOLUMETYPE_BOX;
		hostname = typeAndHostName.substr(boxKeyLen);
	}

	if (typeAndHostName.substr(0, cylKeyLen).find(scMain.cylinderVolumeKeyword) != string::npos)
	{
		type = CF_VOLUMETYPE_CYL;
		hostname = typeAndHostName.substr(cylKeyLen);
	}



	if (type == -1 || hostname == "NoHost") continue;

	std::vector<GameObject*> objects = scene->getObjects();

	for (int objCount = 0; objCount < objects.size(); objCount++)
	{
		GameObject* currObject = objects[objCount];

		if (currObject != nullptr)
		{
			if (currObject->name == hostname)
			{
				Volume * vol = nullptr;
				currObject->componentReference->physics = new PhysicsComponent(currObject);
				if (type == CF_VOLUMETYPE_BOX)
				{
					vol = objectMaker->makeBoxCollider(volumeExt, currObject);
					currObject->componentReference->physics->setState(PhysicsStates::PHYSICS_STATIC);
				}
				if (type == CF_VOLUMETYPE_CYL)
				{
					vol = objectMaker->makeCylinderCollider(volumeExt, currObject);
					currObject->componentReference->physics->setState(PhysicsStates::PHYSICS_STATIC);
				}

				currObject->setVolume(vol);


			}
		}
	}
}

LogicalColliderFactory* LogicalColliderFactory::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer == new LogicalColliderFactory();
	}
	return thisPointer;
}