#include "WaterPlane.h"
#include "WaterFBO.h"
#include "LightingState.h"
#include "Config.h"
#include "Light.h"
#include "ShaderManager.h"
#include "DebugCentre.h"
#include "Game.h"

WaterPlane::WaterPlane(glm::vec2 d)
{
	setup();

	name = "WaterPlane";

	dimensions = d * 20.0f;
	intializeFBOGroup();
	applyScaling();
	sortVAO();


	Model* model = new Model();

	MeshData* mesh = new MeshData(thisVAO, 6, 6);

	model->addMesh(mesh);

	getComponent<RenderConditions>()->setModel(model);

	ref = waterFBO->getReflectionTexture();
	refra = waterFBO->getRefractionTexture();
	depthMap = waterFBO->getDepthBuffer();

	waterMaterial = new WaterMaterial(new Texture(ref), new Texture(refra), new Texture(dudv), new Texture(normal), new Texture(depthMap));

	getComponent<RenderConditions>()->setMaterial(waterMaterial);

	getComponent<RenderConditions>()->renderType = (PassFlagTypes::Transparent);
	getComponent<RenderConditions>()->setShader(WaterShaders::getInstance());

	DebugCentre::singleton()->addDebugTexture(waterMaterial->reflection);
	DebugCentre::singleton()->addDebugTexture(waterMaterial->refraction);

}

void WaterPlane::setDudv(GLuint x)
{
	waterMaterial->dudv = new Texture(x);
}

void WaterPlane::setNormal(GLuint x)
{
	waterMaterial->normal = new Texture(x);
}

WaterPlane::~WaterPlane()
{
	DebugCentre::singleton()->removeDebugTexture(waterMaterial->reflection);
	DebugCentre::singleton()->removeDebugTexture(waterMaterial->refraction);
}


void WaterPlane::setup()
{
	wrc = new WaterRenderingComponent(this);

	addComponent(wrc);

	wrc->dimensions = dimensions;

	addPrivateBehaviour(new WaterUpdateBehaviour(this));
}

void WaterPlane::applyScaling()
{
	for (int i = 0; i< 6; i++)
	{
		vertices[i*3] *= dimensions.x;
		vertices[2+(i*3)] *= dimensions.y;
	}
}

void WaterPlane::draw(RenderSettings*rs)
{

	////Set the program, and enable alpha blending
	//glUseProgram(thisConfig.waterShaders->getProgramID());
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	////Set the texturess
	//thisConfig.waterShaders->setTextures();
	////Srt the offset
	//thisConfig.waterShaders->setOffset(offset);

	////Set the mats
	//thisConfig.waterShaders->setMats(T, R, S, rs->cameraMat);

	////Set the lighting uniforms
	//thisConfig.waterShaders->setLighting(0, thisConfig.currLighting);

	////Set tiling factor
	//thisConfig.waterShaders->setTiling(dimensions);
	////Bind the correct textures
	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, ref);
	//
	//glActiveTexture(GL_TEXTURE1);
	//glBindTexture(GL_TEXTURE_2D, refra);

	//glActiveTexture(GL_TEXTURE2);
	//glBindTexture(GL_TEXTURE_2D, dudv);

	//glActiveTexture(GL_TEXTURE3);
	//glBindTexture(GL_TEXTURE_2D, normal);

	//glActiveTexture(GL_TEXTURE4);
	//glBindTexture(GL_TEXTURE_2D, depthMap);

	//glBindVertexArray(thisVAO);
	//glDrawArrays(GL_TRIANGLES, 0, 6);

	//glDisable(GL_BLEND);
}

void WaterPlane::sortVAO()
{
	glGenVertexArrays(1, &thisVAO);
	Game::singleton()->getRenderEngine()->bindVAO(thisVAO);

	//Setup the Vertex buffer
	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* 3 * 6, vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	
	GLuint indiciesVBO = 0;
	GLuint indicies[6] = { 0,1,2,3,4,5 };

	glGenBuffers(1, &indiciesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), indicies, GL_STATIC_DRAW);

	Game::singleton()->getRenderEngine()->bindVAO(0);

}

void WaterPlane::intializeFBOGroup()
{
	waterFBO = new WaterFBO(this);
	thisFBO = waterFBO;
}

