#pragma once
#include "Shader.h"
class GUIShader :
	public Shader
{
public:
	static GUIShader * getInstance();
	~GUIShader() {}

	void setProj(glm::mat4);

	void setup();

	void setScreenPos(glm::mat4);

	void setScaleMat(glm::mat4);

	void prepareToRender(GameObject*);

	void prepareToRender(RenderSettings*);

	void setColor(glm::vec4);

	void setColorStrength(float);
	
	void setNoTex(bool);

	void setZPos(float z);

private:
	GUIShader();

	std::string vertexFilename = "Shaders\\vertex_2d.shader";
	std::string fragFilename = "Shaders\\fragment_2d.shader";

	GLuint locScale = 0;
	GLuint locProj = 0;
	GLuint locPos = 0;
	GLuint locColor = 0;
	GLuint locStrength = 0;
	GLuint locNoTex = 0;
	GLuint locZVal = 0;

	static GUIShader* thisPointer;
};

