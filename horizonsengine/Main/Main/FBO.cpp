#include "FBO.h"
#include "Game.h"


FBOGroup::FBOGroup()
{
	dd = Game::singleton()->getDisplayDetails();
}


FBOGroup::~FBOGroup()
{
}

ScreenBuffer::ScreenBuffer(PassFlags pf = 0)
{
	//FBO id etcs are all left as 0 as just rendering to frame buffer
	bufferCount = 1;

	flagsWantedByPass = pf;
}