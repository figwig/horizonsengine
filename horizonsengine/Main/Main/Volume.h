#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Triangle.h"
class Scene;
class GameObject;

//Collider types	
class AABBCollider;
class StaticUprightCylinderVolume;
class TerrainCollider;
class PlaneCollider;
class PointVolume;
class TrackCollider;


struct TriangleCheckResult
{
	float height = -100000.0f;
	bool above = false;
	Triangle tri;
};
struct CheckResult
{
	bool collided = false;
	float magnitude;
	glm::vec3 normalOfImpact;
	glm::vec3 penetration;
	glm::vec3 pointOfImpact;
}; 

struct HeightCheckResult
{
	bool above = false;
	float height;
};


class Volume
{
public:
	Volume(GameObject* h) { host = h; }
	virtual ~Volume() {}

	//Check if a point has collided with this volume
	virtual CheckResult checkPoint(glm::vec3) = 0;
	
	//check if a volume has collided with this volume
	virtual bool checkVolume(Volume *) = 0;

	//return the effective bounding box minExtent
	virtual glm::vec3 getMinExtentsWorld() = 0;
	
	//return the effective bounding box maxExtent
	virtual glm::vec3 getMaxExtentsWorld() = 0;

	//get the hightest Y co-ord of this object given an XZ position
	virtual HeightCheckResult getHeightAtXZPoint(glm::vec3) = 0;

	glm::vec3 getCentre() { return centre; }

	void setOwner(GameObject * newObject) { host = newObject; }

	virtual CheckResult CollideWith(AABBCollider* other) = 0;
	virtual CheckResult CollideWith(StaticUprightCylinderVolume* other) = 0;
	virtual CheckResult CollideWith(TerrainCollider* other) = 0;
	virtual CheckResult CollideWith(PlaneCollider* other) = 0;
	virtual CheckResult CollideWith(PointVolume* other) = 0;
	virtual CheckResult CollideWith(TrackCollider* other) = 0;

	virtual CheckResult CollideWithCollider(Volume * other) =0;

protected:

	int pointsCount = 0;

	glm::vec3 centre= glm::vec3(0);

	glm::vec3 *checkPoints= { nullptr };

	int bufferIndex = 0;

	GameObject * host;
};

