#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include <stdio.h>
#include <vector>
#include <wincodec.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <string>
#include "Volume.h"
#include "Material.h"
#include "Extents.h"
class AssimpContainer;

class MeshData
{
public:

	MeshData(GLuint VAO, GLuint newTexture, GLuint newSpecTexture, GLuint newNormalMap, float *, float *, float*, float *, float *,GLuint * indices, int vertsCount, int indiciesCount);

	MeshData(GLuint VAO , int newIndiciesCount, int vertsCount = 0)
	{
		//Deal with this incredibly clumsy work around!
		indiciesV.resize(newIndiciesCount);
		defaultVAO = VAO;
	}
	MeshData(GLuint VAO, float*, float*, float*, float*, float*, GLuint* indices, int vertsCount, int newIndiciesCount);

	MeshData(std::vector<glm::vec3> points, std::vector<glm::vec3> normals, std::vector<glm::vec2> texts, std::vector<glm::vec4> tans, std::vector<glm::vec4> bitans, std::vector<GLuint> indicies, int count);

	MeshData() {//used for text only
	}

	~MeshData() 
	{
	}
	
	std::string name;

	//use THIS for draw calls and indicie buffers
	int getElementsCount() { return indiciesV.size(); }
	int getTrianglesCount() { return indiciesV.size() / 3; }

	GLuint getVAO() { return defaultVAO; }

	void setVAO(GLuint x) { defaultVAO = x; }

	GLuint getTexture() { return texture; }
	GLuint getSpecTexture() { return specTexture; }
	GLuint getNormalMap() { return normalMap; }
	
	float * getPoints() { return pointsV.data(); }
	float * getNormals() { return normalsV.data(); }
	float * getTangents() { return tangentsV.data(); }
	float * getBitangents() { return bitangentsV.data(); }
	float * getTexts() { return textsV.data(); }
	GLuint * getIndices() { return indiciesV.data(); }

	std::vector<float>* getPointsV() { return &pointsV; }
	std::vector<float>* getNormalsV() { return &normalsV; }
	std::vector<float>* getTangentsV() { return &tangentsV; }
	std::vector<float>* getBitangentsV() { return &bitangentsV; }
	std::vector<float>* getTextsV() { return &textsV; }
	std::vector<GLuint>* getIndiciesV() { return &indiciesV; }

	void setTexts(std::vector<float> newTexts)
	{
		textsV = newTexts;
	}

	glm::vec3 getVertexByIndex(int i);
	void setExtents(Extents x) { e = x; }
	Extents getExtents() { return e; }

	Material* getDefaultMaterial() { return defaultMaterial; }

	void newData(std::vector<float> points, std::vector<float> normals, std::vector<float> tangents, std::vector<float> bitangents, std::vector<float> texts, std::vector<GLuint> indicies);

	void add(MeshData*);

	//Use THIS for vertex buffers, tis the count of the buffer 
	int getPointsCount() { return pointsV.size()/3; }

	int getBaseVertexOffset() { return offsetVAO; }
	int getElementsOffset() { return offsetElements; }

	void setOffset(int baseVertexOffset, int elementsOffset) { offsetVAO = baseVertexOffset; offsetElements = elementsOffset; }
	


protected:

	friend class AssimpContainer;

	Material* defaultMaterial = nullptr;
	std::vector<float> pointsV;
	std::vector<float> normalsV;
	std::vector<float> tangentsV;
	std::vector<float> bitangentsV;
	std::vector<float> textsV;
	std::vector<GLuint> indiciesV;

	Extents e;
	GLuint defaultVAO;

	GLuint texture =0;
	GLuint specTexture=0;
	GLuint normalMap=0;

	int offsetVAO = 0;
	int offsetElements = 0;
};




class Model
{
public:
	//New style constructor, assuming multiple meshdatas are going to be used using the new callback methods. 
	Model() {}
	~Model() {}

	void addMesh(MeshData* newMd) { meshes[totalMeshes] = newMd; totalMeshes++; }


	//Fetch the induvidual  VAO by index
	GLuint getVAOByIndex(int i) { return meshes[i]->getVAO(); }

	//fetch the induvidual verts count
	int getElementsCountByIndex(int i) { return meshes[i]->getElementsCount(); }

	GLuint getTextureByIndex(int i) { return meshes[i]->getTexture(); }
	GLuint getSpecMapByIndex(int i) { return meshes[i]->getSpecTexture(); }
	GLuint getNormalMapByIndex(int i) { return meshes[i]->getNormalMap(); }

	MeshData* getMeshByIndex(int i) { return meshes[i]; }

	int getMeshCount() { return totalMeshes; }

	Extents getExtents(int i) { return meshes[0]->getExtents(); }

protected:

	//The VAO to store things in
	GLuint shapeVAO = 0;

	int totalVerts = 0;

	int totalMeshes = 0;

	const static int MAX_MESHES = 1024;
	MeshData* meshes[MAX_MESHES] = { nullptr };

};




class InstancedVAOData : public Model
{
public:
	InstancedVAOData() {}
	~InstancedVAOData() {}
	int getInstances() { return instances; }
	void setInstances(int x) { instances = x; }
protected:
	int instances = 0;
};