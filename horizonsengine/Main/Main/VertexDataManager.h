#pragma once
#include <glad\glad.h>
class MeshData;
class Model;
class Shader;
#include <vector>
#include <glm-0.9.9.2/glm/glm.hpp>
#include <map>

class InstancedMaterial;

typedef uint32_t VAOType;
typedef uint32_t HE_PerInstanceDataFlags;
class VertexDataManager;


enum VAOTypes
{
	FullFat = 0b1,
	GUI = 0b01
};


class HE_ModelInstanceHandle
{
public:
	//The index for the meshdata itself
	std::vector<int> meshDataIndexes;
	//The index for every property within each meshdata's property lists
	std::vector<int> propertyIndexes;

private:
	bool valid = true;
	static HE_ModelInstanceHandle invalid() { HE_ModelInstanceHandle he; he.valid = false; return he; }
	friend class VertexDataManager;
};

struct HE_PerInstanceDataSet
{
public:
	static enum HE_PerInstanceDataSetFlags
	{
		ModelMatrix = 0b1,
		MaterialID = 0b10
	};
	 
	std::vector<int> indexMask;

	std::vector<glm::mat4> modelMatricies;

	std::vector<GLuint> materialIDs;
};

struct VAO
{
public:
	GLuint id = 0;

	int vertexSize = 0;
	int elements = 0;

	VAOType vaoType = 0;

	GLuint shapeVAO;
	GLuint vertsVBO;
	GLuint normsVBO;
	GLuint tangentsVBO;
	GLuint bitangentsVBO;
	GLuint textsVBO;
	GLuint indiciesVBO;
	GLuint modelMatriciesVBO;
	GLuint materialIDVBO;

	//Each entry to this represents the data set per mesh that  is included
	std::vector<std::pair<MeshData *, HE_PerInstanceDataSet >> VAOlayout;

	std::vector<GLuint> fullMaterialBuffer;

	bool matrixBufferNeedsFlushing = false;

	bool materialBufferNeedsFlushing = false;

	//Default is model matrix and materialID, which evaluates to 3
	HE_PerInstanceDataFlags flags = 3;

	InstancedMaterial* vaoMaterial = nullptr;

	Shader* shader = nullptr;
};

class VertexDataManager
{
public:
	static VertexDataManager * getInstance();

	GLuint addMeshToVAO(MeshData *, bool indexedAlready = false);

	GLuint makeBasicVAO(std::vector<float> points);

	void submitModelMatrix(glm::mat4, HE_ModelInstanceHandle);

	void submitMaterialID(std::vector<GLuint> materialIDs,  HE_ModelInstanceHandle);

	HE_ModelInstanceHandle addInstanceOfMeshes( Model*);
	
	void removeInstanceOfMeshes(Model*, HE_ModelInstanceHandle);

	void flushBuffers();

	GLuint getVAOForShaderType(Shader*);
	InstancedMaterial* getMaterialForShaderType(Shader*);
	GLuint getBaseInstance(Shader*, MeshData*);
	GLuint getInstanceCount(Shader*, MeshData*);

private:
	static VertexDataManager* thisPointer;
	VertexDataManager();
	~VertexDataManager();

	//Deprecated - while it works it's not very fast
	void indexMeshData(MeshData*);

	bool indexingAccurayOn = false;

	int indexingAccuracy = 30000;

	bool isPointNear(float a, float b, float accuracy = 0.9f);

	std::vector<VAO> vaos;

	VAO indexedVAO;

	void processModelMatrixUpdates(VAO *vao);
	void processMaterialUpdates(VAO * vao);
};

