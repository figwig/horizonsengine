#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>
#include "RenderSettings.h"
#include "Shader.h"
#include "PrivateBehaviour.h"
#include "ComponentReference.h"
#include "Extents.h"
#include "Volume.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <btBulletDynamicsCommon.h>
#include <btBulletCollisionCommon.h>
#include <glm-0.9.9.2/glm/gtx/matrix_decompose.hpp>
#include "Object.h"
#include "RenderConditions.h"
#include "Config.h"
#include <functional>
#include <optional>

class GameObject : public virtual Object
{
public:
	GameObject();


	virtual ~GameObject();


	virtual void draw() {}

	//return the position
	glm::vec3 getPos();

	//return the rotation
	glm::vec3 getRot();

	virtual glm::vec3 getScale();

	glm::mat4 getRotMat();

	glm::mat4 getTransMat();

	glm::mat4 getScaleMat();

	glm::mat4 getModelMat();


	virtual void setPos(glm::vec3 pos);

	void setRot(glm::vec3 pos);

	void setRot(glm::quat q);

	void setScale(glm::vec3 pos);

	void setConfig(Config newConfig) { thisConfig = newConfig; }

	Config getConfig() { return thisConfig; }

	btCollisionShape* getCollisionShape() { return btVolume; }

	void setCollisionShape(btCollisionShape* v) { btVolume = v; }

	virtual void setParent(GameObject* newParent);

	virtual GameObject* getParent() { return parent; }

	void addPrivateBehaviour(PrivateBehaviour* x)
	{
		for (int count = 0; count < MAX_BEHAVIOURS; count++)
		{
			if (privateBehaviours[count] == nullptr)
			{
				privateBehaviours[count] = x;
				x->setTarget(this);
				x->setup();
				BehaviourManager::singleton()->newBehaviour(x);
				return;
			}
		}
	}

	//returns the extents timed by the scale and the transformation. Please fix to include rotation
	Extents getWorldExtents();
	Extents getScaledExtents();

	std::string name = "No Name Assigned";
	Extents e;

	void kill() { dead = true; }

	std::vector<GameObject* > children;

	void removeChild(GameObject* go);

protected:

	void setup() {  }

	bool dead = false;

	Config thisConfig;

	GameObject* parent = nullptr;


	const static int MAX_BEHAVIOURS = 24;
	PrivateBehaviour* privateBehaviours[MAX_BEHAVIOURS] = { nullptr };
	btCollisionShape* btVolume = nullptr;

	glm::mat4 R = glm::toMat4(glm::quat(glm::vec3(0.0f, 0.0f, 0.0f)));

	glm::mat4 T = glm::mat4();

	glm::mat4 S = glm::mat4(1);

	glm::mat4 modelMat = glm::mat4();

	float rotX = 0.0f;
	float rotY = 0.0f;
	float rotZ = 0.0f;

	friend class RenderConditions;

	void invalidateMatricies();
	bool callbackSet = false;

	std::function<void()> callOnModelMatChange;
};

