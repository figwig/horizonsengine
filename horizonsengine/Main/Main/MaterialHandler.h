#pragma once
#include "Material.h"
#include <vector>

class MaterialHandler;

class NullMaterial : public Material
{
public:
	void Bind() {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
private:
	friend class MaterialHandler;
	NullMaterial() {}
};


class MaterialHandler
{
public:

	Material* getNullMat() { return nm; }
	static MaterialHandler* singleton();

	Material* makeMaterial(Texture* diffuse, Texture* specular, Texture* normal);
	Material* makeMaterial(std::string diffuse, std::string specular, std::string normal);

	SingleMaterial* makeSingleMaterial(Texture* t);

	//Look for all the textures needed for a skybox ina specific directory
	SkyboxMaterial* makeSkyboxMaterial(std::string directory);

private:
	std::vector<Material*> materials;
	std::vector<SingleMaterial*> singleMaterials;

	static MaterialHandler* thisPointer;

	NullMaterial* nm = nullptr;

	MaterialHandler() { nm = new NullMaterial(); }
	~MaterialHandler() { delete nm; }
};

