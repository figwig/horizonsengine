#pragma once
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include "VAOData.h"
#include "TextureExaminer.h"

class AssimpContainer
{
public:
	AssimpContainer() { TextureExaminer::singleton()->setDirectory("Textures\\"); }
	~AssimpContainer() {}

	Model* readFile(std::string file);

private:

	void lookThroughNode(aiNode*, glm::mat4 carryThroughTrans, const aiScene *);

	void dealWithMesh(aiMesh*, const aiScene*, glm::mat4 = glm::mat4(1));

	std::vector<MeshData*> meshDatas;

	aiScene* scene = nullptr;
};

