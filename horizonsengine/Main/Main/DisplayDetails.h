#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class DisplayDetails
{
public:
	float height = 1080.0f;
	float width = 1920.0f;

	float aaAmount = 0;

	void update()
	{
		
	}

	float fovDegs = 60;

	float nearPlane = 0.1f;

	float farPlane = 200000.0f;

	float getAspectRatio() { return width / height; }

	float shadowMapSize = 4096.0f;
	float shadowDistance = 200.0f;
};