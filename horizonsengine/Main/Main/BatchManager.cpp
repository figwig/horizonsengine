#include "BatchManager.h"



BatchManager::BatchManager(Config c) : thisConfig(c)
{

}


BatchManager::~BatchManager()
{
}

void BatchManager::makeNewBatchSet(Scene * currScene)
{
	GameObject * currObject;
	//int max = currScene->getNumObjects();
	int batchCount = 0;
	int unbatchableCount = 0;
	int meshesToBatchCount = 0;
	//First sort all objects into things that can be batched and things that can't
	for (int count = 0; count < 5; count++)
	{

		//currObject = currScene->getObjectByIndex(count);


		//	//batchable object
		//	notBatched[batchCount] = currObject;
		//	batchCount++;
		//	int max = currObject->getVAOData()->getMeshCount();
		//	//Add the meshes to batch to the list
		//	for (int count2 = 0; count2< max; count2++)
		//	{
		//		meshesToBatch[meshesToBatchCount] = currObject->getVAOData()->getMeshByIndex(count2);
		//		meshesToBatchCount++;
		//	}
		//
		//else
		//{
		//	//not batchable - don't worry about it, just render solo
		//	notBatchable[unbatchableCount] = currObject;
		//	unbatchableCount++;
		//}
		
	}

	Batch* currBatch = new Batch();
	int texturesLeft = 16;
	int matriciesLeft = currBatch->matsPerBatch;
	int meshesLeft = meshesToBatchCount;

	while (meshesLeft > 0)
	{
		for (int count = 0; count < meshesToBatchCount; count++)
		{

		}
	}

}

bool BatchManager::tryMeshFitInBatch(Batch* batch, MeshData * mesh, glm::mat4 mat)
{
	//First, check if there are enough texture slots

	//4 textures per mesh
	int tex[4];

	tex[0]= mesh->getTexture();
	tex[1] = mesh->getSpecTexture();
	tex[2] = mesh->getNormalMap();
	tex[3] = 0;

	bool noSpace = false;
	int spaceCount = 0;

	for (int slotCount = 0; slotCount < 16; slotCount++)
	{
		if (batch->textures[slotCount] == -1)
		{
			spaceCount++;
			continue;
		}
		for (int texCount = 0; texCount < 4; texCount++)
		{
			if (batch->textures[slotCount] == tex[texCount])
			{
				spaceCount++;

			}
		}
	}

	//if there were less than 4 spaces/common textures, return false
	if (spaceCount < 4) return false;

	int texIndexes[4];
}