#include "LightingState.h"
#include "Light.h"
#include "VAOData.h"
#include "Game.h"
#include "ModelManager.h"

LightingState::LightingState()
{

	sphereData = ModelManager::getModel("Objects/LightSphere.obj");

}
void LightingState::addLight(Light* newLight) 
{
	for (int count = 0; count < MAX_LIGHTS; count++)
	{
		if (lights[count] == nullptr)
		{
			lights[count] = newLight;
			numberOfLights++;
			Game::singleton()->getRenderEngine()->checkForLights();
			return;
		}
	}
}


