#pragma once
#include "TerrainTextureData.h"
#include "texture_loader.h"

class GrassyTerrainTextureData : public TerrainTextureData
{
public:
	GrassyTerrainTextureData()
	{
		bases[0] = fiLoadTexture("Textures/grass01.jpg");
		specs[0] = fiLoadTexture("Textures/grass01_s.jpg");
		normals[0] = fiLoadTexture("Textures/grass01_n.jpg");
		normalInvertY[0] = 1;

		bases[1] = fiLoadTexture("Textures/grass02.jpg");
		specs[1] = fiLoadTexture("Textures/grass02_s.jpg");
		normals[1] = fiLoadTexture("Textures/grass02_n.jpg");

		bases[2] = fiLoadTexture("Textures/stone_01.jpg");
		specs[2] = fiLoadTexture("Textures/stone_01_s.jpg");
		normals[2] = fiLoadTexture("Textures/stone_01_n.jpg");

		bases[3] = fiLoadTexture("Textures/grass02.jpg");
		specs[3] = fiLoadTexture("Textures/grass02_s.jpg");
		normals[3] = fiLoadTexture("Textures/grass02_n.jpg");


		splat = fiLoadTexture("Textures/grassSplat.png");
	}
};
