#include "DebugCentre.h"
#include "Game.h"
#include <map>

DebugCentre* DebugCentre::thisPointer = nullptr;

DebugCentre::DebugCentre()
{
	init();
}

void DebugCentre::init()
{
	fps = new TextObject(Game::singleton()->getConfig().codeFontCharacters);
	fps->setPos(glm::vec3(0.0f, 10.0f, 0.0f));
	fps->setConfig(Game::singleton()->getConfig());
	fps->setScale(glm::vec3(0.4f));



	sr = Game::singleton()->getRenderEngine();

	rs = new RenderSettings(glm::mat4(1), false, glm::vec4(0));
}

DebugCentre::~DebugCentre()
{
}


DebugCentre * DebugCentre::singleton()
{
	//Lazy initialization
	if (thisPointer == nullptr)
	{
		thisPointer = new DebugCentre();
	}
	return thisPointer;
}

void DebugCentre::addDebugTexture(Texture* t)
{
	float spacing = Game::singleton()->getDisplayDetails()->height * 0.01f;
	float size = Game::singleton()->getDisplayDetails()->height * 0.1f;


	Panel * newImg = new Panel(t);

	newImg->setConfig(Game::singleton()->getConfig());

	//Largest dimension has to fit into a portion of the screen
	float lDim = glm::max(t->res.x, t->res.y);

	//Debug images take up a tenth of the screen
	float pixels = size;

	//Theres the scale!
	float scale = pixels / lDim;

	glm::vec3 pos = glm::vec3(spacing);

	int xOffset = debugTextures.size() *( pixels *2.0f + spacing*2.0f);

	pos.x += xOffset;

	pos.z = 0.0f;


	newImg->setScale(glm::vec3(scale));
	newImg->transform->anchorType = GUI_TRANSFORM_ANCHOR_BL;
	newImg->setPos(pos);
	
	debugTextures.push_back(newImg);

	TextObject* title = new TextObject(Game::singleton()->getConfig().codeFontCharacters);

	title->setConfig(Game::singleton()->getConfig());


	title->setPos(pos+ glm::vec3(0, size + 40.0f, 0));

	title->setScale(glm::vec3(0.2f));

	title->transform->anchorType = GUI_TRANSFORM_ANCHOR_BL;

	title->setText(t->filename);


	textureTitles.push_back(title);


}

void DebugCentre::removeDebugTexture(GLuint x)
{
	for(int count = 0 ; count < debugTextures.size(); count++)
	{
		Panel * img = debugTextures[count];
		if (x == img->getTextureID())
		{
			debugTextures.erase(debugTextures.begin() + count);
			textureTitles.erase(textureTitles.begin() + count);

			delete img;
			return;
		}
	}
}

void DebugCentre::removeDebugTexture(Texture* x)
{
	for (int count = 0; count < debugTextures.size(); count++)
	{
		Panel* img = debugTextures[count];
		if (x->id == img->getTextureID())
		{
			debugTextures.erase(debugTextures.begin() + count);
			textureTitles.erase(textureTitles.begin() + count);

			delete img;
			return;
		}
	}
}


void DebugCentre::update(double delta)
{
	if (Game::singleton()->getKeys() & Keys::F3 && inputTimer <= 0.0)
	{
		open = !open;
		inputTimer = 400;
	}
	if (Game::singleton()->getKeys() & Keys::F5 && inputTimer <= 0.0)
	{
		fpsOpen = !fpsOpen;
		inputTimer = 400;
	}


 	if (inputTimer > 0.0)
	{
		inputTimer -= delta;
	}
	
	updateTicker += delta;
	if (updateTicker < 1000) 
		return;
	else 
		updateTicker = 0.0;



	if (!fpsOpen && !open) return;

	//Calculate and update the FPS counter
	int fpsAc = 1000.0f / delta;

	fps->setText("FPS: " + std::to_string(fpsAc));

	fps->update(delta);

	if (!open) return;

	for each (std::pair<TextObject*, string> pair in watchList)
	{
		pair.first->setText(pair.second);
	}


	for each (std::pair<GameObject*, TextObject*  >  currObject in trackedGameObjects)
	{
		GameObject* go = currObject.first;
		
		currObject.second->setText(go->name + " : " + std::to_string(go->getPos().x) + ", " + std::to_string(go->getPos().y) + ", " + std::to_string(go->getPos().z));
	}


}


void DebugCentre::draw()
{
	

	if (!open)
	{ 
		if (fpsOpen)
		{
			//At the top, the fps
			fps->draw();
		}
		return;
	}
	//At the top, the fps
	fps->draw();

	//Update debug textures
	for each(Panel * currImg in debugTextures)
	{
		currImg->draw(rs);

	}

	for each(TextObject * currTex in printOuts)
	{
		currTex->draw();
	}
	for each(TextObject * currTex in textureTitles)
	{
		currTex->draw();
	}
}

void DebugCentre::addTextObject(TextObject* title)
{
	//Set the config of the text so that it can get access to shaders etc
	title->setConfig(Game::singleton()->getConfig());

	//Fetch the display details
	DisplayDetails * dd = Game::singleton()->getDisplayDetails();

	//The yOffset is proportional to the height
	float yOffset = dd->height * 0.018f;

	//make the position vector
	glm::vec3 pos = glm::vec3(0.0f, (printOuts.size()+ 2) * yOffset, 0);

	title->setPos(pos);

	title->setScale(glm::vec3(0.25f));

	title->transform->anchorType = GUI_TRANSFORM_ANCHOR_TL;

	printOuts.push_back(title);

}


void DebugCentre::addObject(GameObject * go, std::string name)
{
	TextObject* title = new TextObject(Game::singleton()->getConfig().codeFontCharacters);

	trackedGameObjects.push_back(std::pair<GameObject*, TextObject* >(go, title));

	go->name = name;

	title->setText(name + " : " + std::to_string(go->getPos().x) + ", " + std::to_string(go->getPos().y) + ", " + std::to_string(go->getPos().z));

	addTextObject(title);

}

int DebugCentre::addWatch(float val, string name)
{
	TextObject* title = new TextObject(Game::singleton()->getConfig().codeFontCharacters);

	addTextObject(title);
	title->setText(name + std::to_string(val));
	watchList.push_back(std::make_pair(title, name));
	title->name = name;

	return watchList.size() - 1;
}

int DebugCentre::addWatch(int val , string name)
{
	TextObject* title = new TextObject(Game::singleton()->getConfig().codeFontCharacters);

	addTextObject(title);
	title->setText(name + std::to_string(val));
	watchList.push_back(std::make_pair(title, name));
	title->name = name;
	return watchList.size() - 1;
}

int DebugCentre::addWatch(glm::vec3 go, string name)
{
	TextObject* title = new TextObject(Game::singleton()->getConfig().codeFontCharacters);

	addTextObject(title);
	title->name = name;
	title->setText(name + " : " + std::to_string(go.x) + ", " + std::to_string(go.y) + ", " + std::to_string(go.z));

	watchList.push_back(std::make_pair(title, name));


	return watchList.size() - 1;
}

void DebugCentre::removeWatch(int location)
{
	TextObject* t = watchList[location].first;

	for (int count = 0; count < printOuts.size(); count++)
	{
		if (printOuts[count] == t)
		{
			printOuts.erase(printOuts.begin() + count);

			delete t;
			break;
		}
	}


	watchList.erase(watchList.begin() + location);

}

void DebugCentre::updateWatch(int location, float value)
{
	std::string name = watchList[location].first->name;
	watchList[location].second = name + ": " + std::to_string( value);
}

void DebugCentre::updateWatch(int location, int value)
{
	std::string name = watchList[location].first->name;
	watchList[location].second = name + ": " + std::to_string(value);
}

void DebugCentre::updateWatch(int location, glm::vec3 go)
{
	std::string name = watchList[location].first->name;

	watchList[location].second = name + " : " + std::to_string(go.x) + ", " + std::to_string(go.y) + ", " + std::to_string(go.z);
}