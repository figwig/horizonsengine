#pragma once
#include "Text.h"

class TextRenderingComponent : public Component
{
public:
	TextRenderingComponent(TextObject* t) : Component((GameObject*)t)
	{
	}
	~TextRenderingComponent() {}

	glm::vec3 colour = glm::vec3(1.0f);
};


class TextMeshData : public MeshData
{
public:
	static TextMeshData* getTextMeshData()
	{
		if (tmd == nullptr)
		{
			tmd = new TextMeshData(0);
		}
		return tmd;
	}
private:

	static TextMeshData* tmd;
	GLuint VAO = 0;

	TextMeshData(GLuint VAO)
	{
		defaultVAO = VAO;
		pointsV.resize(18);
	}
	~TextMeshData()
	{

	}
};

class TextModel : public Model
{
public: 
	static TextModel* getTextModel()
	{
		if (tm == nullptr)
		{
			tm = new TextModel();
		}
		return tm;
	}
private:
	TextModel()
	{

	}
	static TextModel* tm;
};

class TextMaterial : public Material
{
public:
	static TextMaterial* getTextMaterial()
	{
		if (tm == nullptr)
		{
			tm = new TextMaterial();
		}
		return tm;
	}

	void Bind()
	{

	}
private:
	TextMaterial()
	{

	}
	static TextMaterial* tm;
};