#pragma once
#include "BehaviourManager.h"
#include <iostream>
class Behaviour
{
public:
	Behaviour()
	{

	}
	virtual ~Behaviour() 
	{

	}

	void setActive(bool x) { active = x; }

	virtual void update(double f) = 0;
	virtual void setup() {}

	//Overload if this component wants to access other behaviours
	virtual bool isContained() { return contained; }
	int getSortOrder() { return sortOrder; }
	void setSortOrder(int x) { sortOrder = x;  BehaviourManager::singleton()->sortBehaviours(); }
protected:
	int sortOrder = 0;
	bool active = true;

	bool contained = false;
};