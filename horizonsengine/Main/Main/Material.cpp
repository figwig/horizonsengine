#include "Material.h"
int Material::ID = 0;

InstancedMaterial::InstancedMaterial(Shader *shader) :shader(shader)
{
	glGenBuffers(1, &UBOHandle);
	glBindBuffer(GL_UNIFORM_BUFFER, UBOHandle);
	glBufferData(UBOHandle, 64 *materialSize, (void*)0, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, 16, UBOHandle);


}

InstancedMaterial::~InstancedMaterial() {

}

void InstancedMaterial::addTexture(Material *material)
{
	for (auto a : materialPositions)
	{
		if (a.first == material) return;
	}

	addTexture(material->m_diff->id);
	addTexture(material->m_spec->id);
	addTexture(material->m_norm->id);

	materialPositions.push_back(std::make_pair(material, materialPositions.size()));
}

void InstancedMaterial::addTexture(GLuint textureID)
{
	textureIDs.push_back(textureID);
	GLuint64 handle = getBindlessHandle(textureID);
	bindlessTextureHandles.push_back(handle);
	makeHandleResident(handle);
	hasChanged = true;
}

void InstancedMaterial::Bind()
{
	if (!hasChanged) return;
	//GLuint program = shader->getProgramID();
	//std::vector<GLuint> locations = shader->getBindlessLocations();

	//glProgramUniformHandleui64ARB(program, locations[0], bindlessTextureHandles[0]);
	//glProgramUniformHandleui64ARB(program, locations[1], bindlessTextureHandles[1]);
	//glProgramUniformHandleui64ARB(program, locations[2], bindlessTextureHandles[2]);

	updateBuffer();

	hasChanged = false;
}

void InstancedMaterial::updateBuffer()
{
	int ticker = 0;

	std::vector<GLuint64> bufferedTextureHandles;
	
	for (GLuint64 a : bindlessTextureHandles)
	{
		bufferedTextureHandles.push_back(a);
		ticker++;

		if(ticker%3 == 0)
			bufferedTextureHandles.push_back(0);


	}

	glBindBuffer(GL_UNIFORM_BUFFER, UBOHandle);


	glBufferData(GL_UNIFORM_BUFFER, 64 * bufferedTextureHandles.size(), bufferedTextureHandles.data(), GL_STATIC_DRAW);
}


GLuint InstancedMaterial::getMaterialBufferIndex(Material* m)
{
	return getMaterialBufferIndex(m->getMaterialID());
}


GLuint InstancedMaterial::getMaterialBufferIndex(GLuint materialID)
{
	for(auto a : materialPositions)
	{
		if (a.first->getMaterialID() == materialID)
		{
			return a.second;
		}

	}
	return 10;
}
