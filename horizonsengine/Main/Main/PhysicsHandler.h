#pragma once
#include "TerrainCollider.h"
#include <btBulletCollisionCommon.h>
#include <functional>
#include <atomic>
class Scene;
class PhysicsComponent;
class ColliderFactory;

struct RayCastResult
{
	float distanceTravelled;
	glm::vec3 hitPos;
	bool hit;

	const btCollisionShape* collider = nullptr;
	GameObject* object = nullptr;
};


class PhysicsHandler
{
public:
	PhysicsHandler();
	~PhysicsHandler();

	virtual void doPhysics(double, Scene*) = 0;
	virtual void doLatePhysics(double, Scene*) = 0;
	virtual void checkScene(Scene*)= 0;

	void setG(float x) { g = x; }

	virtual RayCastResult castRayDown(glm::vec3 position) = 0;


	virtual RayCastResult castRay(glm::vec3 position, glm::vec3 towards) = 0;

	virtual bool useDefaultColliderTypes() = 0;

	virtual void addToSimulation(PhysicsComponent*)=0;

	virtual void removeFromSimulation(PhysicsComponent*) = 0;

	std::function<void()> physicsFunction;

	std::atomic<bool> physicsDone = false;

protected:

	
	const static int MAX_PHYSICS_COMPONENTS = 2048;
	PhysicsComponent * objectsWithPhysics[MAX_PHYSICS_COMPONENTS] = { nullptr };
	int actObjects = 0;


	const static int MAX_STATIC_PHYSICS_COMPONENTS = 64;
	PhysicsComponent * objectsWithStaticPhysics[MAX_STATIC_PHYSICS_COMPONENTS] = { nullptr };
	int actStatics = 0;

	float g = 9.81f;
};

