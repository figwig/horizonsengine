#include "GLRenderer.h"
#include "Game.h"
#include "ShaderManager.h"
#include "PostProccessingFBO.h"
#include "PostProcessingShader.h"
#include "ShadowShaders.h"
#include "VertexDataManager.h"

GLRenderer* GLRenderer::thisPointer = nullptr;

GLRenderer::GLRenderer()
{

}

GLRenderer::~GLRenderer()
{

}

GLRenderer* GLRenderer::singleton()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new GLRenderer();
	}
	return thisPointer;
	
}

void GLRenderer::initialize()
{
	glGenBuffers(1, &indirectBuffer);

	//The plain post processing rendersettings, save rendering every frame
	postProcessRenderSettings = new RenderSettings(glm::mat4(1), false, glm::vec4(0));

	DisplayDetails* dd = Game::singleton()->getDisplayDetails();
	
	//glViewport(0,0, dd->width, dd->height);

	ppFBO = new PostProccessingFBO(Game::singleton()->getConfig(), 0);

	msFBO = new PostProccessingFBO(Game::singleton()->getConfig(), dd->aaAmount);

	GLuint ppTex = ppFBO->getTexture();

	finalRender = new PostProcessRect(ppTex);

	finalRender->setConfig(Game::singleton()->getConfig());

	finalRender->setShader(ShaderManager::singleton()->getShader<PostProcessingShader*>());

	finalRender->setTexture(ppFBO->getTexture());

	DebugCentre::singleton()->addDebugTexture(new Texture(ppFBO->getTexture(), glm::vec2(200), "Post Proccesing"));

	//Allocate all of the Pass and Render data classes
	FBOPasses = new RenderData();
	masterRenderPass = new RenderData();

	opaquePass = new PassData();

	transparentPass = new PassData();
	GUI1 = new PassData();
	GUI2 = new PassData();



	//Setup master pass and masterpass data (used to store all objects in scene organised
	masterPassData = new PassData();

	masterPassData->name = "Master Pass";

	screenBuffer = new ScreenBuffer(PassFlagTypes::AllObjects | PassFlagTypes::GUI1 | PassFlagTypes::GUI2);

	masterPassData->commonPart = screenBuffer;

	masterRenderPass->contents.push_back(masterPassData);

	//Add the opaque pass first with just opaque objects
	masterRenderPass->contents.push_back(opaquePass);

	opaquePass->commonPart = new ScreenBuffer(PassFlagTypes::OpaqueObjects | PassFlagTypes::Reflective);

	opaquePass->name = "Opaque Pass";

	//Next, the transparent pass
	masterRenderPass->contents.push_back(transparentPass);

	transparentPass->commonPart = new ScreenBuffer(PassFlagTypes::Transparent);

	transparentPass->name = "Transparent Pass";


	//The two GUI passes
	GUI1->commonPart = new ScreenBuffer(PassFlagTypes::GUI1);
	GUI2->commonPart = new ScreenBuffer(PassFlagTypes::GUI2);

	GUI1->name = "GUI1 Pass";
	GUI2->name = "GUI2 Pass";

	masterRenderPass->contents.push_back(GUI1);
	masterRenderPass->contents.push_back(GUI2);
}

void GLRenderer::update(double)
{
	for each (PassData * pd in masterRenderPass->contents)
		pd->commonPart->setCameraAngle(0, gameCamera->getCameraMat());

	VertexDataManager::getInstance()->flushBuffers();
	

}

void GLRenderer::setScene(Scene* scene)
{
	//For now - delete all content from old scene

	for each (PassData * pd in masterRenderPass->contents)
	{
		pd->contents.clear();
	}

	FBOPasses->contents.clear();

	shadowData.clear();

	instancedModelData.clear();

	currentScene = scene;

	masterRenderPass->commonPart = currentScene;

	//Then loop through and add in any existing game objects

	for each (GameObject * go in scene->getAllObjects())
	{
		onAddGameObject(go);
	}

}

//Scene is in for multi-scene support in future
void GLRenderer::onAddFBO(FBOGroup* fbo, Scene* scene)
{
	//For now - add FBO to every pass

	PassData* pd = new PassData();

	pd->commonPart = fbo;

	FBOPasses->contents.push_back(pd);

	addObjectsToPassDataFBO(pd, scene);
}

void GLRenderer::addObjectsToPassDataFBO(PassData* pd, Scene* scene)
{
	FBOGroup* fbo = pd->commonPart;
	for each (GameObject * go in scene->getAllObjects())
	{
		RenderConditions* rc = go->getComponent<RenderConditions>();

		if (rc == nullptr) continue;

		if (fbo->includeGameObject(go))
		{
			addToPassData(pd, go, rc);
		}
		else
		{
			continue;
		}
	}
}

void GLRenderer::addPostProcessor(PostProcessingShader* x)
{
	if (!(ppCount < MAX_PP_SHADERS))
	{
		printf("Error: max post processors already in use\n");
		return;
	}

	auxPPFBOs.push_back(new PostProccessingFBO(thisConfig, 0));

	auxPPFBOs[ppCount]->shader = x;

	Game::singleton()->debugCentre->addDebugTexture(auxPPFBOs[ppCount]->getTextureDetailsByIndex(0));

	PostProcessRect * newPPRect = new PostProcessRect(auxPPFBOs[ppCount]->getTexture());

	newPPRect->setConfig(Game::singleton()->getConfig());

	ppRects.push_back(newPPRect);

	newPPRect->setShader(x);


	textureChainPP();

	ppCount++;
}

void GLRenderer::removePostProcessor(PostProcessingShader* x)
{
	bool foundPP = false;
	for (int count = 0; count < ppCount; count++)
	{
		if (auxPPFBOs[count]->shader == x)
		{
			foundPP = true;
			Game::singleton()->debugCentre->removeDebugTexture(auxPPFBOs[count]->getTexture());

			auxPPFBOs[count]->shader->destroy();



			delete auxPPFBOs[count];

			auxPPFBOs.erase(auxPPFBOs.begin() + count);
			ppCount--;
			


			break;
		}
	}
	if(!foundPP)
		std::cout << "Error: post processor not found\n";


	for (int count = 0; count < ppRects.size(); count++)
	{
		if (ppRects[count]->getShader() == x)
		{
			delete ppRects[count];

			ppRects.erase(ppRects.begin() + count);
		}
	}

	textureChainPP();
}

void GLRenderer::textureChainPP()
{
	if (ppCount == 0)
	{
		finalRender->setTexture(ppFBO->getTexture());
		return;
	}

	//First ppRect has texture from the ppFBO
	ppRects[0]->setTexture(ppFBO->getTexture());

	//Set up texture chaining
	for (int count = 1; count < ppRects.size(); count++)
	{
		ppRects[count]->setTexture(auxPPFBOs[count - 1]->getTexture());
	}

	//final render has to grab the texture  from the final auxFBO
	finalRender->setTexture(auxPPFBOs[ppRects.size() - 1]->getTexture());
}

void GLRenderer::onAddGameObject(GameObject* go)
{
	passesAddedTo = 0;
	RenderConditions* rc = go->getComponent<RenderConditions>();

	if (rc == nullptr) 
	{
		std::cout << "No render component\n";
		return;
	}

	for each (PassData * pd in masterRenderPass->contents)
	{
		addToPassData(pd, go, rc);
	}

	for each (PassData * pd in FBOPasses->contents)
	{
		if (pd->commonPart->includeGameObject(go))
			addToPassData(pd, go, rc);
	}

	for each (auto pair in shadowData)
	{
		PassData* pd = pair.second;
		if (pd->commonPart->includeGameObject(go))
			addToPassData(pd, go, rc);
	}

	std::cout << "Added " << go->name << " to GLRenderer in " << passesAddedTo << " passes\n\n";


}

void GLRenderer::addToPassData(PassData* pd, GameObject* go, RenderConditions * rc )
{
	//First check if this pass wants this object
	PassFlags pf = pd->commonPart->flagsWantedByPass;

	//if all objects and neither GUI1 or 2 add to any pass that wants all objects
	if (pf & PassFlagTypes::AllObjects && 
		!((rc->renderType & PassFlagTypes::GUI1 ) || (rc->renderType &  PassFlagTypes::GUI2)))
	{
		//std::cout << "Added to an AllObjects Pass, pass name " << pd->name << "\n";
	}
	else if ((pf & PassFlagTypes::OpaqueObjects) && (rc->renderType & PassFlagTypes::OpaqueObjects))
	{
		//std::cout << "Added to an Opaque Pass, pass name " << pd->name << "\n";
	}
	else if (pf & PassFlagTypes::Transparent && rc->renderType & PassFlagTypes::Transparent)
	{
		//std::cout << "Added to an Transparent Pass, pass name " << pd->name << "\n";
	}
	else if (pf & PassFlagTypes::Reflective && rc->renderType & PassFlagTypes::Reflective)
	{
		//std::cout << "Added to an reflective Pass, pass name " << pd->name << "\n";
	}
	else if (pf & PassFlagTypes::GUI1 && rc->renderType & PassFlagTypes::GUI1)
	{
		//std::cout << "Added to an GUI1 Pass, pass name " << pd->name << "\n";
		GUI1NeedsSort = true;
	}
	else if (pf & PassFlagTypes::GUI2 && rc->renderType & PassFlagTypes::GUI2)
	{
		//std::cout << "Added to an GUI2 Pass, pass name " << pd->name << "\n";
		GUI2NeedsSort = true;
	}
	else
	{
		return;
	}


	//For every shader type already in the pass data, check if this is the shader in use. If not already featured, add it to the passdata's shader list
	ShaderData* correctShader = nullptr;
	for each (ShaderData * sd in pd->contents)
	{
		//
		if (rc->getShader() == sd->commonPart)
		{
			//This is the right shader!
			correctShader = sd;
			break;
		}
	}

	if (correctShader != nullptr)
	{
		//The shader is featured this pass! Nothing to be done :)
	}
	else
	{
		//The shader isn't featured in this pass yet!
		correctShader = new ShaderData();
		correctShader->commonPart = rc->getShader();
		pd->contents.push_back(correctShader);
		std::cout << "New Shader!\n";
	}

	//If shader is instanced, add it anyway, will never be used in a regular pass but needed for shadows etc
	addToShaderData(correctShader, go, rc);
}

void GLRenderer::addToShaderData(ShaderData* sd, GameObject* go, RenderConditions* rc)
{
	if (sd->commonPart->getInstancedBool())
	{
		Material* imat = nullptr;
		//Instanced ShaderDatas have one material. If this doesn't exist yet, create it!
		if (sd->contents.size() == 0)
		{
			sd->contents.push_back(new MaterialData());
			imat = VertexDataManager::getInstance()->getMaterialForShaderType(sd->commonPart);
			sd->contents[0]->commonPart = imat;
		}
		else
		{
			imat = sd->contents[0]->commonPart;
		}

		
		imat->addTexture(rc->getMaterial(0));

		//Mat is the materialData for this
		MaterialData* mat = sd->contents[0];

		for(int meshCount = 0; meshCount < rc->getModel()->getMeshCount(); meshCount++)

			addToMaterialData(mat, go, rc, meshCount);

		return;
	}



	//For every material featured in the shader data, check if this is the correct material, and add to it. If the materiala is not found, add a new material data
	if (rc->getModel() == nullptr)
	{
		std::cout << "Model was null, object name " << go->name << std::endl;
		return;
	}

	//each mesh can have a different material
	for (int meshCount = 0; meshCount < rc->getModel()->getMeshCount(); meshCount++)
	{
		if (rc->getMaterial(meshCount) == nullptr)
		{
			std::cout << "Material was null, object name " << go->name << " mesh index " << meshCount << std::endl;
			continue;
		}
		MaterialData* correctMaterial = nullptr;

		for each (MaterialData * md in sd->contents)
		{
			//
			if (rc->getMaterial(meshCount) == md->commonPart)
			{
				//This is the right Material!
				correctMaterial = md;
				break;
			}
		}

		if (correctMaterial != nullptr)
		{
			//The Material is featured this pass! Nothing to be done :)
		}
		else
		{
			//The shader isn't featured in this pass yet!
			correctMaterial = new MaterialData();
			correctMaterial->commonPart = rc->getMaterial(meshCount);
			sd->contents.push_back(correctMaterial);
		}

		addToMaterialData(correctMaterial, go, rc, meshCount);
	}
}

void GLRenderer::addToMaterialData(MaterialData* md, GameObject* go, RenderConditions* rc, int meshIndex)
{
	//For every model featured in the shader data, check if this is the correct model, and add to it. If the model is not found, add a new model data
	ModelData* correctModel = nullptr;

	for each (ModelData * md in md->contents)
	{
		//
		if (rc->getModel()->getMeshByIndex(meshIndex) == md->commonPart)
		{
			//This is the right model!
			correctModel = md;
			break;
		}
	}

	if (correctModel != nullptr)
	{
		//The model is featured this pass! Nothing to be done :)
	}
	else
	{
		//The model isn't featured in this pass yet!
		correctModel = new ModelData();
		correctModel->commonPart = rc->getModel()->getMeshByIndex(meshIndex);
		md->contents.push_back(correctModel);
	}

	addToModelData(correctModel, go);
}

void GLRenderer::addToModelData(ModelData* md, GameObject* go)
{
	md->contents.push_back(go);
	passesAddedTo++;
}

void GLRenderer::render()
{
	VertexDataManager::getInstance()->flushBuffers();


	//Release the shaders
	ShaderManager::singleton()->beginFrame();

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	if (thisConfig.debugSettings->wireFrame)

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//Render all the crap that various FBOs want 
	renderPass(FBOPasses, true);

	//Prepare for shadow rendering

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	if (thisConfig.debugSettings->wireFrame)

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	renderShadows();

	//Turn both of those back on
	glEnable(GL_CULL_FACE);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//Shader begin frame - lights!
	ShaderManager::singleton()->beginFrame();

	//Bind the multi sampling FBO
	msFBO->bind();

	//Disable blend - no need  for opaque pass
	glDisable(GL_BLEND);

	if (thisConfig.debugSettings->wireFrame)

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//Do that pass
	renderPassData(opaquePass, 0, false);


	//Enable blend and define type
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Render the transparent pass
	renderPassData(transparentPass, 0, false);

	//Disable depth test for GUI
	glDisable(GL_DEPTH_TEST);

	if (thisConfig.debugSettings->wireFrame)

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//Render first
	renderPassData(GUI1, 0, false);

	//Unbind the ms FBO
	msFBO->unBind();

	//Blit to the post processing FBO
	msFBO->blitTo(ppFBO);

	//Do the post processing
	doPostProcessing();

	//Render second GUI passe
	renderPassData(GUI2, 0, false);

	//Disable BLend
	glDisable(GL_BLEND);

	//Draw the debug centre
	DebugCentre::singleton()->draw();
}

void GLRenderer::doPostProcessing()
{
	//THIS is inefficient as hell
	for (int count = 0; count < auxPPFBOs.size(); count++)
	{
		auxPPFBOs[count]->bind();

		auxPPFBOs[count]->shader->update();

		ppRects[count]->draw(postProcessRenderSettings);

		auxPPFBOs[count]->unBind();
	}

	finalRender->draw(postProcessRenderSettings);
}

void GLRenderer::renderPass(RenderData * rd, bool binding)
{
	for each (PassData * pd in rd->contents)
	{
		for (int count = 0; count < pd->commonPart->getTotalPasses(); count++)
		{
			renderPassData(pd, count, binding);
		}
	}
}

void GLRenderer::renderPassData(PassData *pd, int i, bool binding )
{
	currentPassData = pd;
	pd->commonPart->update();

	if (binding) {
		pd->commonPart->bindFBOByIndex(i);
		pd->commonPart->prepareToRender();
	}

	if (rs == nullptr)
		rs = new RenderSettings(pd->commonPart->getCameraAngleRequiredByIndex(i), pd->commonPart->getClipPlaneUseBoolByIndex(i), pd->commonPart->getClipPlaneByIndex(i));
	else
		rs;
		//RS has been made by the shadow renderering method

	for each (ShaderData * sd in pd->contents)
	{
		renderShaderData(sd);
	}

	if (binding) {
		pd->commonPart->unBind();
		pd->commonPart->finishRender();
	}

	delete rs;
	rs = nullptr;
}

void GLRenderer::renderShaderData(ShaderData* sd)
{
	if (shaderOverride == nullptr)
		currentShader = sd->commonPart;
	else
		currentShader = shaderOverride;

	glUseProgram(currentShader->getProgramID());

	if (!currentShader->getInstancedBool())
	{
		currentShader->setTextures();

		currentShader->prepareToRender(rs);

		if (currentShader == thisConfig.textShaders)
		{
			for each (GameObject * go  in sd->contents[0]->contents[0]->contents)
			{
				go->draw();
			}
		}
		else
		{

			for each (MaterialData * md in sd->contents)
			{
				renderMaterialData(md);
			}
		}
	}
	else
	{
		renderInstanced(sd);
	}
}

void GLRenderer::renderMaterialData(MaterialData* Md)
{
	if (rs->materialsOn) {
		currentShader->setNormalBool(Md->commonPart->useNormal);

		Md->commonPart->Bind();

		currentShader->setInvertY(Md->commonPart->invertY);
	}

	currentShader->setLighting(Md->commonPart->shininess, currentScene->currLighting);

	for each (ModelData * md in Md->contents)
	{
		renderModelData(md);
	}
}

void GLRenderer::renderModelData(ModelData* md)
{
		renderNormal(md);
}

void GLRenderer::renderNormal(ModelData * md)
{
	int indicesLength = md->commonPart->getElementsCount();

	bindVAO(md->commonPart->getVAO());

	for each (GameObject * go in md->contents)
	{
		currentShader->prepareToRender(go);

		glDrawElementsBaseVertex(currentShader->getPrimitiveType(), indicesLength, GL_UNSIGNED_INT, (void*)(md->commonPart->getElementsOffset() * sizeof(GLuint)), md->commonPart->getBaseVertexOffset());
	}
}

void GLRenderer::renderInstanced(ShaderData* sd)
{
	currentShader->setTextures();

	currentShader->prepareToRender(rs);

	currentShader->setLighting(20, currentScene->currLighting);

	bindVAO(VertexDataManager::getInstance()->getVAOForShaderType(currentShader));
	drawCount = 0;

	//In instanced shaders, there will only ever be one material, effectively skipping this layer
	for (MaterialData* md : sd->contents)
	{

		md->commonPart->Bind();

		for (ModelData* modelData : md->contents)
		{
			int indicesLength = modelData->commonPart->getElementsCount();

			DrawElementsIndirectCommand drawCmd;

			int instances = VertexDataManager::getInstance()->getInstanceCount(currentShader, modelData->commonPart);

			VertexDataManager* vdm = VertexDataManager::getInstance();

			//Indices Count
			drawCmd.elementsCount = indicesLength;
			//How many versions of this mesh?
			//drawCmd.instanceCount = md->contents.size();
			drawCmd.instanceCount = instances;
			//Start from which index?
			drawCmd.firstIndex = modelData->commonPart->getElementsOffset();
			//Add what to each index?
			drawCmd.baseVertex = modelData->commonPart->getBaseVertexOffset();	//1 instance
			//Which instance is this?
			drawCmd.baseInstance = VertexDataManager::getInstance()->getBaseInstance(currentShader, modelData->commonPart);

			drawCommands[drawCount] = drawCmd;
			drawCount++;
		}
	}

	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, indirectBuffer);
	glBufferData(GL_DRAW_INDIRECT_BUFFER, sizeof(drawCommands), drawCommands, GL_DYNAMIC_DRAW);

	glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, (GLvoid*)0, drawCount, 0);
}

void GLRenderer::renderShadows()
{
	shaderOverride = ShaderManager::singleton()->getShader<ShadowShaders*>();

	for each (auto lightPair in shadowData)
	{
		FBOGroup * fbo = lightPair.second->commonPart;

		fbo->update();



		ShadowBox * sBox = gameCamera->getShadowBox();

		sBox->update(lightPair.first);

		lightPair.first->updateLightViewMatrix(sBox->getCenter());

		lightPair.first->projMatrix = sBox->getOrthoProjectionMatrix();

		rs = new RenderSettings(lightPair.first->getLightViewMatrix(), false, glm::vec4(0));

		rs->projection = lightPair.first->projMatrix;

		rs->materialsOn = false;

		renderPassData(lightPair.second, 0, true);
	}

	shaderOverride = nullptr;
}

void GLRenderer::checkForLights()
{
	//Update thisConfig
	thisConfig = Game::singleton()->getConfig();

	if (thisConfig.currLighting == nullptr)
	{
		std::cout << "Current Lighting State is nullptr\n";
		return;
	}

	//Check if all lights are included in this system
	for(int count = 0; count < thisConfig.currLighting->numberOfLights; count++)
	{
		Light* light = thisConfig.currLighting->lights[count];
		if (light->castShadows)
		{
			if (shadowData.find(light) == shadowData.end())
			{
				light->setupShadows();

				PassData* pd = new PassData();

				pd->commonPart = light->shadowFBOGroup;



				addObjectsToPassDataFBO(pd, currentScene);

				shadowData.insert(std::make_pair(light, pd));
			}
			else
			{
				//This light is already included, no need here
			}
		}
	}


}
