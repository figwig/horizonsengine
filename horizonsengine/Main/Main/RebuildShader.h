#pragma once
#include "GlobalBehaviour.h"
#include "Shader.h"
#include "Game.h"

class RebuildShader : public GlobalBehaviour
{
public:
	RebuildShader(Shader* shader): shader(shader){}
	~RebuildShader() {}

	void update(double x)
	{
		if (timer >= 0.0)
		{
			timer -= x;
			return;
		}

		if(target->getKeys() & k)
		{ 
			timer = 200;
			shader->rebuild();
		}
	}
private:
	double timer = 0.0;

	Game* game;

	Keystate k = Keys::F7;

	Shader* shader;

};