#pragma once
#include "WorldGUI.h"
#include "Text.h"

class FloatingHealthBarUpdateBehaviour;

class FloatingHealthBar :public Object3D
	
{
public:
	FloatingHealthBar(GLuint barTex, GLuint progressTex, glm::vec2 res, float portionX, float portionY, bool facingCamera = false);
	~FloatingHealthBar();

	void update(double);
	void draw(RenderSettings*);

	void setup(float);
	void setProgress(float x);

protected:

	friend class FloatingHealthBarUpdateBehaviour;
	GLuint barTex = 0;
	GLuint progressTex = 0;
	float progress = 50.0f;
	
	glm::vec2 barRes;
	glm::vec2 progressRes;

	GLuint barVAO;
	GLuint progressVAO;

	bool first = true;

	WorldGUI * barGUI;
	WorldGUI * progressGUI;

	TextObject* text;

	float progWidth; 

	float maxProgress;

	bool faceCamera;
};

class FloatingHealthBarUpdateBehaviour : public PrivateBehaviour {
public: 
	FloatingHealthBarUpdateBehaviour(FloatingHealthBar* fhb) :fhb(fhb) {}
	~FloatingHealthBarUpdateBehaviour() {}


	void update(double d);
private:
	FloatingHealthBar* fhb = nullptr;
};