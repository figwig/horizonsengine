#pragma once
#include "ImageRect.h"
#include "Shader.h"
class PostProcessRect :
	public Panel
{
public:
	PostProcessRect(GLuint x);
	~PostProcessRect();

	void draw(RenderSettings*);
	void setShader(Shader * x) { postProcessingShader = x; }
	Shader *  getShader() { return postProcessingShader; }
private:
	Shader * postProcessingShader;
};

