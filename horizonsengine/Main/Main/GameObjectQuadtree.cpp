#include "GameObjectQuadtree.h"
#include "Helpers.h"

GameObjectQuadtree::GameObjectQuadtree(Bounds2DXZ b, int level, GameObjectQuadtree * p) : bounds(b)
{
	thisLevel = level;
	split = false;
	parent = p;
}


GameObjectQuadtree::~GameObjectQuadtree()
{
	clear();
}



void GameObjectQuadtree::addGameObject(GameObject *)
{
}

void GameObjectQuadtree::splitIntoNodes()
{
	split = true;
	//too many triangles, split
	double subWidth = bounds.width / 2;
	double subHeight = bounds.height / 2;
	double x = bounds.bottomLeft.x;
	double y = bounds.bottomLeft.y;

	nodes[0] = new GameObjectQuadtree(Bounds2DXZ(glm::vec2(x, y), glm::vec2(x + subWidth, y + subHeight)), thisLevel + 1, this);

	nodes[1] = new GameObjectQuadtree(Bounds2DXZ(glm::vec2(x + subWidth, y), glm::vec2(x + bounds.width, y + subHeight)), thisLevel + 1, this);

	nodes[2] = new GameObjectQuadtree(Bounds2DXZ(glm::vec2(x, y + subHeight), glm::vec2(x + subWidth, y + bounds.height)), thisLevel + 1, this);

	nodes[3] = new GameObjectQuadtree(Bounds2DXZ(glm::vec2(x + subWidth, y + subHeight), glm::vec2(x + bounds.width, y + bounds.height)), thisLevel + 1, this);
}

void GameObjectQuadtree::moveAllObjectsToNodes()
{
	objectsToRetain.clear();

	for (int count = 0; count < objects.size(); count++)
	{
		tryMoveObjectToNode(objects[count]);
	}

	objects.clear();
	objectCount = 0;

	for (int count = 0; count < objectsToRetain.size(); count++)
	{
		objects.push_back(objectsToRetain[count]);
		objectCount++;
	}
}

void GameObjectQuadtree::tryMoveObjectToNode(GameObject* obj)
{
	Extents exts = obj->getWorldExtents();

	if (exts.centre.x >(bounds.width / 2) + bounds.bottomLeft.x)
	{
		if (exts.centre.z > (bounds.height / 2) + bounds.bottomLeft.y)
		{
			//std::cout << "3 \n";
			if (!extentsExceedsBounds(exts, nodes[3]->getBounds()))
			{
				nodes[3]->addGameObject(obj);
				return;
			}
		}
		else if (exts.centre.z <= (bounds.height / 2) + bounds.bottomLeft.y)
		{
			//std::cout << "1 \n";
			if (!extentsExceedsBounds(exts, nodes[1]->getBounds()))
			{
				nodes[1]->addGameObject(obj);
				return;
			}
		}
	}
	else if (exts.centre.x <= (bounds.width / 2) + bounds.bottomLeft.x)
	{
		if (exts.centre.z > (bounds.height / 2) + bounds.bottomLeft.y)
		{
			//std::cout << "2 \n";
			if (!extentsExceedsBounds(exts, nodes[2]->getBounds()))
			{
				nodes[2]->addGameObject(obj);
				return;
			}
		}
		else if (exts.centre.z <= (bounds.height / 2) + bounds.bottomLeft.y)
		{
			//std::cout << "0 \n";
			if (!extentsExceedsBounds(exts, nodes[0]->getBounds()))
			{
				nodes[0]->addGameObject(obj);
				return;
			}
		}
	}
	objectsToRetain.push_back(obj);
}

void GameObjectQuadtree::clear()
{
	for (int count = 0; count < 4; count++)
	{
		if (nodes[count] != nullptr)
		{
			delete nodes[count];
			nodes[count] = nullptr;
		}
	}
}