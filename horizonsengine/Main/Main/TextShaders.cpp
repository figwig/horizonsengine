#include "TextShaders.h"
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#include "Text.h"
#include "TextRenderingComponent.h"

TextShaders* TextShaders::thisPointer = nullptr;

TextShaders::TextShaders()
{
	programID = setupShaders(vertexFilename, fragFilename);
	setup();
}


TextShaders::~TextShaders()
{
}

void TextShaders::setProj(glm::mat4 p)
{
	glUniformMatrix4fv(locProj, 1,GL_FALSE , glm::value_ptr(p));
}

TextShaders* TextShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new TextShaders();
	}
	return thisPointer;
}

void TextShaders::setup()
{
	locProj = glGetUniformLocation(programID, "projection");
	locTextColour = glGetUniformLocation(programID, "textColour");
}

void TextShaders::setColour(glm::vec3 colour)
{
	glUniform3fv(locTextColour, 1, glm::value_ptr(colour));
}

void TextShaders::prepareToRender(GameObject* go)
{
	TextRenderingComponent* trc = go->getComponent<TextRenderingComponent>();

	setColour(trc->colour);
}

void TextShaders::prepareToRender(RenderSettings* rs)
{

}