#include "MouseRaycaster.h"
#include "PhysicsHandler.h"

MouseRaycaster::MouseRaycaster()
{
	dd = Game::singleton()->getDisplayDetails();
	watchLoc = DebugCentre::singleton()->addWatch(glm::vec3(0), "RaycastPos");
	mouseLoc = DebugCentre::singleton()->addWatch(glm::vec3(0), "MousePos");
}

MouseRaycaster::~MouseRaycaster()
{
	
}

void MouseRaycaster::mouseMovePos(float x, float y)
{
	lastX = x;
	lastY = y;

	DebugCentre::singleton()->updateWatch(mouseLoc, glm::vec3(lastX, lastY, 0));
}



glm::vec3 MouseRaycaster::getMousePosWorld()
{
	Camera* cam = Game::singleton()->getCamera();
	PhysicsHandler* phys = Game::singleton()->getPhysicsEngine();
	float x = (2.0f * lastX) / dd->width - 1.0f;
	float y = 1.0f - (2.0f * lastY) / dd->height;
	float z = 1.0f;
	glm::vec3 ray_nds = glm::vec3(x, y, z);

	glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);
	glm::vec4 ray_eye = glm::inverse(cam->getPersp()) * ray_clip;

	ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);

	glm::mat4 camMat = cam->getCameraMatNoPersp();
	glm::vec4 ray_wor = glm::inverse(camMat) * ray_eye;
	// don't forget to normalise the vector at some point
	ray_wor = glm::normalize(ray_wor);

	ray_wor *= 1000.0f;


	RayCastResult res = Game::singleton()->getPhysicsEngine()->castRay(cam->getCameraPos(), cam->getCameraPos() + glm::vec3(ray_wor));

	DebugCentre::singleton()->updateWatch(watchLoc, res.hitPos);

	return res.hitPos;
}


GameObject* MouseRaycaster::getObjectUnderMouse()
{
	Camera* cam = Game::singleton()->getCamera();
	PhysicsHandler* phys = Game::singleton()->getPhysicsEngine();
	float x = (2.0f * lastX) / dd->width - 1.0f;
	float y = 1.0f - (2.0f * lastY) / dd->height;
	float z = 1.0f;
	glm::vec3 ray_nds = glm::vec3(x, y, z);

	glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);
	glm::vec4 ray_eye = glm::inverse(cam->getPersp()) * ray_clip;

	ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);

	glm::mat4 camMat = cam->getCameraMatNoPersp();
	glm::vec4 ray_wor = glm::inverse(camMat) * ray_eye;
	// don't forget to normalise the vector at some point
	ray_wor = glm::normalize(ray_wor);

	ray_wor *= 1000.0f;


	RayCastResult res = Game::singleton()->getPhysicsEngine()->castRay(cam->getCameraPos(), cam->getCameraPos() + glm::vec3(ray_wor));

	if (res.hit)
	{
		return res.object;
	}
	return nullptr;

}