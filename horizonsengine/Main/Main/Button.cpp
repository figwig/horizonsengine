#include "Button.h"
#include "Game.h"
#include "GrowAnimation.h"
#include "AnimationComponent.h"

Button::Button(Texture* t, glm::vec3 scale,  ButtonAction * buttonA, std::string label): Panel(t)
{
	setScale(scale);

	setup();

	buttonAction = buttonA;

	name = label;
}


void Button::setup()
{
	//Put here as ImageRect's setup can be called in constructor. Rethinking this could be a good idea
	animator = new AnimationComponent(this);

	addComponent(animator);

	zoomAnimation = new GrowAnimation(100.0f, transform->getRelativeScale().x, 0.007f, this);
	revAnimation = new GrowAnimation(100.0f, transform->getRelativeScale().x + 0.007f, 0.007f, this, true);
	locAnimation = animator->addAnimation(zoomAnimation);
	locRevAnimation = animator->addAnimation(revAnimation);

	RenderConditions* rc = getComponent<RenderConditions>();

	rc->tintColor = glm::vec4(1, 0, 0, 1);
	rc->tintStrength = 0.4f;

}

Button::~Button()
{
	if (buttonAction != nullptr)
	{
		delete buttonAction;
	}
}


void Button::OnClick(int type,float x, float y)
{
	if (mouseOver&&buttonAction!= nullptr)
	{
		buttonAction->onClick(type, x, y);
	}
}

void Button::mouseMovePos(float x, float y)
{
	if (transform->isMouseOver(x, y) && !mouseOver)
	{
		mouseOver = true;
		animator->setAnimation(locAnimation);
	}
	if (mouseOver && !transform->isMouseOver(x, y))
	{
		mouseOver = false;
		animator->setAnimation(locRevAnimation);
	}
}