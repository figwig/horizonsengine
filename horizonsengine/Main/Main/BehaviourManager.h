#pragma once
#include <vector>
#include <map>
#include "JobCentre.h"

class Behaviour;
class PrivateBehaviour;
class PublicBehaviour;
class GlobalBehaviour;
class Game;
class BehaviourManager
{
public:
	static BehaviourManager* singleton();

	void newBehaviour(Behaviour*);
	void removeBehaviour(Behaviour*);
private:
	friend class Behaviour;
	friend class Game;
	void update(double delta);

	void sortBehaviours();

	static BehaviourManager* thisPointer;
	BehaviourManager();
	~BehaviourManager();

	std::vector<Behaviour*> behaviours;
	std::vector<Behaviour*> containedBehaviours;
	std::vector<JobReport*> containedBehaviourReports;

	JobCentre* jc = nullptr;
};

