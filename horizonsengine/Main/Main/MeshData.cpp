#include "VAOData.h"
#include "MaterialHandler.h"


MeshData::MeshData(GLuint VAO, GLuint newTexture, GLuint newSpecTexture, GLuint newNormalMap, float *newPoints, float *newNormals, float* newTangents, float *newBitangents, float *newTextureCoords,GLuint * newIndices, int vertsCount, int indiciesC)
{
	defaultVAO = VAO;
	texture = newTexture;
	specTexture = newSpecTexture;
	normalMap = newNormalMap;

	//Fill the dynamically allocated arrays with the numbers from the buffers. MeshData keeps the raw data so that more efficient VAOs can be constructed when needed
	for (int i = 0; i < vertsCount * 3; i++)
	{
		pointsV.push_back(newPoints[i]);

		normalsV.push_back(newNormals[i]);
	}

	//and the text coords
	for (int i = 0; i < vertsCount * 2; i++)
	{
		textsV.push_back(newTextureCoords[i]);
	}

	//once for each vertex
	for (int i = 0; i < vertsCount * 4; i++)
	{
		bitangentsV.push_back(newBitangents[i]);
		tangentsV.push_back(newTangents[i]);

	}
	//Move the indices across
	for (int i = 0; i < indiciesC; i++)
	{
		indiciesV.push_back(newIndices[i]);
	}

	defaultMaterial = MaterialHandler::singleton()->makeMaterial(new Texture(newTexture, glm::vec2(0), ""), new Texture(newSpecTexture, glm::vec2(0), ""), new Texture(newNormalMap, glm::vec2(0), ""));
}

MeshData::MeshData(GLuint VAO,  float* newPoints, float* newNormals, float* newTangents, float* newBitangents, float* newTextureCoords, GLuint* newIndices, int vertsCount, int indiciesC)
{
	defaultVAO = VAO;

	//Fill the dynamically allocated arrays with the numbers from the buffers MeshData keeps the raw data so that more efficient VAOs can be constructed when needed
	for (int i = 0; i < vertsCount * 3; i++)
	{
		pointsV.push_back(newPoints[i]);
		normalsV.push_back(newNormals[i]);
	}

	//and the text coords
	for (int i = 0; i < vertsCount * 2; i++)
	{
		textsV.push_back(newTextureCoords[i]);
	}

	//once for each vertex
	for (int i = 0; i < vertsCount * 4; i++)
	{
		bitangentsV.push_back(newBitangents[i]);
		tangentsV.push_back(newTangents[i]);

	}
	//Move the indices across
	for (int i = 0; i < indiciesC; i++)
	{
		indiciesV.push_back( newIndices[i]);
	}

}

MeshData::MeshData(std::vector<glm::vec3> newPoints, std::vector<glm::vec3> newNormals, std::vector<glm::vec2> newTexts, std::vector<glm::vec4> newTans, std::vector<glm::vec4> newBitans, std::vector<GLuint> newIndices, int elementsCount)
{

	int count = newPoints.size();

	for each (glm::vec3 points in newPoints)
	{
		pointsV.push_back(points.x);
		pointsV.push_back(points.y);
		pointsV.push_back(points.z);
	}
	
	for each (glm::vec3 normal in newNormals)
	{
		normalsV.push_back(normal.x);
		normalsV.push_back(normal.y);
		normalsV.push_back(normal.z);
	}

	for each (glm::vec2 text in newTexts)
	{
		textsV.push_back(text.x);
		textsV.push_back(text.y);
	}

	for each (glm::vec4 tangent in newTans)
	{
		tangentsV.push_back(tangent.x);
		tangentsV.push_back(tangent.y);
		tangentsV.push_back(tangent.z);
		tangentsV.push_back(tangent.w);
	}
	
	for each (glm::vec4 bitangent in newBitans)
	{
		bitangentsV.push_back(bitangent.x);
		bitangentsV.push_back(bitangent.y);
		bitangentsV.push_back(bitangent.z);
		bitangentsV.push_back(bitangent.w);
	}

	indiciesV = newIndices;
}

glm::vec3 MeshData::getVertexByIndex(int i)
{
	int index = indiciesV[i];

	return glm::vec3(pointsV[index * 3], pointsV[(index * 3) + 1], pointsV[(index * 3) + 2]);
}

void MeshData::newData(std::vector<float> points, std::vector<float> normals, std::vector<float> tangents, std::vector<float> bitangents, std::vector<float> texts, std::vector<GLuint> indicies)
{
	pointsV = points;
	normalsV = normals;
	textsV = texts;
	tangentsV = tangents;
	bitangentsV = bitangents;
	indiciesV = indicies;
}

void MeshData::add(MeshData* other)
{
	pointsV.insert(pointsV.end(), other->getPointsV()->begin(), other->getPointsV()->end());

	normalsV.insert(normalsV.end(), other->getNormalsV()->begin(), other->getNormalsV()->end());

	tangentsV.insert(tangentsV.end(), other->getTangentsV()->begin(), other->getTangentsV()->end());

	bitangentsV.insert(bitangentsV.end(), other->getBitangentsV()->begin(), other->getBitangentsV()->end());

	textsV.insert(textsV.end(), other->getTextsV()->begin(), other->getTextsV()->end());

	indiciesV.insert(indiciesV.end(), other->getIndiciesV()->begin(), other->getIndiciesV()->end());
}
