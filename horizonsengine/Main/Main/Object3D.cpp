#include "Object3D.h"
#include "Game.h"
#include "texture_loader.h"
#include "tiny_obj_loader.h"
#include <FreeImage\FreeImagePlus.h>
#include <iostream>
#include <stdio.h>
#include "VAOData.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#include "Light.h"
#include "DefaultShaders.h"

///<Summary>
///An Object3D is defined as an object rendered in 3d space, even if itself is only 2 dimensional
///every object3d must have an update call that calls Object3D::update and Object3D::lateupdate
///</Summary>

Object3D::Object3D()
{
	setup();
}




void Object3D::drawNormals(glm::mat4 camera)
{
	//glUseProgram(thisConfig.normalShaders->getProgramID());

	//thisConfig.normalShaders->setProj(thisConfig.currLighting->persp);

	//thisConfig.normalShaders->setMats(T,R,S,thisConfig.currLighting->cameraWithOutPersp);

	//for (int count = 0; count < renderConditions->model->getMeshCount(); count++)
	//{
	//	glBindVertexArray(renderConditions->model->getVAOByIndex(count));
	//	glDrawArrays(GL_TRIANGLES, 0, renderConditions->model->getElementsCountByIndex(count));
	//}
}

void Object3D::drawTangents(glm::mat4 camera)
{
	//glUseProgram(thisConfig.tanShaders->getProgramID());

	//thisConfig.tanShaders->setProj(thisConfig.currLighting->persp);

	//thisConfig.tanShaders->setMats(T, R, S, thisConfig.currLighting->cameraWithOutPersp);

	//for (int count = 0; count < renderConditions->model->getMeshCount(); count++)
	//{
	//	glActiveTexture(GL_TEXTURE0);
	//	glBindTexture(GL_TEXTURE_2D, renderConditions->model->getNormalMapByIndex(count));

	//	glBindVertexArray(renderConditions->model->getVAOByIndex(count));
	//	glDrawArrays(GL_TRIANGLES, 0, renderConditions->model->getElementsCountByIndex(count));
	//}

}


