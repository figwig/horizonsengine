#include "NormalDrawingShaders.h"


NormalDrawingShaders* NormalDrawingShaders::thisPointer = nullptr;

NormalDrawingShaders::NormalDrawingShaders()
{
	programID = setupShadersGeometry(vertFilename, geometryFilename, fragFilename);
	setup();
}


NormalDrawingShaders::~NormalDrawingShaders()
{
}

NormalDrawingShaders* NormalDrawingShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new NormalDrawingShaders();
	}
	return thisPointer;
}

void NormalDrawingShaders::setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S, glm::mat4 C)
{
	glUniformMatrix4fv(locTn, 1, GL_FALSE, glm::value_ptr(T));
	glUniformMatrix4fv(locRn, 1, GL_FALSE, (GLfloat*)&R);
	glUniformMatrix4fv(locSn, 1, GL_FALSE, (GLfloat*)&S);
	glUniformMatrix4fv(locCn, 1, GL_FALSE, (GLfloat*)&C);

}

void NormalDrawingShaders::setup()
{
	locTn = glGetUniformLocation(programID, "T");
	locSn = glGetUniformLocation(programID, "S");
	locRn = glGetUniformLocation(programID, "R");
	locCn = glGetUniformLocation(programID, "camera");

	nLocProj = glGetUniformLocation(programID, "projection");
}


void NormalDrawingShaders::setProj(glm::mat4 p)
{
	glUniformMatrix4fv(nLocProj, 1, GL_FALSE, (GLfloat*)&p);
}