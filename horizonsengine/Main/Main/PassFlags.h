#pragma once
#include <cstdint>

typedef  uint32_t PassFlags;

enum PassFlagTypes
{
	AllObjects = 0b1,
	OpaqueObjects = 0b10,
	Transparent = 0b100,
	Reflective = 0b1000,
	GUI1 = 0b10000,
	GUI2 = 0b100000,
	Text = 0b1000000
};
