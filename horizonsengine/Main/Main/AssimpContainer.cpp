#include "AssimpContainer.h"
#include "VAOData.h"
#include "VertexDataManager.h"
#include "Material.h"
#include "TextureExaminer.h"
#include "MaterialHandler.h"
#include "Helpers.h"
#include <string>
Model*  AssimpContainer::readFile(std::string pFile)
{
	meshDatas.clear();

	// Create an instance of the Importer class
	Assimp::Importer importer;
	// And have it read the given file with some example postprocessing
	// Usually - if speed is not the most important aspect for you - you'll 
	// propably to request more postprocessing than we do in this example.
	const aiScene *scene = importer.ReadFile(pFile,
		aiProcess_CalcTangentSpace | aiProcess_GenNormals  | aiProcess_Triangulate);

	// If the import failed, report it
	if (!scene)
	{
		std::cout << importer.GetErrorString() << "\n";
		return false;
	}


	lookThroughNode(scene->mRootNode, glm::mat4(1), scene);

	// Now we can access the file's contents. 
	//for (int meshCount = 0; meshCount < scene->mNumMeshes; meshCount++)
	//{
	//	dealWithMesh(scene->mMeshes[meshCount], scene);
	//}

	VertexDataManager* vaoMaker = VertexDataManager::getInstance();

	Model* model = new Model();

	int count = 0;

	for each(MeshData * md in meshDatas)
	{
		GLuint VAO = vaoMaker->addMeshToVAO(md, true);

		md->setVAO(VAO);

		model->addMesh(md);

		md->name = pFile + " " + std::to_string(count);

		count++;
	}


	return model;
}

void AssimpContainer::lookThroughNode(aiNode* node,glm::mat4 carryThroughTrans, const aiScene* scene)
{
	for(int i = 0; i < node->mNumChildren; i++)
	{
		glm::mat4 x = convert(node->mTransformation);
		lookThroughNode(node->mChildren[i],  x* carryThroughTrans, scene);
	}
	for (int i = 0; i < node->mNumMeshes; i++)
	{
		dealWithMesh(scene->mMeshes[node->mMeshes[i]], scene, carryThroughTrans);
	}
}

void AssimpContainer::dealWithMesh(aiMesh* mesh, const aiScene* scene, glm::mat4 trans)
{
	std::vector<glm::vec3> points;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texts;
	std::vector<glm::vec4> tangents;
	std::vector<glm::vec4> bitangents;
	std::vector<GLuint> indicies;

	glm::vec3 minPoint = glm::vec3(10000000);
	glm::vec3 maxPoint = glm::vec3(-10000000);
	glm::vec3 averageTot = glm::vec3(0);

	for (int vCount = 0; vCount < mesh->mNumVertices; vCount++)
	{
		glm::vec3 v = trans * glm::vec4(mesh->mVertices[vCount].x, mesh->mVertices[vCount].y, mesh->mVertices[vCount].z, 0.0);

		averageTot += v;

		points.push_back(v);

		if (v.x > maxPoint.x) maxPoint.x = v.x;
		if (v.x < minPoint.x) minPoint.x = v.x;
		if (v.y > maxPoint.y) maxPoint.y = v.y;
		if (v.y < minPoint.y) minPoint.y = v.y;
		if (v.z > maxPoint.z) maxPoint.z = v.z;
		if (v.z < minPoint.z) minPoint.z = v.z;


		normals.push_back(glm::vec3(mesh->mNormals[vCount].x, mesh->mNormals[vCount].y, mesh->mNormals[vCount].z));

		if (mesh->mTextureCoords[0])
		{
			texts.push_back(glm::vec2(mesh->mTextureCoords[0][vCount].x, mesh->mTextureCoords[0][vCount].y));


			tangents.push_back(glm::vec4(mesh->mTangents[vCount].x, mesh->mTangents[vCount].y, mesh->mTangents[vCount].z, 0.0f));

			bitangents.push_back(glm::vec4(mesh->mBitangents[vCount].x, mesh->mBitangents[vCount].y, mesh->mBitangents[vCount].z, 0.0f));
		}
	}

	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++)
			indicies.push_back(face.mIndices[j]);
	}

	averageTot /= mesh->mNumVertices;

	glm::mat4 neu = glm::mat4(1);

	if (scene->mNumMeshes == 1) {
		//Centre the mesh on (0,0,0)
		for (int i = 0; i < points.size(); i++)
		{
			points[i] -= averageTot;
		}
	}

	MeshData* md = new MeshData(points, normals, texts, tangents, bitangents, indicies, mesh->mNumVertices);

	Extents e = Extents(minPoint, maxPoint);
	e.centre = averageTot;

	md->setExtents(e);

	aiMaterial* mat = scene->mMaterials[mesh->mMaterialIndex];

	aiString diffuse;
	aiString normal;
	aiString spec;

	mat->GetTexture(aiTextureType_DIFFUSE, 0, &diffuse);
	mat->GetTexture(aiTextureType_SPECULAR, 0, &spec);
	mat->GetTexture(aiTextureType_HEIGHT, 0, &normal);

	TextureExaminer::singleton()->setDirectory("");


	Texture* diff = TextureExaminer::singleton()->getTexture(diffuse.C_Str());
	Texture* specular = TextureExaminer::singleton()->getTexture(spec.C_Str());
	Texture* norm = TextureExaminer::singleton()->getNormalTexture(normal.C_Str());

	Material* material = MaterialHandler::singleton()->makeMaterial(diff, specular, norm);

	md->defaultMaterial = material;


	meshDatas.push_back(md);
}