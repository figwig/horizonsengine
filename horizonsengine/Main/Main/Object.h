#pragma once
#include "Component.h"
#include <vector>
#include <typeindex>
#include <map>
#include <cassert>

//Common Parent for any object that can contain a component. 

class Object
{
public:
	template<typename T,
		typename = std::enable_if_t<std::is_base_of_v<Component, T>>>
	void addComponent(T * component) 
	{
		components.insert(std::make_pair(std::type_index(typeid(T)), component));
	}




	template<typename CompType,
		typename = std::enable_if_t<std::is_base_of_v<Component, CompType>>>
	inline void removeComponent()
	{
		auto it = components.find(std::type_index(typeid(CompType)));

		if (it == components.end())
			return;

		components.erase(it->first);
	}


	template<typename CompType,
		typename = std::enable_if_t<std::is_base_of_v<Component, CompType>>>
	inline CompType* getComponent()
	{
		auto it = components.find(std::type_index(typeid(CompType)));

		if (it == components.end())
			return nullptr;
		return dynamic_cast<CompType*>(it->second);
	}


	Object() {}
	virtual ~Object()
	{
		for (auto c : components)
		{
			delete c.second;
		}
	}

protected:
	bool setupAlready = false;
	std::multimap<std::type_index, Component*> components;

	virtual void setup() {}
private:

};