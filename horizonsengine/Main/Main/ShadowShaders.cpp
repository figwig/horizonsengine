#include "ShadowShaders.h"
#include "GameObject.h"

ShadowShaders* ShadowShaders::thisPointer = nullptr;

ShadowShaders::ShadowShaders()
{
	programID = setupShaders("Shaders/vertex_shadow.shader", "Shaders/fragment_shadow.shader");
	setup();
}


ShadowShaders::~ShadowShaders()
{
}

void ShadowShaders::setMats(glm::mat4 p, glm::mat4 l, glm::mat4 T, glm::mat4 R, glm::mat4 S)
{
	setMats(p, l);
	setMats(T, R, S);
}

void ShadowShaders::setMats(glm::mat4 p, glm::mat4 l)
{
	glUniformMatrix4fv(locP, 1, GL_FALSE, (GLfloat*)& p);
	glUniformMatrix4fv(locL, 1, GL_FALSE, (GLfloat*)& l);
}

void ShadowShaders::setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S)
{
	glUniformMatrix4fv(locT, 1, GL_FALSE, (GLfloat*)& T);
	glUniformMatrix4fv(locR, 1, GL_FALSE, (GLfloat*)& R);
	glUniformMatrix4fv(locS, 1, GL_FALSE, (GLfloat*)& S);
}

ShadowShaders* ShadowShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new ShadowShaders();
	}
	return thisPointer;
}

void ShadowShaders::setup()
{
	locP = glGetUniformLocation(programID, "proj");
	locL = glGetUniformLocation(programID, "light");
	locT = glGetUniformLocation(programID, "T");
	locR = glGetUniformLocation(programID, "R");
	locS = glGetUniformLocation(programID, "S");

}

void ShadowShaders::prepareToRender(GameObject* go)
{
	setMats(go->getTransMat(), go->getRotMat(), go->getScaleMat());
}

void ShadowShaders::prepareToRender(RenderSettings* rs)
{
	setMats(rs->projection, rs->cameraMat);
}