#pragma once
#define NOMINMAX
#include <vector>
#include "Texture.h"
#include "ImageRect.h"
#include "Text.h"
#include "SceneRenderer.h"

class DebugCentre
{
public:

	static DebugCentre* singleton();
	void addDebugTexture(Texture* t);
	void removeDebugTexture(GLuint);
	void removeDebugTexture(Texture * t);

	void update(double);
	void draw();

	void addObject(GameObject *, std::string);
	int addWatch(int initialWatch, string name);
	int addWatch(float initialWatch, string name);
	int addWatch(glm::vec3 initialWatch, string name);

	void removeWatch(int location);

	void updateWatch(int location , float value);
	void updateWatch(int location, int value);
	void updateWatch(int location, glm::vec3 value);

	void init();
private:

	void addTextObject(TextObject*);
	static DebugCentre* thisPointer;
	DebugCentre();
	~DebugCentre();

	std::vector<Panel*> debugTextures;

	std::vector<TextObject*> textureTitles;

	bool open = false;
	bool fpsOpen = false;
	
	int readingOut = 0;

	TextObject* fps = nullptr;

	//This vector is a simple reference, not an ownership
	std::vector<std::pair<GameObject *, TextObject* >> trackedGameObjects;
	std::vector<TextObject* > printOuts;


	RenderSettings * rs;
	double inputTimer = 0;

	//This vector is reference, not ownership
	std::vector<std::pair<TextObject*, string>> watchList;


	double updateTicker = 0;

	SceneRenderer* sr = nullptr;
};

