#include "GameObject.h"
#include "JobCentre.h"


GameObject::GameObject()
{

	setup();
	setPos(glm::vec3(0));
	setScale(glm::vec3(1));
	setRot(glm::vec3(0));
}

GameObject::~GameObject()
{
	components;

	dead = true;
	if(parent!= nullptr)
		parent->removeChild(this);

	for (auto a : children)
	{
		a->setParent(nullptr);
	}

	for (int count = 0; count < MAX_BEHAVIOURS; count++)
	{

		if (privateBehaviours[count] != nullptr) {
			BehaviourManager::singleton()->removeBehaviour(privateBehaviours[count]);
			delete privateBehaviours[count];
		}
	}
}

void GameObject::removeChild(GameObject* go)
{
	for (int i = 0; i < children.size(); i++)
	{
		if (children[i] == go)
		{
			children.erase(children.begin() + i);
			return;
		}
	}
}

Extents GameObject::getWorldExtents()
{
	return e*(getTransMat() *getScaleMat());
}

Extents GameObject::getScaledExtents()
{
	Extents f = e * getScaleMat();
	return f;
}

glm::vec3 GameObject::getPos()
{
	return glm::vec3(T[3]);
}

glm::vec3 GameObject::getRot()
{
	return glm::vec3(rotX, rotY, rotZ);
}

glm::vec3 GameObject::getScale()
{
	glm::vec3 scale;
	glm::decompose(S, scale, glm::quat(), glm::vec3(), glm::vec3(), glm::vec4());
	return scale;
}

glm::mat4  GameObject::getRotMat()
{
	return R;
}

glm::mat4  GameObject::getTransMat()
{
	return T;
}

glm::mat4 GameObject::getScaleMat()
{
	return S;
}


void GameObject::setParent(GameObject* g)
{
	if (g != nullptr)
	{
		parent = g;
		g->children.push_back(this);
	}
	else
	{
		parent = nullptr;
	}
}

glm::mat4 GameObject::getModelMat()
{
	return modelMat;
}

void GameObject::setPos(glm::vec3 pos)
{
	T = glm::translate(glm::mat4(1), pos);

	invalidateMatricies();
}

void GameObject::setRot(glm::vec3 pos)
{

	R = glm::toMat4(
		glm::quat(glm::vec3(0.0f, pos.y, 0.0f))* 
		glm::quat(glm::vec3(pos.x, 0.0f, 0.0f))* 
		glm::quat(glm::vec3(0.0f, 0.0f, pos.z))
	);

	rotX = pos.x;
	rotY = pos.y;
	rotZ = pos.z;

	invalidateMatricies();
}

void GameObject::setRot(glm::quat q)
{
	R = glm::toMat4(q);
	glm::vec3 qV = glm::eulerAngles(q);
	rotX = qV.x;
	rotY = qV.y;
	rotZ = qV.z;
	invalidateMatricies();

}

void GameObject::setScale(glm::vec3 pos)
{
	S = glm::scale(glm::mat4(1), pos);

	invalidateMatricies();
}

void GameObject::invalidateMatricies()
{
	if (parent != nullptr)
	{
		modelMat = parent->getModelMat() * T * R * S;
	}
	else
	{
		modelMat = T * R * S;
	}

	if (callbackSet)
	{
		callOnModelMatChange();
	}

	for (GameObject* go : children)
	{
		go->invalidateMatricies();
	}


}
