#include "HideMouse.h"

#include "Game.h"

HideMouse::HideMouse()
{
}


HideMouse::~HideMouse()
{
}

void HideMouse::update(double x)
{
	Keystate keys = target->getKeys();

	if (timer >= 0.0) timer -= x;

	if (keys & Keys::F4 && timer <= 0.0)
	{
		hidden = !hidden;
		if (hidden)
		{
			glfwSetInputMode(target->currentWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			timer = 200.0;
		}
		else
		{
			glfwSetInputMode(target->currentWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			timer = 200.0;
		}
	}

	
}
