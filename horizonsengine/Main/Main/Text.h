#pragma once
#define NOMINMAX
#include "Object2D.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Config.h"
#include <map>
#include <vector>
#include "VAOData.h"

class TextRenderingComponent;
class TextMaterial;

class RenderCharacter
{
public:
	RenderCharacter(Character c) { ch = c;  }

	Character ch;

	int index = 0;
};




class TextObject :
	public GUIObject
{
public:
	TextObject(std::map<GLchar, Character> font);
	~TextObject();
	void update(double x);

	void draw();
	void draw(RenderSettings*) { draw(); }

	void sortVAO();

	void setText(std::string x) { text = x;  generateVAO(); generateVAO(); }

	void setFont(std::map<GLchar, Character> f) { font = f; }

	void setColour(glm::vec3 c);

	bool centred = false;

	void setScale(glm::vec3 x)
	{
		GUIObject::setScale(x);
		setText(text);
		setText(text);
	}

private:


	Model* m = nullptr;

	void generateVAO();

	std::string text = "Hello World";

	GLuint VBO = 0;
	GLuint VAO = 0;

	TextRenderingComponent* trc = nullptr;

	float yOffset = 20.0f;

	std::map<GLchar, Character> font; 


	static const int maxCharacters = 64;
	std::vector<GLfloat> points;

	std::vector<RenderCharacter> characters;
};

