#include "WorldGUIShader.h"

WorldGUIShader* WorldGUIShader::thisPointer = nullptr;

WorldGUIShader * WorldGUIShader::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new WorldGUIShader();
	}
	return thisPointer;
}

WorldGUIShader::WorldGUIShader()
{
	programID = setupShaders(vertFileName, fragFileName);
	setup();
}

WorldGUIShader::~WorldGUIShader()
{
}

void WorldGUIShader::setup()
{
	DefaultShaders::setup();
}
