#pragma once

#define CLOCK_PHYSICS 0
#define CLOCK_RENDER 1
#define CLOCK_UPDATE 2
#define CLOCK_OVERALL 3
#define CLOCK_FLUSH 4

class Clock
{
public:
	Clock();
	~Clock() {}
	void tick();
	float getDelta() { return delta; }
	float getAvgDelta() { return avgDelta; }
	void end();
	int getFPS() { return avgFPS; }

	void writeTimeForStep(int i);

private:
	double ticks = 0.0f;
	double lastTime = 0.0f;
	float delta = 0.0f;
	float avgDelta = 0.0f;
	float avgDeltaCounter = 0.0f;
	double frames = 0.0f;

	int frameTicker = 0;

	void delay();

	float ticksCounter =0.0f;
	float frameCounter = 0;
	const float target = 1.5f;

	float avgFPS = 0;

	float avgFPS10 = 0;

	double physicsTimer = 0.0;
	double renderTimer = 0.0;

	int updateWatchLoc = 0;
	int renderWatchLoc = 0;
	int physicsWatchLoc = 0;
	int overallWatchLoc = 0;
	int flushWatchLoc = 0;

	double updateStart = 0.0;
	bool updateStarted = false;

	double renderStart = 0.0;
	bool renderStarted = false;

	double physicsStart = 0.0;
	bool physicsStarted = false;

	double overallStart = 0.0;
	bool overallStarted = false;

	double flushStart = 0.0;
	bool flushStarted = false;
};