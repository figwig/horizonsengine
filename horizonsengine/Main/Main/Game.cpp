#include "Game.h"
#include <FreeImage\FreeImagePlus.h>
#include <iostream>
#include "Game.h"
#include "Scene.h"
#include "Scenery.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <oal_ver1.1/include/alc.h>
#include <oal_ver1.1/include/al.h>
#include "RenderSettings.h"
#include "PostProcessRect.h"
#include "BulletPhysicsHandler.h"
#include "HideMouse.h"
#include "SplashScreen.h"
#include "GLRenderer.h"
#include "ShaderManager.h"
#include "WireframeOnKey.h"
#include "InputListener.h"
#include "ModelManager.h"
#include "BehaviourManager.h"

using namespace std;

Game * Game::thisPointer = nullptr;

Game::Game()
{
	flush = true;
}

void Game::changeScene(std::string sceneName)
{
	for (int i = 0; i < scenes.size(); i++)
	{
		if (scenes[i] != nullptr)
			if (scenes[i]->name == sceneName)
			{
				
				updateScene = true;
				newSceneIndex = i;
				return;
			}

	}
	printf("Scene Not Found");
}

void Game::changeSceneNext()
{
	updateScene = true;

	newSceneIndex = currSceneIndex + 1;
}

void Game::changeScene(Scene* scene)
{
	bool found = false;
	int newIndex = 0;
	for (int i = 0; i < scenes.size(); i++)
	{

		if (scene == scenes[i])
		{
			newIndex = i;
			found = true;
		}

	}

	if (!found)
	{
		addScene(scene);
		newIndex = scenes.size() + 1;
	}

	updateScene = true;
	newSceneIndex = newIndex;

}

void Game::setup(Config newConfig, Scene * firstScene, bool useSplash)
{
	thisConfig = newConfig;

	thisConfig.displayDetails = new DisplayDetails();

	renderEngine = GLRenderer::singleton();

	renderEngine->setConfig(thisConfig);

	ModelManager::setup();

	//Set the anti-aliasing
	thisConfig.displayDetails->aaAmount = 4;

	gameClock = new Clock();
	gameClock->tick();

	//Save the scene that has been passed in by the engine wrapper

	if (useSplash)
	{
		TextureExaminer::singleton()->setDirectory("Textures//");
		Texture* tex = TextureExaminer::singleton()->getTexture("HorizonsSplash.png");
		Panel * img = new Panel(tex);
		img->transform->setScale(glm::vec3(0.08f));
		img->transform->setPos(glm::vec3(0, -20, 0));
		img->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;
		scenes.push_back(new SplashScreen(img));
		scenes.push_back(firstScene);
	}
	else
	{
		scenes.push_back(firstScene);
	}

	physicsEngine = BulletPhysicsHandler::getInstance();

	//Use the new renderer!!
	//renderEngine = new LogicalRenderer();


	debugCentre = DebugCentre::singleton();

	addGlobalBehaviour(new HideMouse());

	addGlobalBehaviour(new WireframeOnkey());



}

void Game::physicsUpdateLoop()
{
	gameClock->writeTimeForStep(CLOCK_PHYSICS);
	physicsEngine->doPhysics(gameClock->getDelta(), scenes[currSceneIndex]); 
	gameClock->writeTimeForStep(CLOCK_PHYSICS);
}

Game* Game::singleton()
{

	if (thisPointer == nullptr)
	{
		thisPointer = new Game();
	}
	
	return thisPointer;
}

Game::~Game()
{
	//Start cleanup
	ModelManager::cleanup();


	delete gameClock;

	for (int count = 0; count < globalBehaviours.size(); count++)
	{
		delete globalBehaviours[count];
	}
	delete renderEngine;

	delete physicsEngine;

	//delete[] scenes;

	exit(0);
}


void Game::Start()
{
	renderEngine->setConfig(thisConfig);
	renderEngine->initialize();

	renderEngine->setScene(scenes[currSceneIndex]);

	currSceneIndex = 0;

	scenes[0]->load(this);

	setConfigAll();

	scenes[0]->run();

	if (gameCamera != nullptr)
	{
		gameCamera->update(deltaTime, 0, 0, false);
	}
}

void Game::render(void)
{
	gameClock->writeTimeForStep(CLOCK_RENDER);
	thisConfig.shaderManager->beginFrame();
	renderEngine->update(deltaTime);


	renderEngine->render();
	gameClock->writeTimeForStep(CLOCK_RENDER);

	if (flush)
	{
		gameClock->writeTimeForStep(CLOCK_FLUSH);

		glFlush();
		gameClock->writeTimeForStep(CLOCK_FLUSH);
	}
}

void Game::setRenderEngine(SceneRenderer* render) 
{ 
	delete renderEngine; 
	renderEngine = render;
	render->setConfig(thisConfig);
	render->initialize();
	render->setScene(scenes[currSceneIndex]); 
	render->setCamera(gameCamera);
}

#pragma region Update Calls and Methods

void Game::update(void)
{
	gameClock->writeTimeForStep(CLOCK_UPDATE);
	thisConfig.displayDetails->update();

	//physicsThread = std::thread(&Game::physicsUpdateLoop, this);
	//Tick the clock to count time etc
	gameClock->tick();

	JobCentre::singleton()->QueueJob(physicsEngine->physicsFunction);

//	physicsEngine->physicsFunction();
	//Fill in the local value for the deltaTime
	deltaTime = gameClock->getDelta();

	BehaviourManager::singleton()->update(deltaTime);


	//update the lighting
	lightingUpdate();

	JobCentre::singleton()->DoPostUpdateJobs();

	//Wait for early physics to be done if it isn't already
	while (!physicsEngine->physicsDone);

	gameClock->writeTimeForStep(CLOCK_PHYSICS);
	physicsEngine->doLatePhysics(gameClock->getDelta(), scenes[currSceneIndex]); 
	gameClock->writeTimeForStep(CLOCK_PHYSICS);

	//Get all the camera variables sorted
	cameraUpdate();

	diffX = 0.0f;
	diffY = 0.0f;

	if (thisConfig.debugSettings->statePrints) printf("Finished update\n\n");

	scenes[currSceneIndex]->endOfFrame();

	if (thisConfig.debugSettings->statePrints) printf("Deleted any pending deletes`\n\n");



	debugCentre->update(deltaTime);

	JobCentre::singleton()->waitTillAllIdle();

	renderEngine->update(deltaTime);

	gameClock->writeTimeForStep(CLOCK_UPDATE);

	//physicsThread.join();



	if (updateScene)
	{
		updateScene = false;

		deleteScene(currSceneIndex);

		currSceneIndex = newSceneIndex;

		loadScene(currSceneIndex);
	}
}

void Game::cameraUpdate()
{
	if (gameCamera != nullptr)
	{	//Update the camera with the mouse position, play bool
		gameCamera->update(deltaTime, diffX, diffY, playing);
		//Get a local copy of the camera matrix
		cameraMatrix = gameCamera->getCameraMat();

		//tell current lighting the cameraPosition
		thisConfig.currLighting->cameraPos = gameCamera->getCameraPos();

		thisConfig.currLighting->cameraWithOutPersp = gameCamera->getCameraMatNoPersp();

		thisConfig.currLighting->persp = gameCamera->getPersp();
	}
	else
	{
		if (thisConfig.debugSettings->statePrints) printf("camera was null");
	}

}

void Game::lightingUpdate()
{
	//Update the lightingstate (does nothing yet)
	//thisConfig.currLighting->updateShadows(new ShadowBox());

	LightingState * currLighting = thisConfig.currLighting;

	for (int count = 0; count < currLighting->numberOfLights; count++)
	{
		if (currLighting->lights[count] != nullptr) {
			currLighting->lights[count]->update(deltaTime);
		}
	}

}

#pragma endregion

#pragma region Utility Calls

void Game::addPostProccessor(PostProcessingShader * x)
{
	renderEngine->addPostProcessor(x);
}

void Game::removePostProcessor(PostProcessingShader * x)
{
	renderEngine->removePostProcessor(x);
}

void Game::addGlobalBehaviour(GlobalBehaviour* x)
{
	globalBehaviours.push_back(x);
	x->setTarget(this);
	x->setup();
	BehaviourManager::singleton()->newBehaviour(x);
}

void Game::setConfigAll()
{
	std::vector<GameObject*> objects = scenes[currSceneIndex]->getNormalObjects();

	for (int count = 0; count < objects.size(); count++)
	{
		objects[count]->setConfig(thisConfig);
	}

	renderEngine->setConfig(thisConfig);

	objects = scenes[currSceneIndex]->getTransObjects();

	for (int count = 0; count <objects.size(); count++)
	{
		objects[count]->setConfig(thisConfig);
	}

	std::vector<ReflectiveObject*> rObjects = scenes[currSceneIndex]->getReflectiveObjects();

	for (int count = 0; count < rObjects.size(); count++)
	{
		rObjects[count]->setConfig(thisConfig);
	}

    objects = scenes[currSceneIndex]->getGUIObjects();
	

	for (int count = 0; count <objects.size(); count++)
	{
		objects[count]->setConfig(thisConfig);
	}

	for (int count = 0; count < thisConfig.currLighting->numberOfLights; count++)
	{
		if (thisConfig.currLighting->lights[count] != nullptr)
		{
			thisConfig.currLighting->lights[count]->setConfig(thisConfig);
		}
	}
}

void Game::addGameObject(GameObject * newObject)
{
	scenes[currSceneIndex]->addGameObject(newObject);
}

void Game::addTransparentGameObject(GameObject * newObject)
{
	scenes[currSceneIndex]->addTransparentGameObject(newObject);
}

void Game::addGUIObject(GameObject * newObject)
{
	scenes[currSceneIndex]->addGUIObject(newObject);
}

void Game::addReflectiveGameObject(ReflectiveObject * newObject)
{
	scenes[currSceneIndex]->addReflectiveGameObject(newObject);
}

#pragma endregion

#pragma region Event Handlers

void Game::mouseDown(int buttonID, int state, int mods)
{
	int type = 0;

	if (buttonID == GLFW_MOUSE_BUTTON_LEFT)
	{
		if (state == GLFW_PRESS)
		{
			mouseDownBool = true;
			type = INPUT_LEFT_MOUSE_DOWN;

		}
		else if (state == GLFW_RELEASE)
		{
			mouseDownBool = false;

			type = INPUT_LEFT_MOUSE_UP;
		
		}
	}

	else if (buttonID == GLFW_MOUSE_BUTTON_RIGHT)
	{
		if (state == GLFW_PRESS)
		{
			type = INPUT_RIGHT_MOUSE_DOWN;
		}
		else if (state == GLFW_RELEASE)
		{
			type = INPUT_RIGHT_MOUSE_UP;
		}
	}
	else if (buttonID == GLFW_MOUSE_BUTTON_MIDDLE)
	{
		if (state == GLFW_PRESS)
		{
			type = INPUT_MIDDLE_MOUSE_DOWN;
		}
		else if (state == GLFW_RELEASE)
		{
			type = INPUT_MIDDLE_MOUSE_UP;
		}
	}
	

	std::vector<InputListener*> listeners= scenes[currSceneIndex]->getListeners();

	for (int count = 0; count < listeners.size(); count++)
	{
		listeners[count]->OnClick(type,mousePrevX, mousePrevY);
	}

}

void Game::mouseMove(int x, int y)
{


}

void Game::scrollEvent(double x, double y) 
{
	std::vector<InputListener*> listeners = scenes[currSceneIndex]->getListeners();

	for (int count = 0; count < listeners.size(); count++)
	{
		listeners[count]->scrollWheel(x, y);
	}
}


void Game::keyEvent(int key, int scancode, int action, int mods)
{

	if (action ==GLFW_PRESS)
	{
		switch (key)
		{
		case 'w':
			keys |= Keys::Up;

			break;
		case 'W':
			keys |= Keys::Up;
			break;
		case 's':
			keys |= Keys::Down;
			break;
		case 'S':
			keys |= Keys::Down;
			break;
		case 'a':
			keys |= Keys::Left;
			break;
		case 'A':
			keys |= Keys::Left;
			break;
		case 'd':
			keys |= Keys::Right;
			break;
		case 'D':
			keys |= Keys::Right;
			break;
		case 'r':
			keys |= Keys::R;
			break;
		case 'R':
			keys |= Keys::R;
			break;
		case 'f':
			keys |= Keys::F;
			break;
		case 'F':
			keys |= Keys::F;
			break;
		case '1':
			keys |= Keys::num1;
			break;
		case '2':
			keys |= Keys::num2;
			break;
		case '3':
			keys |= Keys::num3;
			break;
		case '4':
			keys |= Keys::num4;
			break;
		case '5':
			keys |= Keys::num5;
			break;
		case 'x':
			keys |= Keys::x;
			break;
		case 'X':
			keys |= Keys::x;
			break;
		case 'q':
		case 'Q':
			keys |= Keys::q;
			break;
		case 'e':
		case 'E':
			keys |= Keys::e;
			break;
		case 'z':
		case 'Z':
			keys |= Keys::z;
			break;

		case GLFW_KEY_SPACE:
			keys |= Keys::Space;
			break;
		case GLFW_KEY_ESCAPE:
			gameClock->end();
			this->~Game();
		case GLFW_KEY_F4:
			keys |= Keys::F4;
			break;
		case GLFW_KEY_F3:
			keys |= Keys::F3;
			break;
		case GLFW_KEY_F5:
			keys |= Keys::F5;
			break;
		case GLFW_KEY_F6:
			keys |= Keys::F6;
			break;
		case GLFW_KEY_F7:
			keys |= Keys::F7;
			break;
		case GLFW_KEY_F8:
			keys |= Keys::F8;
			break;
		default:

			//

			break;
		}
	}
	//if this is a keyUp event
	else if (action == GLFW_RELEASE)
	{
		switch (key)
		{
		case 'w':
			keys &= (~Keys::Up);
			break;
		case 's':
			keys &= (~Keys::Down);
			break;
		case 'a':
			keys &= (~Keys::Left);
			break;
		case 'd':
			keys &= (~Keys::Right);
			break;
		case 'r':
			keys &= (~Keys::R);
			break;
		case 'f':
			keys &= (~Keys::F);
			break;
		case 'W':
			keys &= (~Keys::Up);
			break;
		case 'S':
			keys &= (~Keys::Down);
			break;
		case 'A':
			keys &= (~Keys::Left);
			break;
		case 'D':
			keys &= (~Keys::Right);
			break;
		case 'R':
			keys &= (~Keys::R);
			break;
		case 'F':
			keys &= (~Keys::F);
			break;
		case '1':
			keys &= (~Keys::num1);
			break;
		case '2':
			keys &= (~Keys::num2);
			break;
		case '3':
			keys &= (~Keys::num3);
			break;
		case '4':
			keys &= (~Keys::num4);
		break;		
		case '5':
			keys &= (~Keys::num5);
			break;
		case 'x':
			keys &= (~Keys::x);
			break;
		case 'X':
			keys &= (~Keys::x);
			break;
		case 'q':
		case 'Q':
			keys &= (~Keys::q);
			break;
		case 'e':
		case 'E':
			keys &= (~Keys::e);
			break;
		case 'z':
			keys &= (~Keys::z);
			break;
		case GLFW_KEY_SPACE:
			keys &= (~Keys::Space);
			break;
		case GLFW_KEY_F4:
			keys &= (~Keys::F4);
			break;
		case GLFW_KEY_F3:
			keys &= (~Keys::F3);
			break;
		case GLFW_KEY_F5:
			keys &= (~Keys::F5);
			break;
		case GLFW_KEY_F6:
			keys &= (~Keys::F6);
			break;
		case GLFW_KEY_F7:
			keys &= (~Keys::F7);
			break;
		case GLFW_KEY_F8:
			keys &= (~Keys::F8);
			break;
		default:
			//

			break;
		}
	}
}

void Game::mousePassiveMouse(double x, double y )
{
	diffX = x - mousePrevX;
	diffY = y - mousePrevY;

	mousePrevX = x;
	mousePrevY = y;

	std::vector<InputListener*> listeners = scenes[currSceneIndex]->getListeners();

	for (int count = 0; count < listeners.size(); count++)
	{
		listeners[count]->mouseMovePos(x,y);
	}
}

Keystate Game::getKeys()
{
	return keys;
}

#pragma endregion

