#pragma once

class Animation
{
public:
	Animation() {}
	virtual ~Animation() {}


	virtual void stepAnimation(double)=0;
	virtual float getAnimationLength() { return animationLength; }

	virtual bool isDone() = 0;
	virtual void reset()=0;
protected:
	//Length in seconds
	double animationLength;
	double animationStartTime;
};