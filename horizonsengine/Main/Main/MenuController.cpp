#include "MenuController.h"



MenuController::MenuController(Game* newRefr)
{
	refr = newRefr;
	contained = true;
}


MenuController::~MenuController()
{
}

void MenuController::update(double dt)
{
	if (!choiceMade)
	{
		Keystate keys = refr->getKeys();

		if (keys & Keys::num1)
		{	
			std::cout << "Loading scene...\n";
			refr->changeScene("Water");
			choiceMade = true;
		}
		if (keys & Keys::num2)
		{
			printf("Loading scene...\n");
			refr->changeScene("Night");
			choiceMade = true;
		}
		if (keys & Keys::num3)
		{
			printf("Loading scene...\n");
			refr->changeScene("Terrain");
			choiceMade = true;
		}
		if (keys & Keys::num4)
		{
			printf("Loading scene...\n");
			refr->changeScene("Physics");
			choiceMade = true;
		}
	}

}