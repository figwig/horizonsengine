#pragma once
#include "Object3D.h"
#include "ReflectiveObject.h"
#include "WaterFBO.h"
#include "RenderSettings.h"
#include "Material.h"


class WaterRenderingComponent : public Component
{
public: 
	WaterRenderingComponent(GameObject* host) : Component(host) {}

	~WaterRenderingComponent() {}

	float offset = 0.0f;
	glm::vec2 dimensions = glm::vec2(0);
};




class WaterMaterial : public Material
{
public :
	WaterMaterial(Texture* reflection, Texture* refraction, Texture* DUDV, Texture* normal, Texture* depth) : reflection(reflection), refraction(refraction), dudv(DUDV), normal(normal), depth(depth)
	{

	}


	void Bind()
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, reflection->id);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, refraction->id);

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, dudv->id);

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, normal->id);

		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, depth->id);
	}

	Texture* reflection = nullptr;
	Texture* refraction = nullptr;
	Texture* dudv = nullptr;
	Texture* normal = nullptr;
	Texture* depth = nullptr;
};

class WaterPlane : public ReflectiveObject
{
public:
	WaterPlane(glm::vec2 dimensions);
	~WaterPlane();

	void update(double f);
	void draw(RenderSettings*);
	void sortVAO();
	void applyScaling();


	void setDudv(GLuint x);
	void setNormal(GLuint x);

	void setup();

private:
	WaterRenderingComponent* wrc = nullptr;

	float offset = 0;
	float waterSpeed = 0.00005f;

	float vertices[18] = {
		// positions          
		-1.0f, 0.0f, -1.0f,
		-1.0f, 0.0f,  1.0f,
		1.0f, 0.0f, -1.0f,
		1.0f, 0.0f, -1.0f,
		-1.0f, 0.0f,  1.0f,
		1.0f, 0.0f,  1.0f
	};
	WaterMaterial* waterMaterial = nullptr;

	GLuint dudv;
	GLuint normal;
	GLuint ref = 0;
	GLuint refra = 0;
	GLuint depthMap = 0;

	GLuint thisVAO = 0;

	GLuint vertsVBO = 0;
	float scale;
	
	//Water FBO is a more specific reference to the thisFBO, thisFBO will be deleted to no need to delete this;
	WaterFBO * waterFBO = nullptr;

	void intializeFBOGroup();

	glm::vec2 dimensions;

	friend class WaterUpdateBehaviour;
};

class WaterUpdateBehaviour : public PrivateBehaviour
{
public:
	WaterUpdateBehaviour(WaterPlane* water) :water(water) {}
	~WaterUpdateBehaviour() {}
	void update(double deltaTime)
	{
		water->offset +=water-> waterSpeed * deltaTime;
		if (water->offset > 1.0f)
		{
			water->offset -= 1.0f;
		}

		water->wrc->offset = water->offset;

		water->thisFBO->update();
	}

	WaterPlane* water = nullptr;
};
