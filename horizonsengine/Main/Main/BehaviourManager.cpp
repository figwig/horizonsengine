#include "BehaviourManager.h"
#include "Behaviour.h"
#include "JobCentre.h"

BehaviourManager* BehaviourManager::thisPointer = nullptr;


BehaviourManager* BehaviourManager::singleton()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new BehaviourManager();
	}
	return thisPointer;
}


BehaviourManager::BehaviourManager()
{
	jc = JobCentre::singleton();
}


BehaviourManager::~BehaviourManager()
{

}


void BehaviourManager::update(double delta)
{



	//contained behaviours can be multithreaded, as they only point to their host game object
	for (int i = 0 ; i < containedBehaviours.size(); i++)
	{

		Behaviour* b = containedBehaviours[i];
		containedBehaviourReports[i]->done = false;

		jc->QueueJob([=]
			{
				b->update(delta);
				containedBehaviourReports[i]->done = true;
			}
		);	
	}


	//regular behaviours have a sort order. Those with the same sortorder can be executed asynchronusly. So, for example, all with sortOrder 0 will be executed first, then sort order 1, then sort order 2

	int size = behaviours.size();

	for (Behaviour* b : behaviours)
	{
		b->update(delta);
	}



	//Work out if the previous frames jobs are done before attemping new ones!
	bool allDone = false;
	//Make sure all behaviours are done!
	while (!allDone)
	{
		allDone = true;
		for (int i = 0; i < containedBehaviours.size(); i++)
		{
			if (!containedBehaviourReports[i]->done)
			{
				allDone = false;
			}
		}
	}


}

void BehaviourManager::newBehaviour(Behaviour* b)
{

	if (b->isContained())
	{
		containedBehaviours.push_back(b);
		containedBehaviourReports.push_back(new JobReport());
	}
	else
		behaviours.push_back(b);
}

void BehaviourManager::removeBehaviour(Behaviour* b)
{
	int  u = 0;
	if (!b->isContained()) {
		for (auto i : behaviours)
		{
			if (i == b)
			{
				behaviours.erase( behaviours.begin() + u);
				return;
			}
			u++;
		}
		std::cout << "Couldn't find behaviour to remove\n";
	}
	else
	{
		for (auto i : containedBehaviours)
		{
			if (i == b)
			{
				containedBehaviours.erase(containedBehaviours.begin() + u);
				containedBehaviourReports.erase(containedBehaviourReports.begin() + u);
				return;
			}
			u++;
		}
		std::cout << "Couldn't find behaviour to remove\n";
	}
}

void BehaviourManager::sortBehaviours()
{
}

