#pragma once
#include "Shader.h"

class ShadowShaders :
	public Shader
{
public:
	static ShadowShaders* getInstance();


	void setMats(glm::mat4 proj, glm::mat4 light, glm::mat4 T, glm::mat4 R, glm::mat4 S);

	void setMats(glm::mat4 proj, glm::mat4 light);

	void setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S);

	void prepareToRender(GameObject* go);
	void prepareToRender(RenderSettings* go);

	void setLighting(float, LightingState*) {}
private:
	void setup();

	static ShadowShaders* thisPointer;
	ShadowShaders();
	~ShadowShaders();
	GLuint locP = 0;
	GLuint locL = 0;
	GLuint locT = 0;
	GLuint locR = 0;
	GLuint locS = 0;

};

