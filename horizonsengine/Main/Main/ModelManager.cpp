#include "ModelManager.h"
#include "VertexDataManager.h"

AssimpContainer* ModelManager::ac = nullptr;
std::map < std::string, Model*> ModelManager::modelMap;

Model* ModelManager::getModel(std::string s)
{
	auto findResult = modelMap.find(s);
	if ( findResult != modelMap.end())
	{
		return findResult->second;
	}

	//Model hasn't already been imported
	Model* newModel = ac->readFile(s);

	modelMap.insert(std::make_pair(s, newModel));

	return newModel;
}


void ModelManager::basicShapesSetup()
{
	//Create a "Plane" shape that can be forwarded around to any object needing a plane
	Model* planeModel = new Model();

	std::vector<glm::vec3> points;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec4> tangents;
	std::vector<glm::vec4> bitangents;
	std::vector<glm::vec2> texts;
	std::vector<GLuint> indicies;


	float vertices[18] = {
		// positions          
		-1.0f, 0.0f, -1.0f,
		-1.0f, 0.0f,  1.0f,
		1.0f, 0.0f, -1.0f,
		1.0f, 0.0f, -1.0f,
		-1.0f, 0.0f,  1.0f,
		1.0f, 0.0f,  1.0f
	};

	for (int i = 0; i < 6; i++)
	{
		normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
		points.push_back(glm::vec3(vertices[i * 3], vertices[(i * 3) + 1], vertices[(i * 3) + 2]));
		tangents.push_back(glm::vec4(0));
		bitangents.push_back(glm::vec4(0));
		texts.push_back(glm::vec2(vertices[i * 3], vertices[(i * 3) + 2]));
		indicies.push_back(i);
	}

	Extents e = Extents(glm::vec3(-1.0f, 0.0f, -1.0f), glm::vec3(1.0f, 0.0f, 1.0f));

	MeshData* mesh = new MeshData(points,normals,texts, tangents, bitangents,indicies, indicies.size() );
	mesh->name = "Plane";

	GLuint VAO = VertexDataManager::getInstance()->addMeshToVAO(mesh);

	mesh->setVAO(VAO);

	planeModel->addMesh(mesh);

	mesh->setExtents(e);

	modelMap.insert(std::make_pair("Plane", planeModel));
}

void ModelManager::setup()
{
	ac = new AssimpContainer(); basicShapesSetup();
}

void ModelManager::cleanup()
{
	delete ac;
}