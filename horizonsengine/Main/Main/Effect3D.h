#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include "GameObject.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtc/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>

class Effect3D : public GameObject
{
public:
	Effect3D();
	~Effect3D();

	void update(double deltaTime);
	virtual void draw(glm::mat4 camera) =0;

	void setTarget(glm::vec3 newTarget) { target = newTarget; }

	void setIntensity(float intense) { magnitude = intense; }

	void setActive(bool newActive) { active = newActive; }
	Volume * getVolume() { return nullptr; }
protected:
	float magnitude = 2.0f;

	double deltaTime = 0;
	double timePassed = 0;
	GLuint VAO = 0;

	glm::mat4 R = glm::toMat4(glm::quat(glm::vec3(0.0f, 0.0f, 0.0f)));
	glm::mat4 pR = glm::toMat4(glm::quat(glm::vec3(0.0f, 0.0f, 0.0f)));



	bool active = true;
	float totalPoints = 6.0f;


	glm::vec3 target = glm::vec3(0.0f, 0.0f, 2.0f);
};