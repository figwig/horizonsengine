#include "Scene.h"
#include <stdio.h>
#include "Game.h"
#include "Config.h"
#include "PhysicsComponent.h"
#include "InputListener.h"


Scene::~Scene()
{

	delete currLighting;

	endOfFrame();

	std::vector<GameObject*> allObjects = getAllObjects();

	for (GameObject* go : allObjects)
	{
		delete go;
	}

	for (PublicBehaviour* pb : publicBehavioursList)
	{
		BehaviourManager::singleton()->removeBehaviour(pb);
		delete pb;
	}

	for (Waypoint* wp : waypointsList)
	{
		delete wp;
	}
	//Hostless Colliders are handled by Bullet, but they need to be informed here

	for (auto i : hostlessColliders) {
		Game::singleton()->getPhysicsEngine()->removeFromSimulation(i.second);
	}

	inputListenersList.clear();
	fboGroups.clear();

	JobCentre::singleton()->clearJobs();

	//InputListeners are always saved to the scene as something else, there is no such thing as an input listener that isn't also a Private/Public behaviour or a GameObject
	//The same is true for FBOGroups

}


void Scene::loadInit()
{
	currLighting = new LightingState();

	gameRefr->thisConfig.currLighting = currLighting;

	debugSettings = new DebugSettings();

	gameRefr->thisConfig.debugSettings = debugSettings;

	gameRefr->getDisplayDetails()->shadowDistance = 200;
}

void Scene::addWaypoint(Waypoint* newWp)
{
	waypointsList.push_back(newWp);
}

void Scene::addHostlessCollider(btCollisionShape * x, glm::mat4 transform)
{
	PhysicsComponent *hostless = new PhysicsComponent(x, transform);

	hostlessColliders.insert(std::make_pair(x, hostless));

	Game::singleton()->getPhysicsEngine()->addToSimulation(hostless);
}

std::vector<btCollisionShape*> Scene::getHostlessColliders()
{
	std::vector<btCollisionShape*> hostless;

	for (auto i : hostlessColliders)
	{
		hostless.push_back(i.first);
	}

	return hostless;
}

void Scene::addFBOGroup(FBOGroup * x)
{
	fboGroups.push_back(x);

	Game::singleton()->getRenderEngine()->onAddFBO(x, this);
}

Waypoint* Scene::findWaypoint(std::string name)
{
	for (int count = 0; count < waypointsList.size(); count++)
	{
		if (waypointsList[count] != nullptr)
		{
			if (waypointsList[count]->getName() == name|| waypointsList[count]->getName().substr(3) == name)
			{
				return waypointsList[count];
			}
		}
	}
}

void Scene::addGameObject(GameObject * newObject)
{

	renderableObjects.push_back(newObject);
	newObject->setConfig(gameRefr->getConfig());

	gameRefr->getRenderEngine()->onAddGameObject(newObject);

	if (newObject->getComponent<PhysicsComponent>() != nullptr)
		gameRefr->getPhysicsEngine()->addToSimulation(newObject->getComponent<PhysicsComponent>());
}

void Scene::addGUIObject(GameObject * newObject)
{
	guiRenderableObjects.push_back(newObject);
	newObject->setConfig(gameRefr->getConfig());

	gameRefr->getRenderEngine()->onAddGameObject(newObject);

	if (newObject->getComponent<PhysicsComponent>() != nullptr)
		gameRefr->getPhysicsEngine()->addToSimulation(newObject->getComponent<PhysicsComponent>());
}

void Scene::setGameObjectPointer(int type, GameObject* go)
{
	switch (type)
	{
	case SCENE_TERRAIN_PTR:
		terrain = go;
		break;
	default:
		std::cout << "oopsie";
		break;
	}
}

void Scene::removeGameObject(GameObject * object)
{
	removeEndOfFrame.push_back(object);
	object->kill();
	printf("added a pending delete object\n");
}

void Scene::endOfFrame()
{
	int deleted = 0;
	for (int eofCount = 0; eofCount < removeEndOfFrame.size(); eofCount++)
	{
		GameObject* object = removeEndOfFrame[eofCount];
		bool found = false;
		for (int count=0; count < renderableObjects.size(); count++)
		{
			if (renderableObjects[count] == object)
			{
				if (object->getComponent<PhysicsComponent>() != nullptr) gameRefr->getPhysicsEngine()->removeFromSimulation(object->getComponent<PhysicsComponent>());

				Game::singleton()->getRenderEngine()->onRemoveGameObject(object);

				delete object;
				renderableObjects.erase(renderableObjects.begin() + count);
				deleted++;
				printf("Deleted\n");
				found = true;
				break;
			}
		}

		if (found) continue;

		for (int count = 0; count < transparentRenderableObjects.size(); count++)
		{
			if (transparentRenderableObjects[count] == object)
			{
				if (object->getComponent<PhysicsComponent>() != nullptr) gameRefr->getPhysicsEngine()->removeFromSimulation(object->getComponent<PhysicsComponent>());

				Game::singleton()->getRenderEngine()->onRemoveGameObject(object);

				delete object;
				transparentRenderableObjects.erase(transparentRenderableObjects.begin() + count);
				deleted++;
				printf("Deleted\n");
				found = true;
				break;
			}
		}

		if (found) continue;

		for (int count = 0; count < reflectiveRenderableObjects.size(); count++)
		{
			if (reflectiveRenderableObjects[count] == object)
			{
				if (object->getComponent<PhysicsComponent>() != nullptr) gameRefr->getPhysicsEngine()->removeFromSimulation(object->getComponent<PhysicsComponent>());

				Game::singleton()->getRenderEngine()->onRemoveGameObject(object);

				delete object;
				reflectiveRenderableObjects.erase(reflectiveRenderableObjects.begin() + count);
				deleted++;
				printf("Deleted\n");
				found = true;
				break;
			}
		}

		if (found) continue;

		for (int count = 0; count < guiRenderableObjects.size(); count++)
		{
			if (guiRenderableObjects[count] == object)
			{
				Game::singleton()->getRenderEngine()->onRemoveGameObject(object);

				delete object;
				guiRenderableObjects.erase(guiRenderableObjects.begin() + count);
				deleted++;
				printf("Deleted\n");
				found = true;
				break;
			}
		}
		
	}
	
	removeEndOfFrame.clear();
}

void Scene::addTransparentGameObject(GameObject * newObject)
{
	transparentRenderableObjects.push_back(newObject);


	newObject->setConfig(gameRefr->getConfig());

	gameRefr->getRenderEngine()->onAddGameObject(newObject);

	if (newObject->getComponent<PhysicsComponent>() != nullptr)
		gameRefr->getPhysicsEngine()->addToSimulation(newObject->getComponent<PhysicsComponent>());
}

void Scene::addReflectiveGameObject(ReflectiveObject* newObject)
{
	reflectiveRenderableObjects.push_back(newObject);
	newObject->setConfig(gameRefr->getConfig());

	gameRefr->getRenderEngine()->onAddGameObject(newObject);


	if (newObject->getComponent<PhysicsComponent>() != nullptr)
		gameRefr->getPhysicsEngine()->addToSimulation(newObject->getComponent<PhysicsComponent>());

	Game::singleton()->getRenderEngine()->onAddFBO(newObject->getFBOGroup(), this);
}

void Scene::addPublicBehaviour(PublicBehaviour * newBeh)
{

	publicBehavioursList.push_back(newBeh);
	newBeh->setTarget(this);
	newBeh->setup();
	BehaviourManager::singleton()->newBehaviour(newBeh);
}

void Scene::registerInputListener(InputListener * x)
{


	for each(InputListener * i in inputListenersList)
	{

		if (i == x) {
			std::cout << "List already contains this input listener\n";
			return;
		}
	}
	inputListenersList.push_back(x);
}

void Scene::removeInputListener(InputListener* x)
{
	for (int i = 0; i < inputListenersList.size(); i++)
	{
		if (inputListenersList[i] == x)
		{
			inputListenersList.erase(inputListenersList.begin() + i);
			return;
		}
	}
}