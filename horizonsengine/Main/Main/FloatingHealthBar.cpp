#include "FloatingHealthBar.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include "Camera.h"

FloatingHealthBar::FloatingHealthBar(GLuint bTex, GLuint pTex, glm::vec2 res, float pX, float pY, bool facingCamera ) 
{
	barRes = res;
	barTex = bTex;
	progressTex = pTex;

	progressRes.x = barRes.x * pX;
	progressRes.y = barRes.y * pY;

	progWidth = progressRes.x/WorldGUI::unitsPerPixel;
	faceCamera = facingCamera;

	addPrivateBehaviour(new FloatingHealthBarUpdateBehaviour(this));
}


FloatingHealthBar::~FloatingHealthBar()
{
	delete barGUI;
	delete progressGUI;
}

void FloatingHealthBar::setup(float m)
{
	maxProgress = m;
	Texture* t = new Texture(barTex, barRes, "");
	barGUI = new WorldGUI(t);
	Texture* text = new Texture(progressTex, progressRes, "");
	progressGUI = new WorldGUI(text);
	//text = new Text();

	barGUI->setConfig(thisConfig);
	progressGUI->setConfig(thisConfig);
	//text->setConfig(thisConfig);
}

void FloatingHealthBar::setProgress(float x)
{
	progress = x;

	//text->setText(std::to_string(progress) + "/" + std::to_string(maxProgress));
}

void FloatingHealthBarUpdateBehaviour::update(double x)
{
	float yRot = 0.0f;
	if (fhb->faceCamera)
	{
		if (fhb->thisConfig.currLighting->currCamera != nullptr)
		{
			yRot = fhb->thisConfig.currLighting->currCamera->getCameraAngle().y;

		}
	}

	glm::vec3 rot = fhb->getRot();
	rot.y = yRot + M_PI;
	fhb->setRot(rot);

	fhb->barGUI->setPos(fhb->getPos());
	fhb->barGUI->setScale(fhb->getScale());
	fhb->barGUI->setRot(fhb->getRot());


	float progressDistance = -fhb->progWidth *(1-(fhb->progress/ fhb->maxProgress));
	//bring the progess bar slightly forward so that it doesn't glitch into the bar
	glm::vec3 gg = fhb->getRotMat()* fhb->getScaleMat() * glm::vec4(progressDistance, 0, 0.03f, 1);
	fhb->progressGUI->setPos(fhb->getPos() + gg );

	glm::vec3 progressScale = glm::vec3((fhb->progress / fhb->maxProgress),1,1);
	fhb->progressGUI->setScale(fhb->getScale()* progressScale );
	fhb->progressGUI->setRot(fhb->getRot());


	//text->setPos(getPos()+ gg*2.0f);
	//text->setScale(getScale());
	//text->setRot(getRot());

	//text->update(x);
}

void FloatingHealthBar::draw(RenderSettings * rs)
{
	progressGUI->draw(rs);
	barGUI->draw(rs);
	//text->draw(rs);
}