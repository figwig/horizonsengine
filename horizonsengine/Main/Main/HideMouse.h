#pragma once
#include "GlobalBehaviour.h"
class HideMouse :
	public GlobalBehaviour
{
public:
	HideMouse();
	~HideMouse();
	void update(double);

private:
	bool hidden = true;

	double timer = 0.0;
};

