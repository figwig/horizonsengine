#include "SimpleCamera.h"



RotatingCamera::RotatingCamera()
{
	cameraPos = glm::vec3(2.5f);
	cameraTarget = glm::vec3(0);
}


RotatingCamera::~RotatingCamera()
{

}


void RotatingCamera::update(double d, int, int, bool)
{
	calcPersp();
	angle += d * speed;
	cameraPos.x = cos(angle) * radius;
	cameraPos.z = sin(angle) * radius;
	cameraPos += cameraTarget;

	cameraNoPersp = glm::lookAt(cameraPos, cameraTarget, glm::vec3(0, 1, 0));

	camera = persp * cameraNoPersp;
}


glm::mat4 RotatingCamera::getLookAtMatrix(bool, glm::vec3 x)
{
	return persp* glm::lookAt(x, cameraTarget, glm::vec3(0, 1, 0));
}

StaticLookAtCamera::StaticLookAtCamera(glm::vec3 pos, GameObject * lookAt)
{
	cameraPos = pos;
	target= lookAt;
}


StaticLookAtCamera::~StaticLookAtCamera()
{

}


void StaticLookAtCamera::update(double d, int, int, bool)
{
	cameraNoPersp = glm::lookAt(cameraPos, target->getPos(), glm::vec3(0, 1, 0));

	camera = persp * cameraNoPersp;
}


glm::mat4 StaticLookAtCamera::getLookAtMatrix(bool, glm::vec3 x)
{
	return persp * glm::lookAt(x, target->getPos(), glm::vec3(0, 1, 0));
}