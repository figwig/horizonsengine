#include "PostProcessRect.h"
#include "Game.h"


PostProcessRect::PostProcessRect(GLuint x)
{
	texture = x;
}


PostProcessRect::~PostProcessRect()
{
}

void PostProcessRect::draw(RenderSettings*)
{
	glUseProgram(postProcessingShader->getProgramID());

	Game::singleton()->getRenderEngine()->bindVAO(VAO);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0);
}