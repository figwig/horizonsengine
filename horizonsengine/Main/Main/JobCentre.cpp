#include "JobCentre.h"
#include <iostream>
JobCentre* JobCentre::thisPointer = nullptr;


JobCentre* JobCentre::singleton()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new JobCentre();
	}

	return thisPointer;
}

void JobCentre::release()
{
	if (thisPointer != nullptr)
	{
		delete thisPointer;
		thisPointer = nullptr;
	}
}

JobCentre::JobCentre()
{
	Initialize();
}

JobCentre::~JobCentre()
{
	isDone = true;

	// Ensure that all jobs are done (by joining the thread, 
	// thus waiting for all its work to be done)
	for (auto& item : workers)
	{
		item->thread.join();
		delete item;
	}
	
}

void JobCentre::Initialize()
{
	int supportedThreads = std::thread::hardware_concurrency();

	isDone = false;

	for (size_t i = 0; i < supportedThreads; ++i)
	{

		workers.push_back(new Worker());
		//workerThreads.push_back(std::thread(&JobCentre::WorkerThread, this));
		workers[i]->jc = this;
		workers[i]->thread = std::thread(&Worker::Work, workers[i]);
	}
}

void JobCentre::clearJobs()
{
	CpuJob j;
	while (locklessReadyQueue.try_dequeue(j));
}

void JobCentre::QueueJob(std::function<void()> function, HE_JobScheduleBit scheduleBit)
{
	CpuJob aJob = {};
	aJob.function = function;
	
	aJob.reportDone = false;


    locklessReadyQueue.enqueue(std::move(aJob));

}


void JobCentre::DoPostUpdateJobs()
{
	locklessReadyQueue.enqueue_bulk(postUpdateJobs.data(), postUpdateJobs.size());
	postUpdateJobs.clear();
}

void JobCentre::waitTillAllIdle()
{
	bool allIdle = false;
	while (!allIdle)
	{
		allIdle = true;
		for (auto i : workers)
		{
			if (!i->idle)
			{
				allIdle = false;
			}
		}
	}
}

void Worker::Work()
{
	while (true)
	{
		// Make sure that we don't need to be done now!

		if (jc->isDone) return;

		CpuJob CurJob;
		bool found = jc->locklessReadyQueue.try_dequeue(CurJob);

		if (found)
		{
			idle = false;
	
			//Do this shit
			CurJob.function();


		}
		else
		{
			idle = true;
			std::this_thread::sleep_for(std::chrono::milliseconds(jc->workerWaitDelay));
		}
	}
}