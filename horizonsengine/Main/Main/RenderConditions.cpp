#include "RenderConditions.h"
#include "Game.h"
#include "Material.h"
#include "VertexDataManager.h"

RenderConditions::RenderConditions(GameObject * h ) : Component(h)
{
	shaderInUse = DefaultShaders::getInstance();
	renderType |= PassFlagTypes::AllObjects;
	renderType |= PassFlagTypes::OpaqueObjects;

	//Set up the on model mat changed callback with gameobject
	host->callbackSet = true;
	host->callOnModelMatChange = [=]
	{
		if(this->getShader()->getInstancedBool())
		VertexDataManager::getInstance()->submitModelMatrix(host->getModelMat(), instanceHandle);
	};
}

RenderConditions::~RenderConditions()
{
	host->callbackSet = false;


	VertexDataManager::getInstance()->removeInstanceOfMeshes(model, instanceHandle);
}

Material* RenderConditions::getMaterial(int meshIndex)
{
	if (materials.size() > meshIndex)
		return materials[meshIndex];
	else
		return nullptr;
}

void RenderConditions::setMaterial(Material* mat, int meshIndex)
{
	if (model == nullptr)
	{
		std::cout << "Tried to assign material before model\n";
		return;
	}

	materials[meshIndex] = mat;

	if(shaderInUse->getInstancedBool())
		submitMaterialIDs();
}

void RenderConditions::setAllMaterials(Material* mat)
{
	for (int i = 0; i < materials.size(); i++)
	{
		materials[i] = mat;
	}
	if (shaderInUse->getInstancedBool())
	{
		submitMaterialIDs();
	}
}

Model* RenderConditions::getModel()
{
	return model;
}

void RenderConditions::setModel(Model* newModel)
{
	//Presumably need some removal code here with the vertexdatamanager if model isn't nullptr,

	if (shaderInUse == nullptr)
	{
		throw std::runtime_error("Tried to assign a model with no shader assigned");
		return;
	}

	if (model != nullptr && shaderInUse->getInstancedBool())
	{
		VertexDataManager::getInstance()->removeInstanceOfMeshes(model, instanceHandle);
	}


	materials.clear();

	for (int i = 0; i < newModel->getMeshCount(); i++)
	{
		materials.push_back(newModel->getMeshByIndex(i)->getDefaultMaterial());
	}

	model = newModel;



	if (shaderInUse->getInstancedBool())
	{
																
		instanceHandle = VertexDataManager::getInstance()->addInstanceOfMeshes(model);

		VertexDataManager::getInstance()->submitModelMatrix(host->getModelMat(), instanceHandle);


		submitMaterialIDs();
	}
}

void RenderConditions::setShader(Shader* newShader)
{
	bool oldInstanced = false;
	if (shaderInUse != nullptr)
	{
		if (shaderInUse->getInstancedBool())
		{
			oldInstanced = true;
		}
	}

	shaderInUse = newShader;

	if (shaderInUse->getInstancedBool() && !oldInstanced)
	{
		instanceHandle = VertexDataManager::getInstance()->addInstanceOfMeshes(model);

		VertexDataManager::getInstance()->submitModelMatrix(host->getModelMat(), instanceHandle);

		submitMaterialIDs();
	}
	else if (oldInstanced)
	{
		//Remove datra code here
	}

}

Shader* RenderConditions::getShader()
{
	return shaderInUse;
}

void RenderConditions::submitMaterialIDs()
{
	std::vector<GLuint> matIDs;

	for (int i = 0; i < model->getMeshCount(); i++)
	{
		if (i < materials.size() && materials[i] != nullptr)
			matIDs.push_back(materials[i]->getMaterialID());
		else
			matIDs.push_back(0);
	}

	VertexDataManager::getInstance()->submitMaterialID(matIDs, instanceHandle);
}


