#include "VertexDataManager.h"
#include <future>
#include <iostream>
#include "Game.h"
#include "ShaderManager.h"
#include "VAOData.h"

VertexDataManager * VertexDataManager::thisPointer = nullptr;

VertexDataManager::VertexDataManager()
{
	indexedVAO = VAO();
	indexedVAO.vertexSize = 0;

	glGenVertexArrays(1, &indexedVAO.id);
	Game::singleton()->getRenderEngine()->bindVAO(indexedVAO.id);

	glGenBuffers(1, &indexedVAO.vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, indexedVAO.vertsVBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &indexedVAO.normsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, indexedVAO.normsVBO);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);

	glGenBuffers(1, &indexedVAO.tangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, indexedVAO.tangentsVBO);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(3);

	glGenBuffers(1, &indexedVAO.bitangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, indexedVAO.bitangentsVBO);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(4);

	glGenBuffers(1, &indexedVAO.textsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, indexedVAO.textsVBO);
	glVertexAttribPointer(8, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(8);


	glGenBuffers(1, &indexedVAO.materialIDVBO);
	glBindBuffer(GL_ARRAY_BUFFER, indexedVAO.materialIDVBO);
	glVertexAttribIPointer(5, 1, GL_UNSIGNED_INT,  0, (void*)0);
	glEnableVertexAttribArray(5);
	glVertexAttribDivisor(5, 1);

	int vec4Size = sizeof(glm::vec4);

	glGenBuffers(1, &indexedVAO.modelMatriciesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, indexedVAO.modelMatriciesVBO);
	glEnableVertexAttribArray(9);
	glVertexAttribPointer(9, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
	glEnableVertexAttribArray(10);
	glVertexAttribPointer(10, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(vec4Size));
	glEnableVertexAttribArray(11);
	glVertexAttribPointer(11, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
	glEnableVertexAttribArray(12);
	glVertexAttribPointer(12, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

	glVertexAttribDivisor(9, 1);
	glVertexAttribDivisor(10, 1);
	glVertexAttribDivisor(11, 1);
	glVertexAttribDivisor(12, 1);

	glGenBuffers(1, &indexedVAO.indiciesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexedVAO.indiciesVBO);

	indexedVAO.shader = ShaderManager::singleton()->getShader<DefaultShaders*>();

	indexedVAO.vaoMaterial = new InstancedMaterial(indexedVAO.shader);

	Game::singleton()->getRenderEngine()->bindVAO(0);
}


VertexDataManager::~VertexDataManager()
{
}


VertexDataManager* VertexDataManager::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new VertexDataManager();
	}
	return thisPointer;
}

GLuint VertexDataManager::addMeshToVAO(MeshData* thisMesh, bool indexedAlready)
{
	if (!indexedAlready)
		indexMeshData(thisMesh);

	GLuint VAO = 0;

	std::vector<float> points = *thisMesh->getPointsV();
	std::vector<float> norms = *thisMesh->getNormalsV();
	std::vector<float> tangents = *thisMesh->getTangentsV();
	std::vector<float> bitangents = *thisMesh->getBitangentsV();
	std::vector<float> texts = *thisMesh->getTextsV();
	std::vector<GLuint> indicies = *thisMesh->getIndiciesV();

	GLuint vertsVBO;
	GLuint normsVBO;
	GLuint tangentsVBO;
	GLuint bitangentsVBO;
	GLuint textsVBO;
	GLuint indiciesVBO;
	GLuint modelMatriciesVBO;

	int inidiciesCount = thisMesh->getElementsCount();
	int totalVerts = thisMesh->getPointsCount();

	//Make the VAO

	Game::singleton()->getRenderEngine()->bindVAO(indexedVAO.id);

	//Setup the Vertex buffer
	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);

	glBufferData(GL_ARRAY_BUFFER, ((indexedVAO.vertexSize * 3) + (totalVerts * 3)) * sizeof(float), (void*)0, GL_STATIC_DRAW);

	glBindBuffer(GL_COPY_READ_BUFFER, indexedVAO.vertsVBO);
	glBindBuffer(GL_COPY_WRITE_BUFFER, vertsVBO);

	glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, indexedVAO.vertexSize * 3 * sizeof(float));

	glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * indexedVAO.vertexSize * 3, sizeof(float) * totalVerts * 3, points.data());

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glDeleteBuffers(1, &indexedVAO.vertsVBO);
	indexedVAO.vertsVBO = vertsVBO;



	//The normal Buffer
	glGenBuffers(1, &normsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, normsVBO);

	glBufferData(GL_ARRAY_BUFFER, ((indexedVAO.vertexSize * 3) + (totalVerts * 3)) * sizeof(float), (void*)0, GL_STATIC_DRAW);

	glBindBuffer(GL_COPY_READ_BUFFER, indexedVAO.normsVBO);
	glBindBuffer(GL_COPY_WRITE_BUFFER, normsVBO);

	glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, indexedVAO.vertexSize * 3 * sizeof(float));

	glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * indexedVAO.vertexSize * 3, sizeof(float) * totalVerts * 3, norms.data());

	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);

	glDeleteBuffers(1, &indexedVAO.normsVBO);
	indexedVAO.normsVBO = normsVBO;


	////the tangent buffer
	glGenBuffers(1, &tangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, tangentsVBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * ((totalVerts * 4) + (indexedVAO.vertexSize * 4)), (void*)0, GL_STATIC_DRAW);
	glBindBuffer(GL_COPY_READ_BUFFER, indexedVAO.tangentsVBO);
	glBindBuffer(GL_COPY_WRITE_BUFFER, tangentsVBO);

	glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, indexedVAO.vertexSize * 4 * sizeof(float));

	glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * indexedVAO.vertexSize * 4, sizeof(float) * totalVerts * 4, tangents.data());

	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(3);

	glDeleteBuffers(1, &indexedVAO.tangentsVBO);
	indexedVAO.tangentsVBO = tangentsVBO;




	//Bitangents
	glGenBuffers(1, &bitangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, bitangentsVBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * ((totalVerts * 4) + (indexedVAO.vertexSize * 4)), (void*)0, GL_STATIC_DRAW);
	glBindBuffer(GL_COPY_READ_BUFFER, indexedVAO.bitangentsVBO);
	glBindBuffer(GL_COPY_WRITE_BUFFER, bitangentsVBO);

	glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, indexedVAO.vertexSize * 4 * sizeof(float));

	glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * indexedVAO.vertexSize * 4, sizeof(float) * totalVerts * 4, bitangents.data());

	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(4);

	glDeleteBuffers(1, &indexedVAO.bitangentsVBO);
	indexedVAO.bitangentsVBO = bitangentsVBO;



	//and the texture co-ord buffer
	glGenBuffers(1, &textsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, textsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * ((totalVerts * 2) + (indexedVAO.vertexSize * 2)), (void*)0, GL_STATIC_DRAW);

	glBindBuffer(GL_COPY_READ_BUFFER, indexedVAO.textsVBO);
	glBindBuffer(GL_COPY_WRITE_BUFFER, textsVBO);

	glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, sizeof(float) * indexedVAO.vertexSize * 2);

	glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * indexedVAO.vertexSize * 2, sizeof(float) * totalVerts * 2, texts.data());

	glVertexAttribPointer(8, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(8);

	glDeleteBuffers(1, &indexedVAO.textsVBO);
	indexedVAO.textsVBO = textsVBO;


	

	//and the elements buffer
	glGenBuffers(1, &indiciesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (inidiciesCount+ indexedVAO.elements) * sizeof(GLuint), (void*)0, GL_STATIC_DRAW);

	glBindBuffer(GL_COPY_READ_BUFFER, indexedVAO.indiciesVBO);
	glBindBuffer(GL_COPY_WRITE_BUFFER, indiciesVBO);

	glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, indexedVAO.elements * sizeof(GLuint));

	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, indexedVAO.elements * sizeof(GLuint), inidiciesCount * sizeof(GLuint), indicies.data());

	glDeleteBuffers(1, &indexedVAO.indiciesVBO);
	indexedVAO.indiciesVBO = indiciesVBO;

	thisMesh->setOffset(indexedVAO.vertexSize, indexedVAO.elements);

	indexedVAO.vertexSize += totalVerts;
	indexedVAO.elements += inidiciesCount;

	//Push back an empty vector
	
	HE_PerInstanceDataSet dataSet = HE_PerInstanceDataSet();
	

	indexedVAO.VAOlayout.push_back(std::make_pair(thisMesh, dataSet));


	return indexedVAO.id;
}

void VertexDataManager::indexMeshData(MeshData* currMesh)
{
	int count2;
	bool foundSimilarIndex;
	//Loop through the vertex, normal and texcoord buffers and find similar combinations
	int indexesDoneSoFar = 0;
	int vertexCount = 0;
	int indexInBuffer = 0;
	int vertCount3 = 0;
	int vertCount2 = 0;
	int indexInBuffer3 = 0;
	int indexInBuffer2 = 0;
	float Va1;
	float Va2;
	float Va3;

	float Vb1;
	float Vb2;
	float Vb3;

	float Na1;
	float Na2;
	float Na3;

	float Nb1;
	float Nb2;
	float Nb3;

	float Ta1;
	float Ta2;
	float Tb1;
	float Tb2;
	//The following is indexing within the given meshdata


	std::vector<float> currentMeshPoints = *currMesh->getPointsV();
	std::vector<float> currentMeshNormals = *currMesh->getNormalsV();
	std::vector<float> currentMeshTexts = *currMesh->getTextsV();
	std::vector<float> currentMeshTangents = *currMesh->getTangentsV();
	std::vector<float> currentMeshBitangents = *currMesh->getBitangentsV();

	std::vector<float> uniqueMeshPoints;
	std::vector<float> uniqueMeshTexts;
	std::vector<float> uniqueMeshNormals;
	std::vector<float> uniqueMeshTangents;
	std::vector<float> uniqueMeshBitangents;

	std::vector<GLuint> indicies;
		

	int passesSinceLastIndexFound = 0;

	int failedOnV = 0;
	int failedOnT = 0;
	int failedOnN = 0;

	//	//For each vertex in shape
	for (vertexCount; vertexCount < currMesh->getElementsCount(); vertexCount++)
	{
		vertCount3 = vertexCount * 3;
		vertCount2 = vertexCount * 2;

		float vertexAccuracy = 0.0f;
		float normalAccuracy = 0.0f;
		float texCoordAccuracy = 0.0f;


		Va1 = (currentMeshPoints)[vertCount3];


		foundSimilarIndex = false;

		//Loop through previous entries to the index buffer and see if there is a similar one.

		count2 = 0;
		//after the indexing accuracy figure is passed, only check that many vertexes for index pairings. 
		if (indexesDoneSoFar > 5 + indexingAccuracy && indexingAccurayOn) count2 = uniqueMeshPoints.size() / 3 - indexingAccuracy;


		for (count2; count2 < indexesDoneSoFar; count2++)
		{
			indexInBuffer = indicies[count2];
			indexInBuffer3 = indexInBuffer * 3;

			Vb1 = uniqueMeshPoints[indexInBuffer3];

			//NEsted Ifs to save processor time
			if (isPointNear(Va1, Vb1, vertexAccuracy))
			{
				indexInBuffer2 = indexInBuffer * 2;

				//Grab some more b stuff for testing
				Vb2 = uniqueMeshPoints[(indexInBuffer3)+1];
				Vb3 = uniqueMeshPoints[(indexInBuffer3)+2];

				Nb1 = uniqueMeshNormals[indexInBuffer3];
				Nb2 = uniqueMeshNormals[(indexInBuffer3)+1];
				Nb3 = uniqueMeshNormals[(indexInBuffer3)+2];

				Tb1 = uniqueMeshTexts[indexInBuffer2];
				Tb2 = uniqueMeshTexts[(indexInBuffer2)+1];

				//and the rest of the A stuff
				Va2 = (currentMeshPoints)[(vertCount3)+1];
				Va3 = (currentMeshPoints)[(vertCount3)+2];

				Na1 = currentMeshNormals[vertCount3];
				Na2 = currentMeshNormals[(vertCount3)+1];
				Na3 = currentMeshNormals[(vertCount3)+2];

				Ta1 = currentMeshTexts[vertCount2];
				Ta2 = currentMeshTexts[(vertCount2)+1];

				if ((isPointNear(Vb2, Va2, vertexAccuracy)) && (isPointNear(Vb3, Va3, vertexAccuracy)))
				{
					if ((isPointNear(Nb1, Na1, normalAccuracy)) && (isPointNear(Nb2, Na2, normalAccuracy)) && (isPointNear(Nb3, Na3, normalAccuracy)))
					{
						if (isPointNear(Tb1, Ta1, texCoordAccuracy) && isPointNear(Ta2, Tb2, texCoordAccuracy))
						{

							//Found a similar vertex on all counts! mark this index as the current indexInBuffer, and don't add a new index

							indicies.push_back(indexInBuffer);

							//Add the tans and bitans to index
							uniqueMeshTangents[indexInBuffer * 4] += currentMeshTangents[vertexCount * 4];
							uniqueMeshTangents[(indexInBuffer * 4) + 1] += currentMeshTangents[(vertexCount * 4) + 1];
							uniqueMeshTangents[(indexInBuffer * 4) + 2] += currentMeshTangents[(vertexCount * 4) + 2];
							uniqueMeshTangents[(indexInBuffer * 4) + 3] += currentMeshTangents[(vertexCount * 4) + 3];

							uniqueMeshBitangents[indexInBuffer * 4] += currentMeshBitangents[vertexCount * 4];
							uniqueMeshBitangents[(indexInBuffer * 4) + 1] += currentMeshBitangents[(vertexCount * 4) + 1];
							uniqueMeshBitangents[(indexInBuffer * 4) + 2] += currentMeshBitangents[(vertexCount * 4) + 2];
							uniqueMeshBitangents[(indexInBuffer * 4) + 3] += currentMeshBitangents[(vertexCount * 4) + 3];

							foundSimilarIndex = true;
							break;
						}
						else
							failedOnT++;
					}
					else
						failedOnN++;
				}
				else
					failedOnV++;

			}
			else
				failedOnV++;
		}

		if (foundSimilarIndex)
		{
			//do not modify indexes done, there are the same number of indexes.
		}

		else
		{
			//create new index entry
			indicies.push_back(indexesDoneSoFar);
			uniqueMeshPoints.push_back( currentMeshPoints[vertexCount * 3]);
			uniqueMeshPoints.push_back(currentMeshPoints[(vertexCount * 3) + 1]);
			uniqueMeshPoints.push_back( currentMeshPoints[(vertexCount * 3) + 2]);

			uniqueMeshNormals.push_back(currentMeshNormals[vertexCount * 3]);
			uniqueMeshNormals.push_back(currentMeshNormals[(vertexCount * 3) + 1]);
			uniqueMeshNormals.push_back(currentMeshNormals[(vertexCount * 3) + 2]);

			uniqueMeshTexts.push_back(currentMeshTexts[vertexCount * 2]);
			uniqueMeshTexts.push_back(currentMeshTexts[(vertexCount * 2) + 1]);

			uniqueMeshTangents.push_back(currentMeshTangents[vertexCount * 4]);
			uniqueMeshTangents.push_back(currentMeshTangents[(vertexCount * 4) + 1]);
			uniqueMeshTangents.push_back(currentMeshTangents[(vertexCount * 4) + 2]);
			uniqueMeshTangents.push_back(currentMeshTangents[(vertexCount * 4) + 3]);

			uniqueMeshBitangents.push_back(currentMeshBitangents[vertexCount * 4]);
			uniqueMeshBitangents.push_back(currentMeshBitangents[(vertexCount * 4) + 1]);
			uniqueMeshBitangents.push_back(currentMeshBitangents[(vertexCount * 4) + 2]);
			uniqueMeshBitangents.push_back(currentMeshBitangents[(vertexCount * 4) + 3]);

			indexesDoneSoFar++;
			

			passesSinceLastIndexFound++;
		}

	}//End of all the vertexes

	std::cout << "Finished indexing. Matching failed on vertexes " << failedOnV << " times, " << failedOnN << " times on normals, and " << failedOnT << " times on texCoords" << std::endl;

	std::cout << (indicies.size() - (uniqueMeshPoints.size() / 3)) << " entries saved\n";

	currMesh->newData(uniqueMeshPoints, uniqueMeshNormals, uniqueMeshTangents, uniqueMeshBitangents, uniqueMeshTexts, indicies);
}

GLuint VertexDataManager::makeBasicVAO(std::vector<float> points)
{
	GLuint thisVAO = 0;
	GLuint vertsVBO = 0;
	glGenVertexArrays(1, &thisVAO);
	Game::singleton()->getRenderEngine()->bindVAO(thisVAO);

	//Setup the Vertex buffer
	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*points.size(), points.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	GLuint indiciesVBO = 0;
	std::vector<GLuint> indicies;
	for (int i = 0; i < points.size() / 3; i++)
	{
		indicies.push_back(i);
	}

	glGenBuffers(1, &indiciesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), indicies.data(), GL_STATIC_DRAW);

	Game::singleton()->getRenderEngine()->bindVAO(0);
	return thisVAO;
}

bool VertexDataManager::isPointNear(float a, float b, float accuracy)
{
	if(a> b)
		return (a - b < accuracy);
	else
		return (b - a < accuracy);
}

HE_ModelInstanceHandle VertexDataManager::addInstanceOfMeshes(Model*model)
{
	HE_ModelInstanceHandle handle;

	for (int meshIndex = 0; meshIndex<model->getMeshCount(); meshIndex++)
	{ 
		bool foundThisMesh = false;
		MeshData* meshData = model->getMeshByIndex(meshIndex);

		int index = 0;

		//Locate mesh data in VAOLayout
		for (index; index< indexedVAO.VAOlayout.size(); index++)
		{
			
			if (indexedVAO.VAOlayout[index].first == meshData) {
				foundThisMesh = true;

				//Pushback the index matrix, copy whatever is in the last entry (last entry index is size())
				int size = indexedVAO.VAOlayout[index].second.indexMask.size();

				//Therefore no entries
				if (size == 0)
				{
					indexedVAO.VAOlayout[index].second.indexMask.push_back(0);
				}
				else
				{
					indexedVAO.VAOlayout[index].second.indexMask.push_back(indexedVAO.VAOlayout[index].second.indexMask[size - 1]);
				}

				//Insert model matrix in modelmatricies list
				if (indexedVAO.flags & HE_PerInstanceDataSet::HE_PerInstanceDataSetFlags::ModelMatrix) {

					indexedVAO.VAOlayout[index].second.modelMatricies.push_back(glm::mat4(1));

				}

				//and Material ID
				if (indexedVAO.flags & HE_PerInstanceDataSet::HE_PerInstanceDataSetFlags::MaterialID)
				{
					indexedVAO.VAOlayout[index].second.materialIDs.push_back(0);
				}
				break;
			}
		}

		if (!foundThisMesh)
		{
			std::cout << "Was unable to find the mesh in current GPU data\n";
			return HE_ModelInstanceHandle::invalid();
		}

		handle.meshDataIndexes.push_back(index);
		handle.propertyIndexes.push_back(indexedVAO.VAOlayout[index].second.indexMask.size() - 1);
		
	}

	//End of every frame, if changes have been made, process buffer updates
	indexedVAO.matrixBufferNeedsFlushing = true;

	return handle;
}

void VertexDataManager::submitModelMatrix(glm::mat4 matrix,HE_ModelInstanceHandle handle)
{
	if (!handle.valid) return;

	for (int meshIndex = 0; meshIndex < handle.meshDataIndexes.size(); meshIndex++)
	{
		//Use the instance handle to find the correct model matrix
		//This is the index of the mesh within the VAO. The first VAO to be added would be 0. Relates to how many meshes there are in the game
		int meshdataLocIndex = handle.meshDataIndexes[meshIndex];

		//This is the index for the specific reference of this mesh. The first instance of a mesh would be 0. Relates to how many instances of this mesh are in the game
		int propertyLocIndex = handle.propertyIndexes[meshIndex];


		//Mesh indexes are mutable. Modify the property index by the value in the index mask, used to compensate for when a mesh index is removed
		int postMaskPropertyIndex = propertyLocIndex + indexedVAO.VAOlayout[meshdataLocIndex].second.indexMask[propertyLocIndex];

		indexedVAO.VAOlayout[meshdataLocIndex].second.modelMatricies[postMaskPropertyIndex] = matrix;
	}

	indexedVAO.matrixBufferNeedsFlushing = true;
}

void VertexDataManager::submitMaterialID(std::vector<GLuint> IDs, HE_ModelInstanceHandle handle)
{
	if (!handle.valid) return;

	for (int meshIndex = 0; meshIndex < handle.meshDataIndexes.size(); meshIndex++)
	{
		//Use the instance handle to find the correct material ID
		//This is the index of the mesh within the VAO. The first VAO to be added would be 0. Relates to how many meshes there are in the game
		int meshdataLocIndex = handle.meshDataIndexes[meshIndex];

		//This is the index for the specific reference of this mesh. The first instance of a mesh would be 0. Relates to how many instances of this mesh are in the game
		int propertyLocIndex = handle.propertyIndexes[meshIndex];


		//Mesh indexes are mutable. Modify the property index by the value in the index mask, used to compensate for when a mesh index is removed
		int postMaskPropertyIndex = propertyLocIndex + indexedVAO.VAOlayout[meshdataLocIndex].second.indexMask[propertyLocIndex];

		indexedVAO.VAOlayout[meshdataLocIndex].second.materialIDs[postMaskPropertyIndex] = IDs[meshIndex];
	}

	indexedVAO.materialBufferNeedsFlushing = true;
}

void VertexDataManager::removeInstanceOfMeshes(Model* model, HE_ModelInstanceHandle handle)
{
	if (!handle.valid) return;

	for (int meshIndex = 0; meshIndex < handle.meshDataIndexes.size(); meshIndex++)
	{
		//Use the instance handle to find the correct material ID
		//This is the index of the mesh within the VAO. The first VAO to be added would be 0. Relates to how many meshes there are in the game
		int meshdataLocIndex = handle.meshDataIndexes[meshIndex];

		//This is the index for the specific reference of this mesh. The first instance of a mesh would be 0. Relates to how many instances of this mesh are in the game
		int propertyLocIndex = handle.propertyIndexes[meshIndex];


		//Mesh indexes are mutable. Modify the property index by the value in the index mask, used to compensate for when a mesh index is removed
		int postMaskPropertyIndex = propertyLocIndex + indexedVAO.VAOlayout[meshdataLocIndex].second.indexMask[propertyLocIndex];

		//Take one from all following entries to the index mask to compensate for the deletion
		for (int indexMaskPosition = propertyLocIndex; indexMaskPosition < indexedVAO.VAOlayout[meshdataLocIndex].second.indexMask.size(); indexMaskPosition++)
		{
			indexedVAO.VAOlayout[meshdataLocIndex].second.indexMask[indexMaskPosition]--;
		}

		if (indexedVAO.flags & HE_PerInstanceDataSet::HE_PerInstanceDataSetFlags::ModelMatrix) {

			indexedVAO.VAOlayout[meshdataLocIndex].second.modelMatricies.erase(indexedVAO.VAOlayout[meshdataLocIndex].second.modelMatricies.begin() + postMaskPropertyIndex);

		}

		//and Material ID
		if (indexedVAO.flags & HE_PerInstanceDataSet::HE_PerInstanceDataSetFlags::MaterialID)
		{
			indexedVAO.VAOlayout[meshdataLocIndex].second.materialIDs.erase(indexedVAO.VAOlayout[meshdataLocIndex].second.materialIDs.begin() + postMaskPropertyIndex);
		}
	}

	indexedVAO.materialBufferNeedsFlushing = true;
}

void VertexDataManager::flushBuffers()
{
	if (indexedVAO.matrixBufferNeedsFlushing)
		processModelMatrixUpdates(&indexedVAO);
	if (indexedVAO.materialBufferNeedsFlushing)
		processMaterialUpdates(&indexedVAO);

}

void VertexDataManager::processMaterialUpdates(VAO *)
{
	GLuint materialIDVBO = 0;


	indexedVAO.fullMaterialBuffer.clear();

	for (auto vec : indexedVAO.VAOlayout)
	{
		indexedVAO.fullMaterialBuffer.insert(indexedVAO.fullMaterialBuffer.end(), vec.second.materialIDs.begin(), vec.second.materialIDs.end());
	}

	if (indexedVAO.fullMaterialBuffer.size() == 0) return;

	std::vector<GLuint> ids;

	//Correct all the indexes to the relevant postions
	for (int i = 0; i < indexedVAO.fullMaterialBuffer.size(); i++)
	{
		ids.push_back( indexedVAO.vaoMaterial->getMaterialBufferIndex(indexedVAO.fullMaterialBuffer[i]));
	}


	//Bind the VAO
	Game::singleton()->getRenderEngine()->bindVAO(indexedVAO.id);

	//Generate a new VBO
	glGenBuffers(1, &materialIDVBO);
	//Bind this VBO
	glBindBuffer(GL_ARRAY_BUFFER, materialIDVBO);

	//Shove some data in it
	glBufferData(GL_ARRAY_BUFFER, ids.size() * sizeof(GLuint), ids.data(), GL_DYNAMIC_DRAW);

	//Enable this vertex attribute
	glEnableVertexAttribArray(5);
	//Tell opengl how I will use this data
	glVertexAttribIPointer(5, 1, GL_UNSIGNED_INT,  0, (void*)0);

	//Only once per instance please
	glVertexAttribDivisor(5, 1);


	glDeleteBuffers(1, &indexedVAO.materialIDVBO);
	indexedVAO.materialIDVBO = materialIDVBO;

	indexedVAO.materialBufferNeedsFlushing = false;
}

void VertexDataManager::processModelMatrixUpdates(VAO* )
{
	GLuint modelMatriciesVBO = 0;

	std::vector<glm::mat4> matriciesToFlush;
	for (auto vec : indexedVAO.VAOlayout)
	{
		matriciesToFlush.insert(matriciesToFlush.end(), vec.second.modelMatricies.begin(), vec.second.modelMatricies.end());
	}

	Game::singleton()->getRenderEngine()->bindVAO(indexedVAO.id);

	//Model Matrix buffer
	glGenBuffers(1, &modelMatriciesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, modelMatriciesVBO);


	glBufferData(GL_ARRAY_BUFFER, (matriciesToFlush.size()) * sizeof(glm::mat4), matriciesToFlush.data(), GL_DYNAMIC_DRAW);

	GLsizei vec4Size = sizeof(glm::vec4);
	glEnableVertexAttribArray(9);
	glVertexAttribPointer(9, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
	glEnableVertexAttribArray(10);
	glVertexAttribPointer(10, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(vec4Size));
	glEnableVertexAttribArray(11);
	glVertexAttribPointer(11, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
	glEnableVertexAttribArray(12);
	glVertexAttribPointer(12, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

	glVertexAttribDivisor(9, 1);
	glVertexAttribDivisor(10, 1);
	glVertexAttribDivisor(11, 1);
	glVertexAttribDivisor(12, 1);

	glDeleteBuffers(1, &indexedVAO.modelMatriciesVBO);
	indexedVAO.modelMatriciesVBO = modelMatriciesVBO;

	indexedVAO.matrixBufferNeedsFlushing = false;
}

GLuint VertexDataManager::getVAOForShaderType(Shader* shader)
{
	//This will return  the VAO for this shader
	return indexedVAO.id;
}

InstancedMaterial* VertexDataManager::getMaterialForShaderType(Shader* shader)
{
	return indexedVAO.vaoMaterial;
}

GLuint VertexDataManager::getBaseInstance(Shader*, MeshData* meshData)
{
	GLuint instanceCount = 0;
	for (auto i : indexedVAO.VAOlayout)
	{
		if (i.first == meshData)
		{
			return instanceCount;
		}
		instanceCount += i.second.modelMatricies.size();
	}
}

GLuint VertexDataManager::getInstanceCount(Shader*, MeshData* meshData)
{
	for (auto i : indexedVAO.VAOlayout)
	{
		if (i.first == meshData)
		{
			return i.second.modelMatricies.size();
		}
	}
}
