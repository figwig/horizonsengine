#include "ImageRect.h"
#include "Game.h"
#include "Material.h"
#include "MaterialHandler.h"


Panel::Panel(Texture* text)
{
	transform->res = text->res;
	texture = text->id;

	applyScaling();

	setup();

	Material* mat = MaterialHandler::singleton()->makeSingleMaterial(text);
	getComponent<RenderConditions>()->setMaterial(mat);
}

Panel::Panel(glm::vec2 r)
{
	//Textureless Panel
	transform->res = r;
	transform->setScale(glm::vec2(0.5f));

	applyScaling();

	setup();
	rc->tintColor = glm::vec4(1, 1, 1, 1);
	rc->setMaterial(MaterialHandler::singleton()->getNullMat());
	rc->tintStrength = 1.0f;

}

Panel::Panel()
{

	transform->res = glm::vec2(1);
	//GUIShader::getInstance()->setScreenPos(glm::translate(glm::mat4(1),glm::vec3((-transform->res/2.0f),0.0f)));

	applyScaling();

	setup();
}

void Panel::setup()
{
	rc = getComponent<RenderConditions>();

	Model* model = new Model();

	model->addMesh(new MeshData(VAO, 6));

	rc->setModel(model);
}

Panel::~Panel()
{
}

void Panel::applyScaling()
{

	for (int i = 0; i< totalPoints/3; i++)
	{
		points[i * 3] = points[i * 3] * (transform->res.x);
		points[(i * 3) + 1] =points[(i * 3) + 1]* (transform->res.y);
	}

	sortVAO();
}


void Panel::sortVAO()
{
	//Setup the VAO
	glGenVertexArrays(1, &VAO);
	Game::singleton()->getRenderEngine()->bindVAO(VAO);

	//Add the points to the VBO
	glGenBuffers(1, &pointsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, pointsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*18, points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &textsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, textsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*12, texts, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);

	GLuint indiciesVBO = 0;

	glGenBuffers(1, &indiciesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, totalPoints/3 * sizeof(GLuint), indicies, GL_STATIC_DRAW);

	Game::singleton()->getRenderEngine()->bindVAO(0);
}

void Panel::draw(RenderSettings*)
{

	glUseProgram(thisConfig.guiShaders->getProgramID());

	glm::mat4 p = transform->getPosMat();

	thisConfig.guiShaders->setScreenPos(p);
	thisConfig.guiShaders->setScaleMat(transform->getScaleMat());
	thisConfig.guiShaders->setProj(transform->getProj());
	
	thisConfig.guiShaders->setColor(rc->tintColor);
	thisConfig.guiShaders->setColorStrength(rc->tintStrength);
	thisConfig.guiShaders->setNoTex(rc->noTexture);

	Game::singleton()->getRenderEngine()->bindVAO(VAO);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	//Depeceated, now using tris
	glDrawElements(GL_TRIANGLES,6 , GL_UNSIGNED_INT, ( void*)0);


}
