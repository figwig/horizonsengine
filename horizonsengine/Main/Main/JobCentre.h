#pragma once
#include <vector>
#include <thread>
#include <atomic>
#include "Job.h"
#include <concurrentqueue.h>
#include <initializer_list>
#include <functional>
#include <mutex>

typedef uint32_t HE_JobScheduleBit;

enum HE_JobScheduleBits
{
	PostUpdate = 0b1,
	Anytime = 0b01
};

class JobCentre;

class Worker
{

private:

	std::atomic<bool> idle = true;

	std::thread thread;

	friend class JobCentre;

	JobCentre* jc = nullptr;

	void Work();
};

class JobReport
{
public:
	bool done = false;
};

class JobCentre
{
public:
	static JobCentre* singleton();
	static void release();

	void QueueJob(std::function<void()> function, HE_JobScheduleBit scheduleBit = HE_JobScheduleBits::Anytime);

	void waitTillAllIdle();

	void clearJobs();

	void DoPostUpdateJobs();

private:
	friend class Worker;
	void Initialize();
	static JobCentre* thisPointer;
	JobCentre();
	~JobCentre();

	std::vector<std::thread> workerThreads;

	std::vector<Worker*> workers;

	std::atomic<bool> isDone;

	moodycamel::ConcurrentQueue<CpuJob> locklessReadyQueue;
	std::vector<CpuJob> postUpdateJobs;

	uint32_t lastJobID = 0;

	int workerWaitDelay = 1;
};

