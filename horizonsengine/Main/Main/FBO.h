#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Texture.h"
#include "PassFlags.h"
#include "DisplayDetails.h"


//Pass Flags
//
//All Objects
//Opaque
//Transparent
//Reflective
//GUI 1
//GUI 2

class GameObject;

class FBOGroup
{
public:
	FBOGroup();
	virtual ~FBOGroup();

	int getTotalPasses() { return bufferCount; }
	virtual void bindFBOByIndex(int i)
	{ 
		bindFrameBuffer(FBOs[i], dimensions[i * 2], dimensions[(i * 2) + 1]);
	}
	glm::mat4 getCameraAngleRequiredByIndex(int i) { return cameraAngles[i]; }
	glm::vec4 getClipPlaneByIndex(int i) { return clipPlanes[i]; }
	bool getClipPlaneUseBoolByIndex(int i) { return FBOs[i]; }

	void setCameraAngle(int i, glm::mat4 newAngle) { cameraAngles[i] = newAngle; }
	virtual void unBind() = 0;
	virtual void update() = 0;

	virtual Texture* getTextureDetailsByIndex(int i) { return nullptr; }
	
	PassFlags flagsWantedByPass = 0;

	virtual bool includeGameObject(GameObject* go) { return true; }

	virtual void prepareToRender() {}

	virtual void finishRender() {}

protected:
	int bufferCount = 0;

	//Arrays to contain relevant FBOs
	static const int MAX_FBOS = 5;
	GLuint FBOs[MAX_FBOS] = { 0 };
	glm::mat4 cameraAngles[MAX_FBOS] = { glm::mat4(1)};
	glm::vec4 clipPlanes[MAX_FBOS] = { glm::vec4(0) };
	bool usePlanes[MAX_FBOS] = { false };
	int dimensions[MAX_FBOS * 2] = { 0 };

	virtual void bindFrameBuffer(GLuint buffer, int width, int height)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, buffer);
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	DisplayDetails* dd = nullptr;
};

class PlainFBO : public FBOGroup
{
public:
	PlainFBO(float width, float height, DisplayDetails * dd)  : dd(dd)
	{
		//Never to be used in GLRenderer, this class simply contains a single FBO
		glGenFramebuffers(1, &FBOs[0]);
		glBindFramebuffer(GL_FRAMEBUFFER, FBOs[0]);
		glDrawBuffer(GL_COLOR_ATTACHMENT0);

		bufferCount = 1;

		glGenTextures(1, &texture);

		glBindTexture(GL_TEXTURE_2D, texture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*)0);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, -720.0f);
		float min;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &min);
		float amount = (glm::min)(4.0f, min);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);

		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);

		dimensions[0] = width;
		dimensions[1] = height;
	}
	virtual ~PlainFBO() 
	{
		glDeleteFramebuffers(1, &FBOs[0]);
		//glDeleteTextures(1, &texture);
	}

	void unBind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, dd->width, dd->height);
	}

	void update()
	{
		//Nothing, just a compulsory override
	}

	GLuint getResult()
	{
		return texture;
	}

protected:
	DisplayDetails* dd = nullptr;
	GLuint texture = 0;
};

class FloatingPointFBO : PlainFBO 
{
public:
	FloatingPointFBO(float width, float height, DisplayDetails* dd) : PlainFBO(width, height, dd)
	{
		//Tunr off clamps for this framebuffer to work
		glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
		//glClampColor(GL_CLAMP_VERTEX_COLOR, GL_FALSE);
		//glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);

		//Never to be used in GLRenderer, this class simply contains a single FBO
		glGenFramebuffers(1, &FBOs[0]);
		glBindFramebuffer(GL_FRAMEBUFFER, FBOs[0]);
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		
		bufferCount = 1;

		glGenTextures(1, &texture);

		glBindTexture(GL_TEXTURE_2D, texture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)0);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, -720.0f);
		float min;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &min);
		float amount = (glm::min)(4.0f, min);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);

		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);

		dimensions[0] = width;
		dimensions[1] = height;
	}
private:

};


class ScreenBuffer : public FBOGroup
{
public:
	ScreenBuffer(PassFlags pf);



	~ScreenBuffer()
	{

	}

	void unBind()
	{
		//nothing
	}

	void update()
	{
		dimensions[0] = dd->width;
		dimensions[1] = dd->height;
	}

private:


};

