#pragma once
#include "Component.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include "PassFlags.h"
#include <vector>
#include <map>
#include "VertexDataManager.h"

class Shader;
class Material;
class Model;

class RenderConditions : public Component
{
public:
	RenderConditions(GameObject* host);
	~RenderConditions();
	void setShader(Shader*);
	Shader* getShader();

	int renderPriority = 0;

	bool containsTransparency = false;
	bool castShadows = true;

	Model* getModel();
	void setModel(Model*);



	Material* getMaterial(int meshIndex = 0);

	//Once material generation is central!!
	//Material * material;

	void setMaterial(Material* mat, int index = 0);

	void setAllMaterials(Material* mat);


	PassFlags renderType = 0;

	float tintStrength = 0.0f;
	bool noTexture = false;
	glm::vec4 tintColor = glm::vec4(0,0,0,1);


private:
	std::vector<Material*> materials;

	Model* model = nullptr;
	bool matSet = false;
	Shader* shaderInUse = nullptr;
	HE_ModelInstanceHandle instanceHandle;

	void submitMaterialIDs();
};

