#pragma once

#include <glm-0.9.9.2\glm\glm.hpp>
class RenderSettings
{
public:
	RenderSettings(glm::mat4 a, bool u, glm::vec4 cp) { cameraMat = a, useClipPlane = u; clipPlane = cp; }

	RenderSettings(glm::mat4 a, bool u, glm::vec4 cp, bool useTan) { cameraMat = a, useClipPlane = u; clipPlane = cp; renderTangents = useTan; }

	RenderSettings(glm::mat4 a, bool u, glm::vec4 cp, bool useTan, bool useNorm) { cameraMat = a, useClipPlane = u; clipPlane = cp; renderTangents = useTan; renderNormals = useNorm; }

	~RenderSettings(){}

	glm::mat4 cameraMat;
	//Used for shadows
	glm::mat4 projection;

	bool useClipPlane;
	glm::vec4 clipPlane;

	bool renderTangents = false;
	bool renderNormals  = false;
	bool materialsOn = true;
};

