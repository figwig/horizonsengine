#pragma once
#include "Object3D.h"
#include "ImageRect.h"
#include "ShaderManager.h"
#include "WorldGUIShader.h"

class WorldGUI :
	public Object3D, public Panel
{
public:
	WorldGUI(Texture* texture);
	~WorldGUI();
	void update(double);
	void draw(RenderSettings*);

	static const int unitsPerPixel = 10;

	void setup()
	{
		Object3D::setup();
		Panel::setup();

		getComponent<RenderConditions>()->setShader(ShaderManager::singleton()->getShader<WorldGUIShader*>());
	}
protected:
	void applySizing();

	
};

