#pragma once
#include "GlobalBehaviour.h"
#include "Game.h"

class WireframeOnkey
	: public GlobalBehaviour
{
public:
	WireframeOnkey() {

	}
	~WireframeOnkey() { }

	void update(double x)
	{
		if (timer >= 0.0) timer -= x;

		if (target->getKeys() & Keys::F6 && timer<= 0.0)
		{
			DebugSettings * ds = target->getConfig().debugSettings;

			ds->wireFrame = !ds->wireFrame;

			timer = 200.0;
		}
	}

private:

	Keystate  k = Keys::F6;

	double timer = 0.0;
};