#pragma once
#include "VAOData.h"
#include "AssimpContainer.h"
#include "VAOData.h"
#include <map>

class ModelManager
{
public:
	static Model* getModel(std::string s);

	static void setup();

	static void cleanup();

private:
	ModelManager() { }
	~ModelManager() { }

	static AssimpContainer* ac;

	//Filenames/shape types saved here with their models
	static std::map < std::string, Model*> modelMap;

	static void basicShapesSetup();
};

