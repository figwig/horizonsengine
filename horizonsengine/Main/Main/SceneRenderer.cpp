#include "SceneRenderer.h"



SceneRenderer::SceneRenderer()
{
}


SceneRenderer::~SceneRenderer()
{
}

void SceneRenderer::bindVAO(GLuint VAO)
{
	if (VAO == boundVAO) return;
	boundVAO = VAO;
	glBindVertexArray(VAO);
}

GLuint SceneRenderer::getBoundVAO()
{
	return boundVAO;
}

