#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Material.h"

class TerrainTextureData : public Material
{
public:
	GLuint bases[5];
	GLuint normals[5];
	int normalInvertY[5] = { -1 };
	GLuint specs[5];
	GLuint splat;

	void Bind()
	{
		for (int count2 = 0; count2 < 4; count2++)
		{
			if (bases[count2] == 0) continue;
			glActiveTexture(GL_TEXTURE0 + count2);
			glBindTexture(GL_TEXTURE_2D, bases[count2]);
		}
		for (int count2 = 0; count2 < 4; count2++)
		{
			if (normals[count2] == 0) continue;
			glActiveTexture(GL_TEXTURE4 + count2);
			glBindTexture(GL_TEXTURE_2D, normals[count2]);
		}
		for (int count2 = 0; count2 < 4; count2++)
		{
			if (specs[count2] == 0) continue;
			glActiveTexture(GL_TEXTURE8 + count2);
			glBindTexture(GL_TEXTURE_2D, specs[count2]);
		}
		glActiveTexture(GL_TEXTURE12);
		glBindTexture(GL_TEXTURE_2D, splat);
	}
};