#include "Clock.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "DebugCentre.h"
#include <Windows.h>

Clock::Clock()
{
	updateWatchLoc = DebugCentre::singleton()->addWatch(0.0f, "Update Time");
	renderWatchLoc = DebugCentre::singleton()->addWatch(0.0f, "Render Time");
	physicsWatchLoc = DebugCentre::singleton()->addWatch(0.0f, "Physics Time");
	flushWatchLoc = DebugCentre::singleton()->addWatch(0.0f, "Flush Time");
}

void Clock::tick()
{
	ticks = glfwGetTime()*1000.0f;
	delta = ticks - lastTime;

	delay();
	lastTime = ticks;

	frames++;

	ticksCounter += delta;

	frameCounter++;

	avgFPS = frames / (ticks/1000.0f);

	if (ticksCounter > 10000.0f)
	{
		avgFPS10 = frameCounter / 10.0f;
		ticksCounter = 0.0f;
		frameCounter = 0.0f;
	}

	//every 60 frames, set the avgDelta
	frameTicker++;
	avgDeltaCounter += delta;

	if (frameTicker == 60)
	{
		frameTicker = 0;
		avgDelta = avgDeltaCounter / 60;
		avgDeltaCounter = 0;
	}

	if (delta > 40.0f)delta = 40.0f;
	if (avgDelta > 40.0f)avgDelta = 40.0f;
}

void Clock::end()
{
	std::cout << "Average FPS: " << avgFPS<<"\n" ;
	std::cout << "Average FPS per 10 second interval: " << avgFPS10<<"\n" ;
}

void Clock::delay()
{
	if (delta < target)
	{
		Sleep(target - delta);
	}
}

void Clock::writeTimeForStep(int id)
{
	switch (id)
	{
	case CLOCK_PHYSICS:
		if (physicsStarted)
		{
			DebugCentre::singleton()->updateWatch(physicsWatchLoc, (float)(glfwGetTime() * 1000.0f - physicsStart));
			physicsStarted = false;
		}
		else
		{
			physicsStart = glfwGetTime() * 1000.0f;
			physicsStarted = true;
		}
		break;
	case CLOCK_UPDATE:
		if (updateStarted)
		{

			DebugCentre::singleton()->updateWatch(updateWatchLoc, (float)(glfwGetTime() * 1000.0f - updateStart));
			updateStarted = false;
		}
		else
		{
			updateStart = glfwGetTime() * 1000.0f;
			updateStarted = true;
		}
		break;
	case CLOCK_RENDER:
		if (renderStarted)
		{
			double ticks = glfwGetTime() * 1000.0f;
			DebugCentre::singleton()->updateWatch(renderWatchLoc, (float)(ticks - renderStart));
			renderStarted = false;
		}
		else
		{
			renderStart = glfwGetTime() * 1000.0f;
 			renderStarted = true;
		}
		break;
	case CLOCK_FLUSH:
		if (flushStarted)
		{
			double ticks = glfwGetTime() * 1000.0f;
			DebugCentre::singleton()->updateWatch(flushWatchLoc, (float)(ticks - flushStart));
			flushStarted = false;
		}
		else
		{
			flushStart = glfwGetTime() * 1000.0f;
			flushStarted = true;
		}
		break;
	}
}
	



