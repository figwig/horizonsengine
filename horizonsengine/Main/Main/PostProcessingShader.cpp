#include "PostProcessingShader.h"


PostProcessingShader* PostProcessingShader::thisPointer = nullptr;

PostProcessingShader::PostProcessingShader(int x)
{
	programID = setupShaders(vertexFilename, fragFilename);
	setup();
}


PostProcessingShader::PostProcessingShader()
{

}


PostProcessingShader::~PostProcessingShader()
{
	if (this == thisPointer)
	{
		thisPointer = nullptr;
	}
}


PostProcessingShader * PostProcessingShader::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new PostProcessingShader(0);
	}
	return thisPointer;
}

void PostProcessingShader::setup()
{

}
