#pragma once
#include "GlobalBehaviour.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include "InputListener.h"
#include "Game.h"
#include "PrivateBehaviour.h"

class MouseRaycaster : public GlobalBehaviour, public InputListener
{
public:
	MouseRaycaster();
	~MouseRaycaster();

	void mouseMovePos(float x, float y);
	glm::vec3 getMousePosWorld();

	GameObject* getObjectUnderMouse();

	void update(double) {}
private:
	float lastX;
	float lastY;

	DisplayDetails* dd = nullptr;

	int watchLoc = 0;
	int mouseLoc = 0;
};



class MousePickTest : public PrivateBehaviour {
public:
	MousePickTest(GameObject* go) 
	{
		mrc = new MouseRaycaster();
	}
	~MousePickTest()
	{
		delete mrc;
	}

	void update(double)
	{
		glm::vec3 newPos = mrc->getMousePosWorld();
		target->setPos(newPos);
	}
private:

	MouseRaycaster* mrc = nullptr;
};