#pragma once
#include "shader_setup.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <vector>
#include <glm-0.9.9.2/glm/glm.hpp>

class GameObject;
class RenderSettings;
class LightingState;

class Shader
{
public:
	Shader();
	virtual ~Shader();
	virtual void setup() = 0;
	GLuint getProgramID();
	void setClipPlane(glm::vec4, bool);
	virtual void newFrame() { newFrameBool = true; }

	virtual void prepareToRender(GameObject*) {}
	virtual void prepareToRender(RenderSettings*) {}

	virtual void setLighting(float shinyness, LightingState*) {}

	virtual void setTextures() {}

	//Will be replaced with setup material or something
	virtual void setNormalBool(bool) {}
	virtual GLuint getPrimitiveType() { return GL_TRIANGLES; }

	virtual void rebuild() {}

	virtual void setInvertY(bool x) {}

	
	virtual bool getInstancedBool() { return false; }

	virtual std::vector<GLuint> getBindlessLocations() { return bindlessHandles; }
protected:
	int maxTexUnits;

	const static int MAX_LIGHTS = 20;
	const static int PARAMS_PER_LIGHT = 10;
	GLuint programID;
	GLuint getLightUniformLocationName(int count, int index);

	std::string propertyNames[PARAMS_PER_LIGHT] =
	{
		"position",
		"colors",
		"attenuation",
		"ambientCoefficient",
		"coneAngle",
		"coneDirection",
		"finalLightMatrix",
		"shadowMap",
		"castShadows",
		"shadowMatrix"
	};

	GLuint locCP;

	bool newFrameBool = false;

	GLuint locCPbool;

	std::vector<GLuint> bindlessHandles;
};

