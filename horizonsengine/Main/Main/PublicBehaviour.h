#pragma once
#include "Behaviour.h"
class Scene;
class GameObject;
class PublicBehaviour :public  Behaviour
{
public:
	PublicBehaviour() {}
	~PublicBehaviour() {}
	void setTarget(Scene* newTarget) { scene = newTarget; }
	virtual void update(double) = 0;

protected:
	Scene * scene;
};
