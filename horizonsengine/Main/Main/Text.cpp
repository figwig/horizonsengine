#include "Text.h"
#include <glm-0.9.9.2/glm/ext.hpp>
#include "Game.h"
#include "TextShaders.h"
#include "TextRenderingComponent.h"


TextObject::TextObject(std::map<GLchar, Character> f)
{
	

	initTransform();
	sortVAO();
	font = f;
	transform->anchorType = GUI_TRANSFORM_ANCHOR_TL;

	RenderConditions* rc = getComponent<RenderConditions>();

	rc->setShader( TextShaders::getInstance());

	m = new Model();
	m->addMesh(TextMeshData::getTextMeshData());

	rc->setModel( m);



	rc->renderType = PassFlagTypes::GUI2 | PassFlagTypes::Text;

	generateVAO();

	rc->setMaterial(TextMaterial::getTextMaterial());

	trc = new TextRenderingComponent(this);

	addComponent(trc);

}


TextObject::~TextObject()
{
	delete m;
}

void TextObject::update(double)
{

}
void TextObject::setColour(glm::vec3 c)
{
	trc->colour = c;
}


void TextObject::draw()
{
#pragma region Old Draw Method
//	float minY = 0.0f;
//
//	DisplayDetails * dd = Game::singleton()->getDisplayDetails();
//
//	float x = transform->getPos().x;
//
//
//
//	if ((transform->anchorType == GUI_TRANSFORM_ANCHOR_CEN )&& centred)
//	{
//		x -= ( abs(transform->res.x)) / 2.0f;
//
//	}
//
//	float startX = x;
//
//	float y = transform->getPos().y -(yOffset*transform->getRelativeScale().y);
//
//	glUseProgram(thisConfig.textShaders->getProgramID());
//
//	glEnable(GL_BLEND);
//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//
//	thisConfig.textShaders->setProj(transform->getProj());
//
//	thisConfig.textShaders->setColour(trc->colour);
//
//	glActiveTexture(GL_TEXTURE0);
//	glBindVertexArray(VAO);
//
//	// Iterate through all characters
//	std::string::const_iterator c;
//
//	for (c = text.begin(); c != text.end(); c++)
//	{
//		Character ch = font[*c];
//
//		GLfloat xpos = x + ch.Bearing.x * transform->getRelativeScale().x;
//		GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * transform->getRelativeScale().y;
//
//		GLfloat w = ch.Size.x * transform->getRelativeScale().x;
//		GLfloat h = ch.Size.y * transform->getRelativeScale().y;
//		// Update VBO for each character
//		GLfloat vertices[6][4] = {
//		{ xpos,     ypos + h,   0.0, 0.0 },
//		{ xpos,     ypos,       0.0, 1.0 },
//		{ xpos + w, ypos,       1.0, 1.0 },
//
//		{ xpos,     ypos + h,   0.0, 0.0 },
//		{ xpos + w, ypos,       1.0, 1.0 },
//		{ xpos + w, ypos + h,   1.0, 0.0 }
//		};
//		// Render glyph texture over quad
//		glBindTexture(GL_TEXTURE_2D, ch.textureID);
//		// Update content of VBO memory
//		glBindBuffer(GL_ARRAY_BUFFER, VBO);
//		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//		// Render quad
//		glDrawArrays(GL_TRIANGLES, 0, 6);
//		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
//		x += (ch.Advance >> 6) * transform->getRelativeScale().x; // Bitshift by 6 to get value in pixels (2^6 = 64)
//
//		minY = (glm::min)(ypos, minY);
//	}
//	glBindVertexArray(0);
//	glBindTexture(GL_TEXTURE_2D, 0);
//
//	glDisable(GL_BLEND);
//
//	if(centred)
//	transform->res = glm::vec2(abs(x- startX), abs(y- minY));
#pragma endregion
	//Text mk 2

	glUseProgram(thisConfig.textShaders->getProgramID());

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	thisConfig.textShaders->setProj(transform->getProj());

	thisConfig.textShaders->setColour(trc->colour);

	glActiveTexture(GL_TEXTURE0);
	Game::singleton()->getRenderEngine()->bindVAO(VAO);

	for (RenderCharacter rc : characters)
	{
		glBindTexture(GL_TEXTURE_2D, rc.ch.textureID);

		glDrawArrays(GL_TRIANGLES, rc.index,  6);
	}
}

void TextObject::sortVAO()
{
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	Game::singleton()->getRenderEngine()->bindVAO(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4 * maxCharacters, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void TextObject::generateVAO()
{
	characters.clear();

	points.clear();

	//Text has changed, generate a VAO for each character
	float minY = 0.0f;
	DisplayDetails* dd = Game::singleton()->getDisplayDetails();
	float x = transform->getPos().x;
	if ((transform->anchorType == GUI_TRANSFORM_ANCHOR_CEN) && centred)
	{
		x -= (abs(transform->res.x)) / 2.0f;
	}

	float startX = x;

	float y = transform->getPos().y - (yOffset * transform->getRelativeScale().y);

	std::string::const_iterator c;

	int index = 0;

	for (c = text.begin(); c != text.end(); c++)
	{	// Iterate through all characters
		GLuint VAO = 0;

		Character ch = font[*c];

		GLfloat xpos = x + ch.Bearing.x * transform->getRelativeScale().x;
		GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * transform->getRelativeScale().y;

		GLfloat w = ch.Size.x * transform->getRelativeScale().x;
		GLfloat h = ch.Size.y * transform->getRelativeScale().y;

		points.push_back(xpos); points.push_back(ypos+h); points.push_back(0.0f); points.push_back(0.0f);

		points.push_back(xpos); points.push_back(ypos); points.push_back(0.0f); points.push_back(1.0f);

		points.push_back(xpos+w); points.push_back(ypos); points.push_back(1.0f); points.push_back(1.0f);

		points.push_back(xpos); points.push_back(ypos + h); points.push_back(0.0f);	points.push_back(0.0f);

		points.push_back(xpos+w); points.push_back(ypos); points.push_back(1.0f); points.push_back(1.0f);

		points.push_back(xpos + w); points.push_back(ypos + h); points.push_back(1.0f); points.push_back(0.0f);


		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		x += (ch.Advance >> 6) * transform->getRelativeScale().x; // Bitshift by 6 to get value in pixels (2^6 = 64)

		minY = (glm::min)(ypos, minY);


		RenderCharacter rc = RenderCharacter(ch);
		rc.index = index * 6;

		characters.push_back(rc);
		index++;
	}
	Game::singleton()->getRenderEngine()->bindVAO(VAO);
	// Update content of VBO memory
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, points.size() * sizeof(GLfloat), points.data());
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	if (centred)
		transform->res = glm::vec2(abs(x - startX), abs(y - minY));
}