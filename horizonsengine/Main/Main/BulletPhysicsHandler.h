#pragma once
#include "PhysicsHandler.h"
#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>
#include <map>

class BulletPhysicsHandler :
	public PhysicsHandler
{
public:
	static BulletPhysicsHandler* getInstance();

	void doPhysics(double, Scene*);
	void doLatePhysics(double, Scene*);

	void checkScene(Scene*);

	RayCastResult castRayDown(glm::vec3 position);
	RayCastResult castRay(glm::vec3 position, glm::vec3 towards);

	bool useDefaultColliderTypes() { return false; }

	void addToSimulation(PhysicsComponent*);

	void removeFromSimulation(PhysicsComponent *);

private:
	void setup();
	static BulletPhysicsHandler * thisPointer;
	BulletPhysicsHandler();
	~BulletPhysicsHandler();

	std::vector<PhysicsComponent*> components;

	btDiscreteDynamicsWorld * dynWorld;
	btDefaultCollisionConfiguration * config;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver * solver;
	btDbvtBroadphase * overlappingPairCache;

	std::map<const btCollisionShape *, GameObject*> colObjects;
};

