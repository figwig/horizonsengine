#pragma once
#include "SceneRenderer.h"
#include "Material.h"
#include "RenderGroupClasses.h"
#include "PostProccessingFBO.h"
#include <map>
#include "PostProcessRect.h"

class Shader;

typedef RenderGroup<GameObject, MeshData> ModelData;

typedef RenderGroup<ModelData, Material> MaterialData;

typedef RenderGroup<MaterialData,Shader> ShaderData;

typedef RenderGroup<ShaderData, FBOGroup> PassData;

typedef RenderGroup<PassData,Scene> RenderData;


typedef struct {
	GLuint  elementsCount;
	GLuint  instanceCount;
	GLuint  firstIndex;
	GLuint  baseVertex;
	GLuint  baseInstance;
} DrawElementsIndirectCommand;


class GLRenderer : public SceneRenderer
{
public:

	static GLRenderer* singleton();

	void update(double);

	void render();

	void initialize();

	void addPostProcessor(PostProcessingShader*);

	void removePostProcessor(PostProcessingShader*);

	void onAddGameObject(GameObject* go);

	void setScene(Scene* scene);

	void onAddFBO(FBOGroup*, Scene * scene);

	void checkForLights();

private:

	static GLRenderer* thisPointer;

#pragma region Indirect Rendering Groups
	GLuint indirectBuffer = 0;

	DrawElementsIndirectCommand drawCommands[4096];

	GLuint baseVert = 0;

	int drawCount = 0;

	std::map<Shader*, std::vector<ModelData*> > instancedModelData;
#pragma endregion

#pragma region Current Pass References
	Shader* currentShader = nullptr;

	PassData* currentPassData = nullptr;

	RenderSettings* rs = nullptr;
#pragma endregion

#pragma region Render Process Methods
	void renderPass(RenderData*, bool binding = false);

	void renderPassData(PassData*, int i, bool binding = false);

	void renderShadows();

	void renderShaderData(ShaderData*);

	void renderMaterialData(MaterialData*);

	void renderModelData(ModelData*);

	void renderInstanced(ShaderData *);

	void doPostProcessing();
#pragma endregion


	//A group of postprocessing FBOs and screen buffers that are the same for each pass, regardless of scene
#pragma region MultiSampling and Post Processing Stuff
	PostProccessingFBO* msFBO = nullptr;

	PostProccessingFBO* ppFBO = nullptr;

	PostProcessRect* finalRender = nullptr;

#pragma region Post Processing
	const static int MAX_PP_SHADERS = 5;

	int ppCount = 0;

	std::vector<PostProccessingFBO*> auxPPFBOs;
	std::vector<PostProcessRect*> ppRects;

	void textureChainPP();

#pragma endregion
#pragma endregion

#pragma region Base Render Passes
	//Render passes requested by the scene, containing references to all objects involved.
	RenderData *masterRenderPass;
	PassData* masterPassData;
	
	///Pass order
	RenderData *FBOPasses;
	RenderData* shadowPasses;
	PassData* opaquePass;
	PassData* transparentPass;
	PassData* GUI1;
	bool GUI1NeedsSort = true;
	PassData* GUI2;
	bool GUI2NeedsSort = true;

	std::multimap<Light*, PassData*> shadowData;
	Shader* shaderOverride = nullptr;

#pragma endregion

	GLRenderer();

	~GLRenderer();

	void addToPassData(PassData*, GameObject*, RenderConditions * );

	void addToShaderData(ShaderData*, GameObject*, RenderConditions*);

	void addToMaterialData(MaterialData*, GameObject*, RenderConditions*, int meshIndex);

	void addToModelData(ModelData*, GameObject*);
	//Temp
	ScreenBuffer* screenBuffer = nullptr;

	RenderSettings* postProcessRenderSettings = nullptr;

	void addObjectsToPassDataFBO(PassData*, Scene* scene);

	int passesAddedTo = 0;

	void renderNormal(ModelData *);
};