#include <JobCentre.h>
#include <iostream>

class Tester
{
public:
	Tester()
	{
		myID = ID++;
	}
	~Tester() {}
	void SomeWork(int, int)
	{
		std::cout << "I'm Cool" << ID << std::endl;
	}

	static int ID;

	int myID = 0;
};


int Tester::ID = 0;



int main()
{
	Tester* t = new Tester();
	Tester* t2 = new Tester();

	JobCentre::singleton()->QueueJob([&] {t->SomeWork(0, 0); });
	JobCentre::singleton()->QueueJob([&] {t2->SomeWork(0, 0); });
	JobCentre::singleton()->QueueJob([&] {t2->SomeWork(0, 0); });
	JobCentre::singleton()->QueueJob([&] {t2->SomeWork(0, 0); });
	JobCentre::singleton()->QueueJob([&] {t->SomeWork(0, 0); });
	JobCentre::singleton()->QueueJob([&] {t->SomeWork(0, 0); });
	JobCentre::singleton()->QueueJob([&] {t->SomeWork(0, 0); });

	while (true);

	return 0;
}




