#pragma once
#include "TerrainNoiseShaders.h"
#include <glfw/glfw3.h>
#include <FBO.h>
#include <Game.h>
#include <PostProcessRect.h>
#include <vector>
#include <PublicBehaviour.h>


struct PlanetGenProperties
{
	float oceanLevel = 0.1f;
};


class ComputeNoiseGenerator : public PublicBehaviour
{
public:
	ComputeNoiseGenerator(PlanetGenProperties pgp);


	~ComputeNoiseGenerator()
	{
		delete tns;

		delete fbo;
	}

	void Generate();

	GLuint getResult();


	void update(double)
	{
		Keystate k = Game::singleton()->getKeys();

		if (k & Keys::F8)
		{
			tns->rebuild();

			Generate();
		}


		//if (k & Keys::Space)
		//{
		//	Generate();
		//	x += Game::singleton()->getClock()->getDelta() /2000.0f;
		//	if (x > loopLimit)
		//		x = 0.8f;
		//}
	}

private:
	PlanetGenProperties pgp;
	TerrainNoiseShaders* tns = nullptr;
	PostProcessRect * ppr = nullptr;

	PlainFBO* fbo;

	GLuint result = 0;
	void setupShader();

	float x = 1.0f;
	float loopLimit = 1.9f;


	GLuint UBOHandle = 0;
	int maxLayerCount = 20;
	int currentLayerCount = 0;

	struct NoiseLayer
	{
		float coordMod = 0.0f;
		float exponent = 0.0f;
		float scale = 0.0f;
		float constructive = 0.0f;

		float scaleDependsOn = -1.0f;
		float pad1;
		float pad2;
		float active = 1.0f;
	};

	std::vector<NoiseLayer> noiseLayers;

	void updateUBO();
};

