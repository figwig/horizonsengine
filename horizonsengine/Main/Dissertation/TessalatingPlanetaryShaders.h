#pragma once
#include "PlanetShaders.h"

class TessalatingPlanetaryShaders :
	public PlanetShaders
{
public:
	static TessalatingPlanetaryShaders* getInstance() {
		if (thisPointer == nullptr)
		{
			thisPointer = new TessalatingPlanetaryShaders();
		}
		return thisPointer;
	}

	GLuint getPrimitiveType() { return GL_PATCHES; }

	void prepareToRender(GameObject* go);
	void setup();

	void rebuild();

	void build();

private:
	static TessalatingPlanetaryShaders* thisPointer;
	TessalatingPlanetaryShaders();

	std::string vertexFilePath = "Shaders/t_vertex_shader_planet.shader";
	std::string fragmentFilePath = "Shaders/t_fragment_shader_planet.shader";
	std::string tcsFilePath = "Shaders/tcs_planet.shader";
	std::string tesFilePath = "Shaders/tes_planet.shader";

	GLuint locRadius = 0;
	GLuint locOrigin = 0;
};

