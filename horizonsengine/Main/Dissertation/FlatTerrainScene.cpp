#include "FlatTerrainScene.h"
#include "PlanetFactory.h"
#include <SimpleCamera.h>
#include <Game.h>
#include <MoonLight.h>
#include <Scenery.h>
#include <Skybox.h>
#include <smallSpotLight.h>
#include "PlanetaryController.h"
#include <FirstPersonCamera.h>
#include <Rotator.h>
#include "Sun.h"
#include <RebuildShader.h>
#include <ShaderManager.h>
#include "TessalatingPlanetaryShaders.h"
#include <ModelManager.h>
#include "PlanetCamera.h"

FlatTerrainScene::FlatTerrainScene()
{
	name = "FlatTerrainScene";
}

FlatTerrainScene::~FlatTerrainScene()
{
}

void FlatTerrainScene::run()
{
	gameRefr->setPlaying(true);
}

void FlatTerrainScene::load(Game* g)
{
	gameRefr = g;

	loadInit();

	LoadPlanet();

	Sun* sun = new Sun(glm::vec3(0.8f, 0.2f, 0.2f));


	currLighting->addLight(sun);


	smallSpotLight* ssl = new smallSpotLight(glm::vec3(2.0f, 2.0f, 2.3f), glm::vec3(2408.0f, 1080.0f, -2495.0f));

	//currLighting->addLight(ssl);

	ssl->attenuation = 0.0f;
	ssl->coneAngle = 100;

	Skybox* sky = new Skybox(1);

	addGameObject(sky);

	HiResSphere();

	Player();

	gameRefr->addGlobalBehaviour(new RebuildShader(ShaderManager::singleton()->getShader<TessalatingPlanetaryShaders*>()));

	gameRefr->setConfigAll();
}

void FlatTerrainScene::LoadPlanet()
{

}

void FlatTerrainScene::Player()
{
	Model* cube = ModelManager::getModel("Objects/cube.obj");

	Custom* cube1 = new Custom(cube, glm::vec3(0.0f), glm::vec3(0.1f), glm::vec3(0.0f, 0.0f, 0.0f));

	addGameObject(cube1);

	player = cube1;

	//Make a player controller
	PlanetaryController* controller = new PlanetaryController(planet, gameRefr, 2000, 0.8f, 1800, 0.01f);

	player->addPrivateBehaviour(controller);

	//	Camera* newCamera = new PlanetCamera(planet, player);
	Camera* newCamera = new FirstPersonCamera(player);
	gameRefr->setCamera(newCamera);
}

void FlatTerrainScene::HiResSphere()
{
	Model* sphere = ModelManager::getModel("Objects/HiResSphere.obj");

	Custom* sphereGO = new Custom(sphere, glm::vec3(25, 2, 0), glm::vec3(0.05f), glm::vec3(0, -1.57f, 0));

	sphereGO->name = "That Sphere";

	addGameObject(sphereGO);
}