#include "Planet.h"

Planet::Planet(float radius)
{
	setPos(glm::vec3(0));
	setRot(glm::vec3(0));
	setScale(glm::vec3(1));

	addComponent(new PlanetRenderingComponent(this, radius));
}

Planet::~Planet()
{
}
