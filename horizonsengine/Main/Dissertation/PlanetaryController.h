#pragma once
#include <PlayerController.h>
#include <glm-0.9.9.2/glm/gtc/matrix_transform.hpp>
#include <Game.h>

class PlanetaryController : public PlayerController
{
public:
	PlanetaryController(GameObject* planet, Game * refr, float evenRatioDistance, float startingSpeed, float planetRadius, float minSpeed) : PlayerController(refr), planet(planet), startingSpeed(startingSpeed), evenRatioDistance(evenRatioDistance), radius(planetRadius), minSpeed(minSpeed)
	{
		setSpeed(startingSpeed);
	}
	~PlanetaryController() {}
	
	void update(double delta)
	{
		PlayerController::update(delta);

		float distance = glm::distance(target->getPos() , planet->getPos());

		setSpeed(startingSpeed * ((distance- radius)/evenRatioDistance)+ minSpeed);
	}
private:
	GameObject* planet = nullptr;

	float startingSpeed = 0.0f;

	float radius = 0.0f;

	float evenRatioDistance = 0.0f;

	float minSpeed = 0.0f;
};

