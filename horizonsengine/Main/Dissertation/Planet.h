#pragma once
#include <Object3D.h>
#include <Material.h>


class PlanetMaterial : public Material
{
public:
	PlanetMaterial(Texture * t, Texture * hMap, Texture * spec, Texture * normal, Texture * wf, Texture * ws)
	{
		useNormal = false;
		basicTexture = t;
		heightMap = hMap;
		normalMap = normal;
		specularMap = spec;
		waterFar = wf;
		waterFarSpec = ws;
		shininess = 20;
	}

	~PlanetMaterial() {}

	void Bind() 
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, basicTexture->id);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, specularMap->id);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, normalMap->id);
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, waterFar->id);
		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, waterFarSpec->id);
		glActiveTexture(GL_TEXTURE15);
		glBindTexture(GL_TEXTURE_2D, heightMap->id);
	}

	Texture* basicTexture = nullptr;
	Texture* specularMap = nullptr;
	Texture* normalMap = nullptr;
	Texture* heightMap = nullptr;
	Texture* waterFar = nullptr;
	Texture* waterFarSpec = nullptr;
};



class Planet : public Object3D
{
public:
	Planet(float radius);

	~Planet();

};

class PlanetRenderingComponent : public Component
{
public:
	PlanetRenderingComponent(Planet* planet, float radius) : Component(planet), radius(radius) {}

	float radius = 0.0f;

	bool tilingOn = false;

	float getTilingFactor()
	{
		//Assumes texture is 1x1m
		if (!tilingOn) return 1.0f;
		return radius * 1.0f * M_PI;
	}
};
