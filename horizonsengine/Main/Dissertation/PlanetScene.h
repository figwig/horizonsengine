#pragma once
#include "Planet.h"
#include <Scene.h>

class PlanetScene : public Scene
{
public:
	PlanetScene();

	~PlanetScene();

	void run();

	void load(Game*);

private:

	void LoadPlanet();

	void Player();

	void HiResSphere();

	GameObject* player = nullptr;

	Planet* planet = nullptr;
};

