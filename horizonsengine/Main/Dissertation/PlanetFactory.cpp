#include "PlanetFactory.h"
#include <VertexDataManager.h>
#include <TextureExaminer.h>
#include "PitchOnKey.h"
#include <ShaderManager.h>
#include "PlanetShaders.h"
#include <future>
#include <iostream>
#include <ModelManager.h>
#include <vector>
#include "TessalatingPlanetaryShaders.h"
#include <glfw/glfw3.h>
#include "ComputeNoiseGenerator.h"

PlanetFactory* PlanetFactory::thisPointer = nullptr;

PlanetFactory::PlanetFactory()
{
	pnGen = new PerlinNoise();
}

PlanetFactory* PlanetFactory::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new PlanetFactory();
	}
	return thisPointer;
}

PlanetFactory::~PlanetFactory()
{
}

Planet* PlanetFactory::makeTestPlanet(float radius)
{
	planetSettings = PlanetSettings();

	planetSettings.active = false;

	Planet* planet = new Planet(radius);

	RenderConditions* rc = planet->getComponent<RenderConditions>();

	rc->setModel(sortModel(radius));

	TextureExaminer::singleton()->setDirectory("Textures/");

	rc->setShader( ShaderManager::singleton()->getShader<PlanetShaders*>());


	Texture* t = TextureExaminer::singleton()->getTexture("earthLowerRes.png");
	Texture* n = TextureExaminer::singleton()->getNormalTexture("earth_normalmap.png");
	Texture* s = TextureExaminer::singleton()->getTexture("earthSpec.jpg");
	Texture* w = TextureExaminer::singleton()->getTexture("waterFar.jpg");
	Texture* ws = TextureExaminer::singleton()->getTexture("waterFarSpec.jpg");

	rc->setAllMaterials(new PlanetMaterial(t,t,s, n,w, ws));

	planet->addPrivateBehaviour(new PitchOnKey(Keys::e, 0.002f, glm::vec3(1, 0, 0)));

	return planet;
}

Planet* PlanetFactory::makeTerrainedPlanet(float radius, std::string diff, std::string spec, std::string norm)
{
	planetSettings = PlanetSettings();

	planetSettings.active = false;

	Planet* planet = new Planet(radius);

	RenderConditions* rc = planet->getComponent<RenderConditions>();

	rc->setShader(ShaderManager::singleton()->getShader<TessalatingPlanetaryShaders*>());

	planet->getComponent<PlanetRenderingComponent>()->tilingOn = true;

	rc->setModel( sortModel(radius));

	TextureExaminer::singleton()->setDirectory("Textures/");

	Texture* t = TextureExaminer::singleton()->getTexture(diff);
	Texture* n = TextureExaminer::singleton()->getNormalTexture(norm);
	Texture* s = TextureExaminer::singleton()->getTexture(spec);
	Texture* w = TextureExaminer::singleton()->getTexture("waterFar.jpg");	Texture* ws = TextureExaminer::singleton()->getTexture("waterFarSpec.jpg");

	rc->setAllMaterials(new PlanetMaterial(t, t, s, n,w,ws));

	planet->addPrivateBehaviour(new PitchOnKey(Keys::e, 0.002f, glm::vec3(1, 0, 0)));

	return planet;
}

Planet* PlanetFactory::makePreGenPlanet(float radius, std::string diff, std::string spec, std::string norm)
{
	ComputeNoiseGenerator* cng = new ComputeNoiseGenerator(PlanetGenProperties());

	cng->Generate();

	GLuint tex = cng->getResult();

	Game::singleton()->currentScene()->addPublicBehaviour(cng);

	DebugCentre::singleton()->addDebugTexture(new Texture(tex, glm::vec2(250), "Generated terrain"));


	planetSettings = PlanetSettings();

	planetSettings.active = false;

	Planet* planet = new Planet(radius);

	RenderConditions* rc = planet->getComponent<RenderConditions>();

	rc->setShader(ShaderManager::singleton()->getShader<TessalatingPlanetaryShaders*>());

	planet->getComponent<PlanetRenderingComponent>()->tilingOn = true;

	planet->setScale(glm::vec3(radius));

	Model* model = ModelManager::getModel("Objects/GeoSphere.obj");

	calcTexCoords(model);

	rc->setModel(model);

	TextureExaminer::singleton()->setDirectory("Textures/");

	Texture* t = TextureExaminer::singleton()->getTexture(diff);
	Texture* n = TextureExaminer::singleton()->getNormalTexture(norm);
	Texture* s = TextureExaminer::singleton()->getTexture(spec);
	Texture* w = TextureExaminer::singleton()->getTexture("waterFar.jpg");	
	Texture* ws = TextureExaminer::singleton()->getTexture("waterFarSpec.jpg");

	Texture* h = new Texture(tex);

	rc->setAllMaterials(new PlanetMaterial(t, h, s, n,w,ws));

	planet->addPrivateBehaviour(new PitchOnKey(Keys::e, 0.002f, glm::vec3(1, 0, 0)));

	return planet;
}

Model* PlanetFactory::sortModel(float r)
{
	radius = r;

	//Do some maths bitch
	initialize_sphere(4,3);

	Model* model = new Model();

	meshData->setVAO(VertexDataManager::getInstance()->addMeshToVAO(meshData, false));

	model->addMesh(meshData);

	return model;
}

glm::vec2 PlanetFactory::calculateTextureCoordinate(glm::vec3 point)
{
	glm::vec3 n = glm::normalize(point);
	float u = atan2(n.x, n.z) / (2.0f * M_PI) + 0.5f;
	float v = n.y * 0.5f + 0.5f;

	return glm::vec2( u , v);
}

void PlanetFactory::calcTexCoords(Model * model)
{
	std::vector<GLuint>* indices = model->getMeshByIndex(0)->getIndiciesV();

	std::vector<float>* points = model->getMeshByIndex(0)->getPointsV();

	std::vector<float> texts;


	for(GLuint e = 0; e< indices->size(); e++)
	{
		GLuint i = indices->at(e);
		GLuint p = i * 3;
		GLuint q = i * 2;
		
		glm::vec3 point = glm::vec3(points->at(p), points->at(p+1), points->at(p+2));
		
		glm::vec2 tex = calculateTextureCoordinate(point);

		texts.push_back(tex.x);
		texts.push_back(tex.y);
	}

	model->getMeshByIndex(0)->setTexts(texts);

}

float PlanetFactory::sortTextCoord(float coord)
{
	if (coord > 1.0f)
	{
		coord = coord - 1.0f;	
		return sortTextCoord(coord);
	}
	else if (coord < 0.0f)
	{
		coord = coord + 1.0f;
		return sortTextCoord(coord);
	}
	else
	{
		return coord;
	}

}

void PlanetFactory::initialize_sphere( const unsigned int depth, const unsigned int meshDepth) {
	const double X = 0.525731112119133606;
	const double Z = 0.850650808352039932;

	singleMeshDepth = meshDepth;

	const glm::vec3 vdata[12] = {
		{-X, 0.0, Z}, { X, 0.0, Z }, { -X, 0.0, -Z }, { X, 0.0, -Z },
		{ 0.0, Z, X }, { 0.0, Z, -X }, { 0.0, -Z, X }, { 0.0, -Z, -X },
		{ Z, X, 0.0 }, { -Z, X, 0.0 }, { Z, -X, 0.0 }, { -Z, -X, 0.0 }
	};

	int tindices[20][3] = {
		{0, 4, 1}, { 0, 9, 4 }, { 9, 5, 4 }, { 4, 5, 8 }, { 4, 8, 1 },
		{ 8, 10, 1 }, { 8, 3, 10 }, { 5, 3, 8 }, { 5, 2, 3 }, { 2, 7, 3 },
		{ 7, 10, 3 }, { 7, 6, 10 }, { 7, 11, 6 }, { 11, 0, 6 }, { 0, 1, 6 },
		{ 6, 1, 10 }, { 9, 0, 11 }, { 9, 11, 2 }, { 9, 2, 5 }, { 7, 2, 11 }
	};

	std::vector<std::future<std::vector<MeshData *>>> threads;

	threads.resize(20);

	for (int i = 0; i < 20; i++)
	{
		threads[i] = std::async ( &PlanetFactory::sphereSection,this, vdata[tindices[i][0]], vdata[tindices[i][1]], vdata[tindices[i][2]], depth);
	}

	for (int i = 0; i < 20; i++)
	{
		std::vector<MeshData*> meshesFromThread = threads[i].get();

		int j = 0;
		if (i == 0)
		{
			meshData = meshesFromThread[0];
			j = 1;
		}
		for (j; j < meshesFromThread.size(); j++)
		{
			meshData->add(meshesFromThread[j]);

			delete meshesFromThread[j];
		}

		meshesFromThread.clear();
	}

}

std::vector<MeshData*> PlanetFactory::sphereSection(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, const unsigned int depth)
{
	std::vector < glm::vec3> spherePoints;
	std::vector < glm::vec3> sphereNormals;
	std::vector < glm::vec2> sphereTexts;
	std::vector < glm::vec4> sphereTangents;
	std::vector < glm::vec4> sphereBiTangents;
	std::vector <MeshData*> generatedMeshes;


	subdivide(v1, v2, v3, depth, &spherePoints, &sphereNormals, &sphereTexts, &sphereTangents, &sphereBiTangents, &generatedMeshes);

	return generatedMeshes;
}

void PlanetFactory::subdivide(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, int depth,std::vector < glm::vec3> *spherePoints,std::vector < glm::vec3> *sphereNormals,std::vector < glm::vec2> *sphereTexts,std::vector < glm::vec4> *sphereTangents,std::vector < glm::vec4> *sphereBiTangents, std::vector<MeshData *> * meshes)
{
	if (depth == 0) {
		sortTriangle(v3, v2, v1,spherePoints,sphereNormals,sphereTexts,sphereTangents,sphereBiTangents);
		return;
	}

	glm::vec3 v12 = glm::normalize(v1 + v2);
	glm::vec3 v23 = glm::normalize(v2 + v3);
	glm::vec3 v31 = glm::normalize(v3 + v1);

	subdivide(v1, v12, v31, depth - 1, spherePoints, sphereNormals, sphereTexts, sphereTangents, sphereBiTangents, meshes);
	subdivide(v2, v23, v12, depth - 1, spherePoints, sphereNormals, sphereTexts, sphereTangents, sphereBiTangents, meshes);
	subdivide(v3, v31, v23, depth - 1, spherePoints, sphereNormals, sphereTexts, sphereTangents, sphereBiTangents, meshes);
	subdivide(v12, v23, v31, depth - 1, spherePoints, sphereNormals, sphereTexts, sphereTangents, sphereBiTangents, meshes);

	if (depth == singleMeshDepth)
	{
		makeMeshData(spherePoints, sphereNormals, sphereTexts, sphereTangents, sphereBiTangents, meshes);
	}
}

void PlanetFactory::sortTriangle(glm::vec3 v0, glm::vec3 v1, glm::vec3 v2,std::vector < glm::vec3> *spherePoints,std::vector < glm::vec3> *sphereNormals,std::vector < glm::vec2> *sphereTexts,std::vector < glm::vec4> *sphereTangents,std::vector < glm::vec4> *sphereBiTangents)
{
	glm::vec2 t0 = calculateTextureCoordinate(v0);
	glm::vec2 t1 = calculateTextureCoordinate(v1);
	glm::vec2 t2 = calculateTextureCoordinate(v2);

	//find the left and right most point

	bool isOnSeam = false;

#pragma region Establishing if a triangle is on a seam and fixing...

	if (t0.x > maxTexFixThreshold)
	{
		if (t2.x < minTexFixThreshold)
		{
			//Seam?
			isOnSeam = true;
			t2.x += 1.0f;
		}
		if (t1.x < minTexFixThreshold)
		{
			isOnSeam = true;
			t1.x += 1.0f;
		}
	}
	if (t1.x > maxTexFixThreshold)
	{
		if (t2.x < minTexFixThreshold)
		{
			//Seam?
			isOnSeam = true;
			t2.x += 1.0f;
		}
		if (t0.x < minTexFixThreshold)
		{
			isOnSeam = true;
			t0.x += 1.0f;
		}
	}
	if (t2.x > maxTexFixThreshold)
	{
		if (t0.x < minTexFixThreshold)
		{
			//Seam?
			isOnSeam = true;
			t0.x += 1.0f;
		}
		if (t1.x < minTexFixThreshold)
		{
			isOnSeam = true;
			t1.x += 1.0f;
		}
	}
#pragma endregion

	v0 = addHeightMapData(v0, t0, glm::normalize(v0));
	v1 = addHeightMapData(v1, t1, glm::normalize(v1));
	v2 = addHeightMapData(v2, t2, glm::normalize(v2));



	spherePoints->push_back(v0);
	spherePoints->push_back(v1);
	spherePoints->push_back(v2);

	sphereTexts->push_back(t0);
	sphereTexts->push_back(t1);
	sphereTexts->push_back(t2);

	//Normals are pure normals - recalculated in the tessalation shader
	sphereNormals->push_back(glm::normalize(v0));
	sphereNormals->push_back(glm::normalize(v1));
	sphereNormals->push_back(glm::normalize(v2));

	//Calulate the tangents
	deltaPos1 = v1 - v0;
	deltaPos2 = v2 - v0;

	deltaUV1 = t1 - t0;
	deltaUV2 = t2 - t0;

	float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
	tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * r;
	bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * r;

	tangent4 = glm::vec4(tangent.x, tangent.y, tangent.z, 0.0f);
	bitangent4 =glm::vec4(bitangent.x, bitangent.y, bitangent.z, 0.0f);

	tangent4 = glm::normalize(tangent4);
	bitangent4 = glm::normalize(bitangent4);

	sphereTangents->push_back(tangent4);
	sphereTangents->push_back(tangent4);
	sphereTangents->push_back(tangent4);

	sphereBiTangents->push_back(bitangent4);
	sphereBiTangents->push_back(bitangent4);
	sphereBiTangents->push_back(bitangent4);
}

void PlanetFactory::makeMeshData(std::vector < glm::vec3>* spherePoints,std::vector < glm::vec3>* sphereNormals,std::vector < glm::vec2>* sphereTexts,std::vector < glm::vec4>* sphereTangents,std::vector < glm::vec4>* sphereBiTangents, std::vector<MeshData*>* meshes)
{
	//Write some indicies in, just 0 - size()
	std::vector<GLuint> indicies;

	for (GLuint count = 0; count < spherePoints->size(); count++)
	{
		indicies.push_back(count);
	}

	for (int count = 0; count < spherePoints->size(); count++)
	{
		(*spherePoints)[count] = radius * (*spherePoints)[count] ;
	}


	MeshData* md = new MeshData(*spherePoints, *sphereNormals, *sphereTexts, *sphereTangents, *sphereBiTangents, indicies, spherePoints->size());

	spherePoints->clear();
	sphereNormals->clear();
	sphereTexts->clear();
	sphereTangents->clear();
	sphereBiTangents->clear();


	meshes->push_back(md);
}


glm::vec3 PlanetFactory::addHeightMapData(glm::vec3 point, glm::vec2 tex, glm::vec3 normal)
{
	//Sample height added from the height map using the texCoord
	float heightAdded = 0.0f;

	if (planetSettings.active)
	{
		//Correct co-ordinates for cicrumfrence difference....
		glm::vec2 texCoord = glm::vec2(0);
		texCoord.y = tex.y;

		//Angle of latitude in radians
		float angle = texCoord.x * M_PI;

		float circumfrence = (radius * 2 * M_PI);

		float lengthOfLongitudeLine =360*  cos(angle) * circumfrence;

		//I think this is the right way round, will become more obvious as the terrain is more defined
		float portion = lengthOfLongitudeLine / circumfrence;

		texCoord.x = portion * tex.x;

		double perlin =
			1.0f * (pnGen->noise(texCoord.x * planetSettings.perlinSize, texCoord.y * planetSettings.perlinSize))
			+ 0.5 * (pnGen->noise(texCoord.x * planetSettings.perlinSize, texCoord.y * planetSettings.perlinSize))
			+ 0.25f * (pnGen->noise(texCoord.x * planetSettings.perlinSize, texCoord.y * planetSettings.perlinSize));

		perlin = pow(perlin, planetSettings.exponent);

		heightAdded = planetSettings.scale * perlin;
	}


	//normal is unit "up" from the point
	point += heightAdded * normal;

	return point;
}
