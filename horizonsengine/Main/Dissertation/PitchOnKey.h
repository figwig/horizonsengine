#pragma once
#include <PrivateBehaviour.h>
#include <Game.h>
#include <glm-0.9.9.2/glm/gtc/matrix_transform.hpp>
class PitchOnKey :public PrivateBehaviour
{
public:
	PitchOnKey(Keystate key,float rot,  glm::vec3 axis)
	{
		k = key;
		r = rot;
		a = axis;
	}


	void update(double x)
	{
		if (Game::singleton()->getKeys() & k)
		{
			glm::mat4 newRotMat = glm::rotate(target->getRotMat(),r, a);
			target->setRot(newRotMat);
		}
	}

private:
	Keystate k;
	glm::vec3 a;
	float r;
};
