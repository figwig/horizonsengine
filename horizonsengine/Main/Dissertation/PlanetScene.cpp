#include "PlanetScene.h"
#include "PlanetFactory.h"
#include <SimpleCamera.h>
#include <Game.h>
#include <MoonLight.h>
#include <Scenery.h>
#include <Skybox.h>
#include <smallSpotLight.h>
#include "PlanetaryController.h"
#include <FirstPersonCamera.h>
#include <Rotator.h>
#include "Sun.h"
#include <RebuildShader.h>
#include <ShaderManager.h>
#include "TessalatingPlanetaryShaders.h"
#include <ModelManager.h>
#include "PlanetCamera.h"
#include "ComputeNoiseGenerator.h"
#include <RenderConditions.h>

PlanetScene::PlanetScene()
{
	name = "PlanetScene";
}

PlanetScene::~PlanetScene()
{
}

void PlanetScene::run()
{


	gameRefr->setPlaying(true);
}

void PlanetScene::load(Game* g)
{
	gameRefr = g;

	loadInit();

	LoadPlanet();

	Sun* sun = new Sun(glm::vec3(0.8f, 0.2f, 0.2f));

	currLighting->addLight(sun);


	smallSpotLight* ssl = new smallSpotLight(glm::vec3(2.0f, 2.0f, 2.3f), glm::vec3(2408.0f, 1080.0f, -2495.0f));

	//currLighting->addLight(ssl);

	ssl->attenuation = 0.0f;
	ssl->coneAngle = 100;

	Skybox *sky = new Skybox(0);

	addGameObject(sky);

	HiResSphere();

	Player();

	gameRefr->addGlobalBehaviour(new RebuildShader(ShaderManager::singleton()->getShader<TessalatingPlanetaryShaders*>()));


	Panel* img = new Panel(TextureExaminer::singleton()->getTexture("Textures/brickwall.jpg"));

	img->getComponent<RenderConditions>()->setShader(new TerrainNoiseShaders());

	//addGUIObject(img);

	gameRefr->setConfigAll();
}

void PlanetScene::LoadPlanet()
{


	//planet = PlanetFactory::getInstance()->makeTerrainedPlanet(1800, "Pebbles.jpg","Pebbles_s.jpg", "Pebbles_n.jpg");

	planet = PlanetFactory::getInstance()->makePreGenPlanet(180.0f, "Pebbles.jpg", "Pebbles_s.jpg", "Pebbles_n.jpg");

	//planet = PlanetFactory::getInstance()->makeTestPlanet(1800);

	addGameObject(planet);

	planet->setPos(glm::vec3(0, 0, 0));

	planet->addPrivateBehaviour(new yRotator(planet, 0.0001f));
}

void PlanetScene::Player()
{
	Model* cube = ModelManager::getModel("Objects/cube.obj");

	Custom* cube1 = new Custom(cube, glm::vec3(0.0f, 0.0f, -5000.0f), glm::vec3(0.1f), glm::vec3(0.0f, 0.0f, 0.0f));

	addGameObject(cube1);

	player = cube1;

	//Make a player controller
	PlanetaryController* controller = new PlanetaryController(planet, gameRefr, 2000, 0.8f, 1800, 0.01f);

	player->addPrivateBehaviour(controller);

	Camera* newCamera = new PlanetCamera(planet);
//	Camera* newCamera = new FirstPersonCamera(player);
	gameRefr->setCamera(newCamera);
}

void PlanetScene::HiResSphere()
{
	Model* sphere = ModelManager::getModel("Objects/HiResSphere.obj");

	Custom* sphereGO = new Custom(sphere, glm::vec3(25, 2, 0), glm::vec3(0.05f), glm::vec3(0, -1.57f, 0));

	sphereGO->name = "That Sphere";

	addGameObject(sphereGO);
}