#pragma once
#include "Planet.h"
#include "PerlinGenerator.h"
#include <string>


struct PlanetSettings
{
public:
	PlanetSettings() {}
	~PlanetSettings() {}

	bool active = false;
	int noiseType = 0;
	float scale = 0.1f;

	float perlinSize = 250.0f;

	double exponent = 2.0f;
};


class PlanetFactory
{
public:
	static PlanetFactory* getInstance();

	Planet* makeTestPlanet(float radius);

	Planet* makeTerrainedPlanet(float radius, std::string diff, std::string spec, std::string norm);

	Planet* makePreGenPlanet(float radius, std::string diff, std::string spec, std::string norm);

private:

	PerlinNoise* pnGen = nullptr;

	static PlanetFactory* thisPointer;

	void calcTexCoords(Model * model);

	PlanetFactory();
	~PlanetFactory();

	Model* sortModel(float radius);

	void initialize_sphere(const unsigned int depth, const unsigned int meshDepth);

	std::vector<MeshData*> sphereSection(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, const unsigned int depth);

	void subdivide(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, int depth,std::vector < glm::vec3>* spherePoints,std::vector < glm::vec3>* sphereNormals,std::vector < glm::vec2>* sphereTexts,std::vector < glm::vec4>* sphereTangents,std::vector < glm::vec4>* sphereBiTangents, std::vector<MeshData *> *meshes);

	void sortTriangle( glm::vec3, glm::vec3, glm::vec3,std::vector < glm::vec3> *spherePoints,std::vector < glm::vec3> *sphereNormals,std::vector < glm::vec2> *sphereTexts,std::vector < glm::vec4> *sphereTangents,std::vector < glm::vec4> *sphereBiTangents);

	void makeMeshData(std::vector < glm::vec3>* spherePoints, std::vector < glm::vec3>* sphereNormals, std::vector < glm::vec2>* sphereTexts, std::vector < glm::vec4>* sphereTangents, std::vector < glm::vec4>* sphereBiTangents, std::vector<MeshData*> * meshes);

	glm::vec2 calculateTextureCoordinate(glm::vec3 point);

	float sortTextCoord(float x);

	glm::vec3 addHeightMapData(glm::vec3 point, glm::vec2 texCoords, glm::vec3 normal);

	std::vector < MeshData*> meshes;

	float minTexFixThreshold = 0.2f;
	float maxTexFixThreshold = 0.8f;

	int singleMeshDepth = 0;

	float radius = 0.0f;


	glm::vec2 deltaUV1;
	glm::vec2 deltaUV2;

	glm::vec3 deltaPos1;
	glm::vec3 deltaPos2;

	glm::vec3 tangent;
	glm::vec3 bitangent;

	glm::vec4 tangent4;
	glm::vec4 bitangent4;

	PlanetSettings planetSettings;

	MeshData* meshData = nullptr;
};

