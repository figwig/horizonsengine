#include <GameObject.h>
#include <EngineWrapper.h>
#include "PlanetScene.h"
#include "PlanetShaders.h"
#include <ShaderManager.h>
#include "TessalatingPlanetaryShaders.h"
int main(int argc, char* argv[]) {

	EngineWrapper* horizonsInstance = new EngineWrapper();


	horizonsInstance->initGL(argc, argv, "Horizons Engine Demo Scenes");

	horizonsInstance->sortConfig();

	Scene* levelOne = new PlanetScene();

	ShaderManager::singleton()->addShader(PlanetShaders::getInstance());
	ShaderManager::singleton()->addShader(TessalatingPlanetaryShaders::getInstance());

	horizonsInstance->initializeGame(levelOne);

	horizonsInstance->startGame();

	return 0;
}
