#pragma once
#include <PostProcessRect.h>
#include <vector>
#include <glm-0.9.9.2/glm/glm.hpp>

class WorldGenCanvas :
	public PostProcessRect
{
public:
	WorldGenCanvas(std::vector<glm::vec3> points);
	~WorldGenCanvas();

	void sortCanvasVAO();
private:
	GLuint spherePointsVBO = 0;
	std::vector<glm::vec3> spherePoints;
};

