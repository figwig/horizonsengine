#version 330

uniform sampler2D textureImage;
uniform sampler2D specularImage;
uniform sampler2D normalMap;

uniform sampler2D waterTex;
uniform sampler2D waterTexSpec;
uniform sampler2D waterTexNorm;

uniform int useNormalMap;
uniform int invertY;
uniform float tilingFactor;

//Uniforms that have to be unique to each surface

#define MAX_LIGHTS 20
float M_PI = 3.14159265358979323846;
float delta = 0.001f;

uniform float materialShininess;

uniform int numLights;

uniform struct Light {
	vec4 position;
	vec3 colors;
	float attenuation;
	float ambientCoefficient;
	float coneAngle;
	vec3 coneDirection;
	mat4 finalLightMatrix;
	sampler2D shadowMap;
	bool castShadows;
	mat4 shadowMatrix;
} allLights[MAX_LIGHTS];


float when_eq(float x, float y) {
	return 1.0 - abs(sign(x - y));
}

in packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;

	vec3 cameraPos;
	vec3 debugTexture;

	float biomeMix;

	vec3 tangent;
	vec3 bitangent;

} inputFragment;

vec4 snow = vec4(0.9, 0.9, 0.95, 1.0);
vec4 water = vec4(0.001, 0.2, 0.6, 1.0);
vec4 grass = vec4(0.1, 0.8, 0.3, 1.0);

float distanceToCam;

//Shadow Consts
const int pcfCount = 9;
const float totalTexels = (pcfCount * 2 + 1) * (pcfCount * 2 + 1);
uniform float mapSize;
uniform float shadowDistance;
uniform sampler2D fragHeightMap;
uniform float radius;
float circumfrence;

float fadeDist;
float texelSize;
vec3 mapSpec;

mat3 calcedTBN;
vec3 calcedNormal;

float shadowTestingMultiplier = 1.0f;

// output packet
layout(location = 0) out vec4 fragmentColour;

vec3 applyLight(Light thisLight, vec3 baseColor, vec3 surfacePos, vec3 surfaceToCamera)
{
	float attenuation = 1.0f;
	vec3 lightPos = (thisLight.finalLightMatrix * thisLight.position).xyz;
	vec3 surfaceToLight;

	vec3 coneDir = normalize(thisLight.coneDirection);

	if (thisLight.position.w == 0.0f)
	{
		//Directional Light (all rays same angle, use position as direction)
		surfaceToLight = normalize((thisLight.position).xyz);
		attenuation = 1.0f;
	}
	else
	{
		//Point light
		surfaceToLight = normalize(lightPos - (surfacePos));


		float distanceToLight = length(lightPos - (surfacePos));
		attenuation = 1.0 / (1.0f + thisLight.attenuation * pow(distanceToLight, 2));

		//Work out the Cone restrictions
		float lightToSurfaceAngle = degrees(acos(dot(-surfaceToLight, normalize(coneDir))));

		if (lightToSurfaceAngle > thisLight.coneAngle)
		{
			attenuation = 0.0;
		}

	}
	if (thisLight.castShadows)
	{

		shadowTestingMultiplier = 0.0f;

		vec4 shadowCoords = thisLight.shadowMatrix * vec4(inputFragment.vert, 1.0f); //

		if (shadowCoords.x > 0.0f && shadowCoords.x < 1.0f && shadowCoords.y >0.0f && shadowCoords.y < 1.0f)
		{

			float bias = max(0.01 * (1.0 - dot(calcedNormal, surfaceToLight)), 0.005);
			//bias = 0.005f;

			float total = 0.0f;

			float distance = distanceToCam - ((shadowDistance * 1.3f) - fadeDist);
			distance /= fadeDist;

			shadowCoords.w = clamp(1.0 - distance, 0.0, 1.0);

			for (int x = -pcfCount; x <= pcfCount; x++)
			{
				for (int y = -pcfCount; y <= pcfCount; y++)
				{
					float objectNearestLight = texture2D(thisLight.shadowMap, shadowCoords.xy + vec2(x, y) * texelSize).r;
					if (shadowCoords.z - bias > objectNearestLight)
					{
						total++;
					}
				}
			}

			total /= totalTexels;

			attenuation -= (total * shadowCoords.w);
			attenuation = clamp(attenuation, 0.05, 1.0);
		}
	}
	//Ambient light calculation
	vec3 ambient = thisLight.ambientCoefficient * baseColor.rgb * thisLight.colors;

	//Diffuse Light Calculation
	float diffuseCoefficient = max(0.0, dot(calcedNormal, normalize(surfaceToLight)));
	vec3 diffuse = diffuseCoefficient * baseColor.rgb * thisLight.colors;

	//Specular Light Calculation
	float specularCoefficient = 0.0;
	if (diffuseCoefficient > 0.0)
	{
		specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, calcedNormal))), 20);
	}
	
	float avgMapSpec = (mapSpec.x + mapSpec.y + mapSpec.z) / 3.0f;

	vec3 specular = specularCoefficient * avgMapSpec * thisLight.colors;

	return ambient + attenuation * (diffuse + specular);
}

float getNoiseAtPoint(vec2 tex)
{
	return texture2D(fragHeightMap, tex).r * 10;
}

float getBiomeMix(vec2 tex)
{
	return texture2D(fragHeightMap, tex).r * 10;
}

float getUnitLengthXAtY(float y)
{
	//Angle from top 
	float angle = y * M_PI;

	//Covert to latitude
	angle -= M_PI / 2.0f;

	float lengthOfLongitudeLine = cos(angle) * circumfrence;

	float unitX = 1.0f / lengthOfLongitudeLine;

	return unitX;
}

vec3 calculateNormal()
{
	vec3 tangent = inputFragment.tangent;

	vec3 bitangent = inputFragment.bitangent;

	vec2 pointCoord = inputFragment.textureCoord;


	//Delta is always delta in 

	vec2 pointCoordL = pointCoord + vec2(-getUnitLengthXAtY(pointCoord.y), 0);
	vec2 pointCoordR = pointCoord + vec2(getUnitLengthXAtY(pointCoord.y), 0);
	vec2 pointCoordU = pointCoord + vec2(0, delta);
	vec2 pointCoordD = pointCoord + vec2(0, -delta);

	//Use the original TBN matrix to calculate this properly
	float heightL = (radius + getNoiseAtPoint(pointCoordL));
	float heightR = (radius + getNoiseAtPoint(pointCoordR));
	float heightU = (radius + getNoiseAtPoint(pointCoordU));
	float heightD = (radius + getNoiseAtPoint(pointCoordD));

	vec3 tangentNormal = normalize(0.25f * vec3(2.0f * (heightL - heightR), 2.0f * (heightU - heightD), -4.0f));


	mat3 TBN = mat3(tangent, bitangent, normalize(-inputFragment.normal));


	vec3 worldNormal = normalize(TBN * tangentNormal);

	return worldNormal;
}


void main(void) {
	texelSize = 1.0f / mapSize;

	fadeDist = shadowDistance * 0.12f;
	distanceToCam = length(inputFragment.vert - inputFragment.cameraPos);

	circumfrence = radius * 2.0f * M_PI;

	delta = 1.0f / (circumfrence / 2.0f);


	//get the base colour from the texture
	vec4 tempFragColor = vec4(0);

	float biomeMix = getBiomeMix(inputFragment.textureCoord);

	//biomeMix -= 0.25f;

	vec4 dirtColour = tempFragColor = texture2D(textureImage, 2 * tilingFactor * inputFragment.textureCoord).rgba;

	vec3 dirtSpec = texture2D(specularImage, tilingFactor * inputFragment.textureCoord).xyz;


	vec4 waterColor = texture2D(waterTex, 2 * tilingFactor * inputFragment.textureCoord).rgba;

	vec3 waterSpec = texture2D(waterTexSpec, 2 * tilingFactor * inputFragment.textureCoord).rgb;
	
	tempFragColor += dirtColour * when_eq(biomeMix, min(biomeMix, 0.8f));
	mapSpec += dirtSpec * when_eq(biomeMix, min(biomeMix, 0.8f));

	tempFragColor += waterColor * when_eq(biomeMix, min(biomeMix, 0.2f));
	mapSpec += waterSpec * when_eq(biomeMix, min(biomeMix, 0.2f));


	calcedNormal = calculateNormal();

	//calcedNormal = vec3(0, 1, 0);

	vec3 surfaceToCameraWorld = normalize((inputFragment.cameraPos) - (inputFragment.vert));

	vec3 tempColour = vec3(0.0, 0.0, 0.0);

	for (int count = 0; count < numLights; count++)
	{
		tempColour += applyLight(allLights[count], tempFragColor.xyz, inputFragment.vert, surfaceToCameraWorld);
	}

	vec3 gamma = vec3(1.0 / 2.2);

	vec4 final = vec4(pow(tempColour, gamma), tempFragColor.a);


	fragmentColour = final;
	//fragmentColour = vec4(inputFragment.debugTexture,1);
	//fragmentColour = vec4(calcedNormal, 1.0f);
	//fragmentColour = vec4(1, 0, 0, 1);
}


