#version 330

uniform mat4 T; // Translation matrix
uniform mat4 S; // Scale matrix
uniform mat4 R; // Rotation matrix
uniform vec4 posRelParent; // the position relative to the parent
uniform vec4 clipPlane; //The clipping Plane
uniform int usePlane; //PLane bool
uniform vec3 origin;// The centre of the sphere

// Input vertex packet
layout(location = 0) in vec4 position;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec4 tangent;
layout(location = 4) in vec4 bitangent;
layout(location = 8) in vec2 textureCoord;
layout(location = 9) in mat4 instanceMatrix;
layout(location = 13) in int materialID;

// Output vertex packet
out packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;

	vec3 tangent;
	vec3 bitangent;
	vec3 origin;
	vec3 modelPoint;

} outputVertex;

mat4 transform;
mat3 TBN;
vec4 pos;
mat3 normalMat;

void main(void) {
	//Calculate transform matrix
	transform = T * R * S;

	//Calculate pos
	pos = vec4(position.x, position.y, position.z, 1.0) + posRelParent;

	outputVertex.modelPoint = vec3(pos);

	//Pass through the texture coords
	outputVertex.textureCoord = textureCoord;

	//Transform matrix dealt with
	pos = transform * pos;

	//Output vertex pos
	outputVertex.vert = pos.xyz;
	outputVertex.origin = mat3(transform) * origin;

	//Work out the normal for lighting
	normalMat = transpose(inverse(mat3(transform)));

	outputVertex.normal = normalize(normalMat * normal);

	vec3 T = normalize(vec3(transform * normalize(tangent)));

	vec3 B = normalize(vec3(transform * bitangent));

	vec3 N = vec3(outputVertex.normal);

	//T = normalize(T - dot(T, N) * N);

	//B = cross(N, T);

	outputVertex.tangent =T;
	outputVertex.bitangent = B;

	gl_Position = pos;
}