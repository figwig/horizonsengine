#version 330

uniform mat4 T; // Translation matrix
uniform mat4 S; // Scale matrix
uniform mat4 R; // Rotation matrix
uniform mat4 camera; // camera matrix

uniform vec3 cameraPos;

layout (location = 0) in vec3 position;

out packet{
	vec4 clipSpace;
	vec2 textureCoords;
	vec3 toCamera;
	vec3 vert;
} outputVertex;

uniform vec2 tiling;

float tilesPerUnit = 1000.0f;

void main(void)
{
	vec4 pos = vec4(position, 1.0);
	mat4 transform = T * R* S;

	vec2 tex = vec2((position.x / 2.0) + 0.5, (position.z / 2.0) + 0.5) * tilesPerUnit;

	
	outputVertex.textureCoords = tex/ tilesPerUnit;

	vec4 worldPos = transform * pos;

	outputVertex.toCamera = cameraPos - worldPos.xyz;
	outputVertex.vert = worldPos.xyz;

	outputVertex.clipSpace = camera * worldPos;

	gl_Position = outputVertex.clipSpace;
}