#pragma once
#include <DefaultShaders.h>

class PlanetShaders : public DefaultShaders
{
public :
	static PlanetShaders* getInstance();

	void prepareToRender(GameObject* go);
	
protected:
	PlanetShaders();
	~PlanetShaders();

	static PlanetShaders* thisPointer;

	void setTextures();

	void setup();

	std::string vertName = "Shaders/vertex_shader_planet.shader";
	std::string fragName = "Shaders/fragment_shader_planet.shader";

	GLuint tilingLoc = 0;
	GLuint texLoc0 = 0;
	GLuint texLoc1 = 0;
	GLuint texLoc2 = 0;
	GLuint texLoc3 = 0;
	GLuint texLoc4 = 0;
	GLuint texLoc15 = 0;
	GLuint texLoc15p2 = 0;
};

