#pragma once
#include <Light.h>

class Sun : public Light
{
public:
	Sun(glm::vec3 pos) 
	{
		intensities = glm::vec3(1.0, 1.0, 1.0);
		ambientCoefficient = 0.0005f;

		position = glm::vec4(pos.x, pos.y, pos.z, 0.0f);
	}
	~Sun() {}


};