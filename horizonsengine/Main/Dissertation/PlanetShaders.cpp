#include "PlanetShaders.h"
#include "Planet.h"

PlanetShaders* PlanetShaders::thisPointer = nullptr;

PlanetShaders* PlanetShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new PlanetShaders();
	}
	return thisPointer;
}

PlanetShaders::PlanetShaders()
{
	glDeleteProgram(programID);
	programID = setupShaders(vertName, fragName);
	glUseProgram(programID);
	setTextures();
	setup();

}

PlanetShaders::~PlanetShaders()
{

}

void PlanetShaders::setup() 
{
	DefaultShaders::setup();

	tilingLoc = glGetUniformLocation(programID, "tilingFactor");

	texLoc0 = glGetUniformLocation(programID, "textureImage");
	texLoc1 = glGetUniformLocation(programID, "specularImage");
	texLoc2 = glGetUniformLocation(programID, "normalMap");
	texLoc3 = glGetUniformLocation(programID, "waterTex");
	texLoc4 = glGetUniformLocation(programID, "waterTexSpec");
	texLoc15 = glGetUniformLocation(programID, "heightMap");
	texLoc15p2 = glGetUniformLocation(programID, "fragHeightMap");
}

void PlanetShaders::setTextures()
{
	glUniform1i(texLoc0, 0);
	glUniform1i(texLoc1, 1);
	glUniform1i(texLoc2, 2);
	glUniform1i(texLoc3, 3);
	glUniform1i(texLoc4, 4);

	glUniform1i(texLoc15, 15);
	glUniform1i(texLoc15p2, 15);
}


void PlanetShaders::prepareToRender(GameObject* go)
{
	PlanetRenderingComponent* prc = go->getComponent<PlanetRenderingComponent>();

	if (prc == nullptr) return;

	DefaultShaders::prepareToRender(go);

	glUniform1f(tilingLoc, prc->getTilingFactor());
}