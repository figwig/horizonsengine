#include "PlanetCamera.h"
#define NOMINMAX
#include <glm-0.9.9.2/glm/glm.hpp>
#include <glm-0.9.9.2/glm/gtc/matrix_transform.hpp>
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include "Planet.h"

PlanetCamera::PlanetCamera(Planet * planet) :planet(planet)
{
	radius = planet->getComponent<PlanetRenderingComponent>()->radius;


	position = planet->getPos() - glm::vec3(0, 0, radius * 30);



	forceUpdate = true;
	update(0, 0, 0, 0);
	forceUpdate = false;


}

PlanetCamera::~PlanetCamera()
{

}
void PlanetCamera::OnClick(int key, float x, float y)
{
	if (key == HE_INPUT_RIGHT_MOUSE_DOWN)
	{
		mouseDown = true;
	}
	if (key == HE_INPUT_RIGHT_MOUSE_UP)
	{
		mouseDown = false;
	}
}
void PlanetCamera::update(double deltaTimed, int mouseX, int mouseY, bool)
{
	calcPersp();


	if (mouseDown || forceUpdate)
	{
		//Rotate based on camera up and camera right

		cameraTarget = planet->getPos();


		float mouseXF = mouseY / 200.0f;
		float mouseYF = -mouseX / 200.0f;

		glm::vec3 planetPos = planet->getPos();

		float distFromPlanetCentre = glm::length(cameraPos - planetPos);

		float distFromPlanetSurface = distFromPlanetCentre - radius;

		float modifier = distFromPlanetSurface / 15000.0f;


		if (modifier < 1.0f)
		{
			mouseXF *= modifier;
			mouseYF *= modifier;
		}


		position = glm::rotate(position, mouseXF, cameraRight);
		
		position = glm::rotate(position, mouseYF, cameraUp);

		cameraPos = position;

		//Get the vector the camera is looking down
		cameraDirection = glm::normalize(cameraTarget - cameraPos);

		lookDir = cameraDirection;

		//work out what camera right is
		cameraRight = glm::normalize(glm::cross(cameraUp, cameraDirection));

		//work out what  camera up is
		cameraUp = glm::cross(cameraDirection, cameraRight);


		cameraNoPersp = glm::lookAt(cameraPos, cameraTarget, cameraUp);

		camera = persp * cameraNoPersp;

		reportPosition();
	}
	else
	{
		//No change this frame
	}


}

void PlanetCamera::scrollWheel(double x, double y)
{
	float zoom = y * zoomSens;

	glm::vec3 planetPos = planet->getPos();

	float distFromPlanetCentre = glm::length(cameraPos - planetPos);

	float distFromPlanetSurface = distFromPlanetCentre - (radius * 10);

	float modifier = distFromPlanetSurface / 300.0f;

	glm::vec3 direction = lookDir *  zoom;
	position += modifier *  direction;

	forceUpdate = true;
	update(0, 0, 0, 0);
	forceUpdate = false;
}