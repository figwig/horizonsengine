#pragma once
#include "Planet.h"
#include <Scene.h>

class FlatTerrainScene : public Scene
{
public:
	FlatTerrainScene();

	~FlatTerrainScene();

	void run();

	void load(Game*);

private:

	void LoadPlanet();

	void Player();

	void HiResSphere();

	GameObject* player = nullptr;

	Planet* planet = nullptr;
};

