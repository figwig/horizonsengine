#pragma once
#include <Camera.h>
#include "Planet.h"
#include <InputListener.h>

class PlanetCamera : public Camera, public InputListener
{

public:

	PlanetCamera(Planet*);

	~PlanetCamera();

	void update(double delta, int mouseX, int mouseY, bool playing);

	void scrollWheel(double x, double y);

	void OnClick(int key, float x, float y);
private:
	Planet* planet = nullptr;
	
	float radius = 0.0f;

	float zoomSens = 10.1f;

	glm::vec3 position;

	glm::vec3 lookDir = glm::vec3(0);

	bool mouseDown = false;
	bool forceUpdate = false;

	float distFromPlanet = 0.0f;
	float optimalDistFromPlanet = 0.0f;

	int watchLocPortion = 0;
};

