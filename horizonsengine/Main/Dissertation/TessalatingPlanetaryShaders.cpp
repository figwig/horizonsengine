#include "TessalatingPlanetaryShaders.h"
#include "Planet.h"

TessalatingPlanetaryShaders* TessalatingPlanetaryShaders::thisPointer = nullptr;

TessalatingPlanetaryShaders::TessalatingPlanetaryShaders()
{
	build();
}

void TessalatingPlanetaryShaders::build()
{
	glDeleteProgram(programID);
	programID = setupShadersTessalation(vertexFilePath, tcsFilePath, tesFilePath, fragmentFilePath);

	glUseProgram(programID);

	setup();

	GLint MaxPatchVertices = 0;
	glGetIntegerv(GL_MAX_PATCH_VERTICES, &MaxPatchVertices);
	printf("Max supported patch vertices %d\n", MaxPatchVertices);
	glPatchParameteri(GL_PATCH_VERTICES, 3);
}

void TessalatingPlanetaryShaders::prepareToRender(GameObject* go)
{
	PlanetRenderingComponent* prc = go->getComponent<PlanetRenderingComponent>();

	if (prc == nullptr) return;

	PlanetShaders::prepareToRender(go);

	glUniform1f(locRadius, prc->radius);

	glUniform3fv(locOrigin, 1,(GLfloat *) &go->getPos());
}


void TessalatingPlanetaryShaders::setup()
{
	PlanetShaders::setup();

	locRadius = glGetUniformLocation(programID, "radius");

	locOrigin = glGetUniformLocation(programID, "origin");


}

void TessalatingPlanetaryShaders::rebuild()
{
	std::cout << "Rebuilding shaders....\n\n";

	build();
}