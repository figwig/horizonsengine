#pragma once
#include <Shader.h>
#include <string>

class TerrainNoiseShaders : public Shader
{
public:
	TerrainNoiseShaders()
	{
		vertFileName = "Shaders/vertex_terrain_noise.shader";
		fragFileName = "Shaders/fragment_terrain_noise.shader";
		setup();
	}
	~TerrainNoiseShaders()
	{

	}

	void rebuild() { glDeleteShader(programID);  setup(); }

	void setLayerCount(int i);

	void setRadius(float x);

	void setOceanLevel(float o);

	void setup();
private:

	std::string vertFileName;
	std::string fragFileName;
	GLuint locRadius = 0;
	GLuint locLayerCount = 0;
	GLuint locOceanLevel = 0;
};

