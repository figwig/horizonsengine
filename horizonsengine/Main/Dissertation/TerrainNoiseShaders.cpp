#include "TerrainNoiseShaders.h"


void TerrainNoiseShaders::setup()
{
	programID = setupShaders(vertFileName, fragFileName);
	locRadius = glGetUniformLocation(programID, "radius");
	locLayerCount = glGetUniformLocation(programID, "actualLayerCount");
	locOceanLevel = glGetUniformLocation(programID, "oceanLevel");
}

void TerrainNoiseShaders::setRadius(float x)
{

	glUniform1f(locRadius, x);
}

void TerrainNoiseShaders::setLayerCount(int x)
{
	glUniform1i(locLayerCount, x);
}

void TerrainNoiseShaders::setOceanLevel(float x)
{
	glUniform1f(locOceanLevel, x);
}