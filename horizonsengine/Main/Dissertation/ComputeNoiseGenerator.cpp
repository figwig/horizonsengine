#include "ComputeNoiseGenerator.h"
#include <RenderSettings.h>

ComputeNoiseGenerator::ComputeNoiseGenerator(PlanetGenProperties pgp) : pgp(pgp)
{
	tns = new TerrainNoiseShaders();
	fbo = new PlainFBO(4000, 4000, Game::singleton()->getDisplayDetails());

	ppr = new PostProcessRect(0);
	ppr->setShader(tns);
	ppr->setConfig(Game::singleton()->getConfig());

	setupShader();

	glGenBuffers(1, &UBOHandle);
	glBindBuffer(GL_UNIFORM_BUFFER, UBOHandle);
	glBufferData(UBOHandle, maxLayerCount * 2* sizeof(glm::vec4), (void*)0, GL_STATIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, UBOHandle);

	noiseLayers.resize(maxLayerCount);

	//Islands Layer
	noiseLayers[0].coordMod = 2;
	noiseLayers[0].exponent = 1.5f;
	noiseLayers[0].scale = 0.95f;
	noiseLayers[0].constructive = 1.0f;

	//Mountains
	noiseLayers[1].coordMod = 8;
	noiseLayers[1].exponent = 5.2f;
	noiseLayers[1].scale = 3.0f;
	noiseLayers[1].constructive = 1.0f;
	noiseLayers[1].scaleDependsOn = 0.0f;
	noiseLayers[1].active = 1.0f;

	//
	noiseLayers[2].coordMod = 20;
	noiseLayers[2].exponent = 2.9f;
	noiseLayers[2].scale = 0.1f;
	noiseLayers[2].constructive = 0.0f;
	noiseLayers[2].scaleDependsOn = 0.0f;
	noiseLayers[2].active = 1.0f;


	noiseLayers[3].coordMod = 50;
	noiseLayers[3].exponent = 1.0f;
	noiseLayers[3].scale = 0.1f;
	noiseLayers[3].constructive = 0.0f;
	noiseLayers[3].scaleDependsOn = 0.0f;
	noiseLayers[3].active = 1.0f;


	currentLayerCount = 3;
}

GLuint ComputeNoiseGenerator::getResult()
{
	return result;
}


void ComputeNoiseGenerator::Generate()
{
	tns->rebuild();

	fbo->bindFBOByIndex(0);
	RenderSettings * rs = new RenderSettings(glm::mat4(1), false, glm::vec4(0));

	glUseProgram(tns->getProgramID());

	tns->setRadius(x);

	updateUBO();

	tns->setLayerCount(currentLayerCount);

	tns->setOceanLevel(pgp.oceanLevel);

	ppr->draw(rs);


	fbo->unBind();

	result = fbo->getResult();
	delete rs;
}

void ComputeNoiseGenerator::setupShader()
{

}


void ComputeNoiseGenerator::updateUBO()
{
	std::vector<float> data;

	for (auto a : noiseLayers)
	{
		data.push_back(a.coordMod);
		data.push_back(a.exponent);
		data.push_back(a.scale);
		data.push_back(a.constructive);

		data.push_back(a.scaleDependsOn);
		data.push_back(a.pad1);
		data.push_back(a.pad1);
		data.push_back(a.active);
	}

	glBindBuffer(GL_UNIFORM_BUFFER, UBOHandle);
		
	glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::vec4) *2* maxLayerCount, data.data(), GL_STATIC_DRAW);

}