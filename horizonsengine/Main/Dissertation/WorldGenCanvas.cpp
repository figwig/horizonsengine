#include "WorldGenCanvas.h"
#include <Game.h>


WorldGenCanvas::WorldGenCanvas(std::vector<glm::vec3> spherePoints) : PostProcessRect(0)
{
	sortCanvasVAO();
}


WorldGenCanvas::~WorldGenCanvas()
{
}


void WorldGenCanvas::sortCanvasVAO()
{
	glDeleteBuffers(1, &VAO);
	glDeleteBuffers(1, &pointsVBO);
	glDeleteBuffers(1, &textsVBO);
	glDeleteBuffers(1, &indiciesVBO);

	//Setup the VAO
	glGenVertexArrays(1, &VAO);
	Game::singleton()->getRenderEngine()->bindVAO(VAO);

	//Add the points to the VBO
	glGenBuffers(1, &pointsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, pointsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints, points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &textsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, textsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints, texts, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);



	glGenBuffers(1, &indiciesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, totalPoints / 2 * sizeof(GLuint), indicies, GL_STATIC_DRAW);


	//glGenBuffers(1, &spherePointsVBO);
	//glBindBuffer(GL_ARRAY_BUFFER, spherePointsVBO);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * spherePoints.size(), spherePoints.data(), GL_STATIC_DRAW);

	Game::singleton()->getRenderEngine()->bindVAO(0);
}