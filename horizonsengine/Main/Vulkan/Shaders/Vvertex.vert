#version 450
#extension GL_ARB_seperate_shader_objects : enable

layout (location =0) in vec3 position;
layout (location =1) in vec3 inColor;
layout (location =2) in vec2 inTexCoord;

layout (location = 0) out vec3 color;
layout (location = 1) out vec2 fragTexCoord;


layout(binding = 0) uniform EssentialsBuffer {
	mat4 model;
	mat4 view;
	mat4 proj;
} essentialsBuffer;

void main()
{
	color = inColor;
	fragTexCoord = inTexCoord;
	gl_Position = essentialsBuffer.proj * essentialsBuffer.view * essentialsBuffer.model  * vec4(position, 1.0f);
}