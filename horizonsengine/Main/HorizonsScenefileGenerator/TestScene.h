#pragma once
#include <Scene.h>
class TestScene : public Scene
{
public:
	TestScene();
	~TestScene();

	void load(Game * refr);

	void run();

private:

	void loadGUI();

	void sceneInit();

	void loadCamera();

	void loadPlane();

	Camera * gameCamera;
};

