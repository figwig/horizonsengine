#include "EditorCamera.h"
#include <Game.h>

EditorCamera::EditorCamera(GameObject* newPlayer)
{
	posTrackRelRotPlayer = glm::vec3(0.0f, 0.0f, 0.0f);
	posRelPlayer = glm::vec3(0.0f, 0.0f, -30.0f);
	target = newPlayer;
	calcPersp();

	lookAround(0, 0);
}


EditorCamera::~EditorCamera()
{

}

void EditorCamera::update(double deltaTimed, int mouseX, int mouseY, bool playing)
{
	if (rightClickDown) lookAround(mouseX, mouseY);

	if (middleMouseDown) moveAround(mouseX, mouseY);

	calcMatrix();
}

void EditorCamera::lookAround(int mouseX, int mouseY)
{
	calcPersp();
	cameraAngle.x -= (1.0f * (mouseY)) / mouseSens;
	cameraAngle.y -= (-1.0f * ((glm::min)(mouseX, 900))) / mouseSens;

	if (cameraAngle.y > M_PI * 2) cameraAngle.y -= M_PI * 2;
	if (cameraAngle.y < -M_PI * 2) cameraAngle.y += M_PI * 2;

	///<Summary>
	///1.5. because this is a first person camera, rotate the target object by the y rotation of the camera
	///</Summary>




	//What is the camera looking at ?

}

void EditorCamera::moveAround(int mouseX, int mouseY)
{
	glm::vec3 cameraPos = target->getPos();

	glm::vec4 moveThisFrame = glm::vec4(0.0f);

	moveThisFrame.x = ((1.0f * (mouseX)) / mouseSens)  * moveSens;
	moveThisFrame.z = ((1.0f * ((glm::min)(mouseY, 900))) / mouseSens) * moveSens;


	moveThisFrame = glm::rotate(glm::mat4(1),cameraAngle.y, glm::vec3(0,1,0)) *moveThisFrame;

	cameraPos += glm::vec3(moveThisFrame);


	target->setPos(cameraPos);
}

void EditorCamera::calcMatrix()
{
	if (target != nullptr)
	{
		target->setRot(glm::rotate(target->getRotMat(), cameraAngle.y - prevCameraY, glm::vec3(0.0f, 1.0f, 0.0f)));

		glm::vec3 totalOffset = (target->getRotMat() * positionOffset);

		cameraPos = target->getPos() + totalOffset;

		prevCameraY = cameraAngle.y;
	}
	else
	{
		cameraPos = glm::vec3(0, 0, 0);
	}


	///<Summary>
	///2. The second step calculates the camera target
	///</Summary>

	float xAngle = cameraAngle.x + (M_PI / 2);
	glm::vec3 lookingAt = glm::rotateX(lookingAtConst, xAngle);
	lookingAt = glm::rotateY(lookingAt, cameraAngle.y);


	cameraTarget = cameraPos - lookingAt;


	///<Summary>
	///3. The third step calculates the camera up vector. Although 0,1,0 would work in this
	///simple case, it assures that the camera stays upright whatever it is looking at
	///</Summary>

	//Get the vector the camera is looking down
	cameraDirection = glm::normalize(cameraTarget - cameraPos);

	//work out what camera right is
	cameraRight = glm::normalize(glm::cross(up, cameraDirection));

	//work out what  camera up is
	cameraUp = glm::cross(cameraDirection, cameraRight);


	///<Summary>
	///4. Calculate the camera matrix and mulitply it by the perspective matrix
	///</Summary>
	cameraNoPersp = glm::lookAt(cameraPos, cameraTarget, cameraUp);

	camera = persp * cameraNoPersp;

	reportPosition();
}

glm::mat4 EditorCamera::getLookAtMatrix(bool invertY, glm::vec3 pos)
{
	if (invertY)
	{
		//Get the vector the camera is looking down
		cameraDirection = glm::normalize(cameraTarget - pos);

		//work out what camera right is
		cameraRight = glm::normalize(glm::cross(up, cameraDirection));

		//work out what  camera up is
		-cameraUp = glm::cross(cameraDirection, cameraRight);


		float xAngle = (-cameraAngle.x) + (M_PI / 2);


		glm::vec3 lookingAt = glm::rotateX(lookingAtConst, xAngle);

		lookingAt = glm::rotateY(lookingAt, cameraAngle.y);

		cameraTarget = pos - lookingAt;



		return persp * glm::lookAt(pos, cameraTarget, cameraUp);
	}
	else
	{
		std::cout << ("THIS DOES NOT WORK YETz\n");
	}

}

void EditorCamera::OnClick(int key, float x, float y)
{
	if (key == INPUT_RIGHT_MOUSE_DOWN)
	{
		rightClickDown = true;
	}
	else if ( key == INPUT_RIGHT_MOUSE_UP)
	{
		rightClickDown = false;
	}
	else if (key == INPUT_MIDDLE_MOUSE_DOWN)
	{
		middleMouseDown = true;
	}
	else if (key == INPUT_MIDDLE_MOUSE_UP)
	{
		middleMouseDown = false;
	}
}

void EditorCamera::scrollWheel(double x, double y)
{
	float zoom = y * zoomSens;

	glm::vec3 direction = getRotMat() * glm::vec4(0.0f, 0.0f, 1.0f, 0.0f) * zoom;

	glm::vec3 currPos = target->getPos();

	currPos += direction;

	target->setPos(currPos);
}

glm::mat4 EditorCamera::getRotMat()
{
	glm::mat4 rotMat = glm::mat4(1);

	rotMat = glm::rotate(rotMat, cameraAngle.y, glm::vec3(0.0f, 1.0f, 0.0f));
	rotMat = glm::rotate(rotMat, cameraAngle.x, glm::vec3(1.0f, 0.0f, 0.0f));

	return rotMat;
}