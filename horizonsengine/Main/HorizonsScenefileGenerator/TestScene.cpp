#include "TestScene.h"
#include <Game.h>
#include <Scenery.h>
#include <glm-0.9.9.2/glm/glm.hpp>
#include <AssimpContainer.h>
#include <MoonLight.h>
#include <Skybox.h>
#include "Plane.h"
#include "EditorCamera.h"
#include <ModelManager.h>
#include "PlaceObjectOnClick.h"

TestScene::TestScene()
{
}


TestScene::~TestScene()
{
}

void TestScene::run()
{
	gameRefr->setCaptureMouse(false);
}
void TestScene::load(Game * refr)
{
	gameRefr = refr;
	sceneInit();

	loadCamera();

	loadGUI();

	loadPlane();

	currLighting->addLight(new Daylight(glm::vec3(1.0,1.0,1.0)));

	gameRefr->setConfigAll();

	Model* cube = ModelManager::getModel("Objects/cube.obj");

	addPublicBehaviour(new PlaceObjectOnClick(cube));

	this;
}

void TestScene::loadGUI()
{

}
void TestScene::sceneInit()
{
	Scene::loadInit();

	Skybox* sky = new Skybox(1);

	addGameObject(sky);
}

void TestScene::loadPlane()
{
	Plane* p = new Plane(glm::vec2(100));

	addGameObject(p);
}

void TestScene::loadCamera()
{
	GameObject* editorCamLocGO = new Object3D();

	editorCamLocGO->setPos(glm::vec3(0.0f));

	editorCamLocGO->removeComponent<RenderConditions>();

	EditorCamera * cam = new EditorCamera(editorCamLocGO);

	gameCamera = cam;

	gameRefr->setCamera(cam);
}

