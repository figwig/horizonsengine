#pragma once
#include <Camera.h>
#include <InputListener.h>

class EditorCamera : public Camera, public InputListener
{
public:
	EditorCamera(GameObject* go);
	~EditorCamera();

	void update(double deltaTimed, int, int, bool playing);
	glm::mat4 getLookAtMatrix(bool invertY, glm::vec3 pos);

	void OnClick(int key, float x, float y);
	void scrollWheel(double x, double y);

protected:
	float y = 0.0f;

	glm::vec3 lookingAtConst = glm::vec3(0.0f, -5.0f, 0.0f);
	glm::vec3 targetAdjust = glm::vec3(0, 3, 0);

	glm::vec4 positionOffset = glm::vec4(0, 4, -4, 0);

	float mouseSens = 500.0f;
	float maxX = 0.6f;

	float minX = -0.6f;

	float radius = 5.0f;

	float prevCameraY = 0.0f;

	bool rightClickDown = false;
	bool middleMouseDown = false;

	void lookAround(int mouseX, int mouseY);
	void moveAround(int mouseX, int mouseY);

	void calcMatrix();

	float moveSens = 10.0f;
	float zoomSens = 1.0f;

	glm::mat4 getRotMat();
};

