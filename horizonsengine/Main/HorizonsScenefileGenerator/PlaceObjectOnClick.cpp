#include "PlaceObjectOnClick.h"
#include <Scenery.h>
#include <ForceOnKey.h>
#include <ObjectFactory.h>
#include <Scene.h>

void PlaceObjectOnClick::OnClick(int key, float , float)
{
	glm::vec3 worldPos = mrc->getMousePosWorld();

	if (key != INPUT_LEFT_MOUSE_DOWN && key != INPUT_RIGHT_MOUSE_DOWN) return;

	if (key == INPUT_LEFT_MOUSE_DOWN) {
		cube = new Custom(
			model,
			worldPos,
			glm::vec3(0.2f),
			glm::vec3(0));
	}
	else
	{
		cube = new Custom(
			model,
			worldPos,
			glm::vec3(0.05f),
			glm::vec3(0));
	}
	cube->e = model->getExtents(0);

	float halfHeight = cube->getScaledExtents().maxExtent.y - cube->getScaledExtents().centre.y;

	cube->setPos(cube->getPos() + glm::vec3(0, halfHeight, 0));

	cube->addPrivateBehaviour(new ForceOnKey(Keys::x, glm::vec3(0, 800, 0)));

	if (physics) {
		cube->setCollisionShape(ObjectFactory::getInstance()->makeBoxCollider(cube));

		cube->addComponent(new PhysicsComponent(cube, 50.0f));
	}
	cube->name = "Generated Cube";

	scene->addGameObject(cube);
}