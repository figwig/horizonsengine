#include "Plane.h"
#include <ModelManager.h>
#include <MaterialHandler.h>
#include <ShaderManager.h>
#include <ObjectFactory.h>
#include <PhysicsComponent.h>
#include <DefaultShaders.h>
#include <RenderConditions.h>

Plane::Plane(glm::vec2 scale)
{
	setScale(glm::vec3(scale.x, 1.0f, scale.y));

	RenderConditions * rc =getComponent<RenderConditions>();
	
	rc->setModel(ModelManager::getModel("Plane"));

	rc->setMaterial(MaterialHandler::singleton()->makeSingleMaterial(TextureExaminer::singleton()->getTexture("Textures/brickwall.jpg")));

	rc->setShader( DefaultShaders::getInstance());

	e = rc->getModel()->getExtents(0);

	addComponent(new PhysicsComponent(this, 0.0f));

	setCollisionShape(ObjectFactory::getInstance()->makePlaneCollider(getScaledExtents()));
}

Plane::~Plane()
{

}