#include <EngineWrapper.h>
#include "TestScene.h"
int main(int argc, char* argv[]) 
{
	EngineWrapper * horizonsInstance = new EngineWrapper();


	horizonsInstance->initGL(argc, argv, "Horizons Development Scenes");

	horizonsInstance->sortConfig();

	Scene * levelOne = new TestScene();

	horizonsInstance->initializeGame(levelOne);


	horizonsInstance->startGame();

	return 0;
}