#pragma once
#include <GameObject.h>
#include <Game.h>
#include <InputListener.h>

class SceneEditor : public GlobalBehaviour, public InputListener
{
public:
	SceneEditor();
	~SceneEditor();

private:
	//Opening and closing functionality
	bool open = false;
	double inputTimer = 0.0;


};

class DragCamera : public GlobalBehaviour
{
public:
	DragCamera(Camera* c) : cam(c) {}
	~DragCamera();

private:
	Camera* cam;
};

