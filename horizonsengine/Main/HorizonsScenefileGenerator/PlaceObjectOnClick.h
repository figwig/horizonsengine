#pragma once
#include <PublicBehaviour.h>
#include <MouseRaycaster.h>

class Custom;
class Scene;

class PlaceObjectOnClick : public PublicBehaviour, public InputListener
{
public:
	PlaceObjectOnClick(Model * model, bool physics = true) : model(model), physics(physics) {
		mrc = new MouseRaycaster();
	}

	~PlaceObjectOnClick() { delete mrc; }

	void update(double)
	{

	}
	void OnClick(int key, float x, float y);
private:
	MouseRaycaster* mrc = nullptr;

	Model* model = nullptr;
	Custom* cube = nullptr;
	bool physics = false;
};

