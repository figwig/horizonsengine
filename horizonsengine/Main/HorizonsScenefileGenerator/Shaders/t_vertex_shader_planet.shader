#version 330

uniform mat4 T; // Translation matrix
uniform mat4 S; // Scale matrix
uniform mat4 R; // Rotation matrix
uniform vec4 posRelParent; // the position relative to the parent
uniform vec4 clipPlane; //The clipping Plane
uniform int usePlane; //PLane bool




// Input vertex packet
layout(location = 0) in vec4 position;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec4 tangent;
layout(location = 4) in vec4 bitangent;
layout(location = 8) in vec2 textureCoord;


// Output vertex packet
out packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;
	vec3 tangent;
	vec3 bitangent;

} outputVertex;

mat4 transform;
mat3 TBN;
vec4 pos;
mat3 normalMat;

void calculateTBN()
{
	vec4 tan = tangent / tangent.w;
	vec3 T = normalize(vec3(transform * tan));
	//vec3 T = normalize(vec3(normalMat * tangent.xyz));

	vec4 bitan = bitangent / bitangent.w;
	vec3 B = normalize(vec3(transform * bitan));
	//vec3 B = normalize(vec3(normalMat * bitangent.xyz));

	//vec3 N = normalize(cross(T, B));

	vec3 N = normalize(vec3(normalMat * vec4(normal, 0.0).xyz));

	TBN = mat3(T, B, N);

	outputVertex.TBN = TBN;

	outputVertex.tangent = T;
	outputVertex.bitangent = B;
}

void main(void) {
	//Calculate transform matrix
	transform = T * R * S;

	//Calculate pos
	pos = vec4(position.x, position.y, position.z, 1.0) + posRelParent;

	//Pass through the texture coords
	outputVertex.textureCoord = textureCoord;

	//Transform matrix dealt with
	pos = transform * pos;

	//Output vertex pos
	outputVertex.vert = pos.xyz;

	//Work out the normal for lighting
	normalMat = transpose(inverse(mat3(transform)));
	outputVertex.normal = normalize(normalMat * normal);

	calculateTBN();

	gl_Position = pos;
}