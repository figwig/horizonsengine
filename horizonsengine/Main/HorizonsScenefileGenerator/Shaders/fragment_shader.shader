#version 450 core
#extension GL_ARB_bindless_texture : require

struct Material
{
	sampler2D diff;
	sampler2D spec;
	sampler2D norm;

};


layout(std140, binding = 2) uniform Materials
{
	Material materials[64];
};



uniform int useNormalMap;
uniform int invertY;

//Uniforms that have to be unique to each surface

#define MAX_LIGHTS 20

uniform float materialShininess;
uniform vec3 cameraPos_World;
uniform int numLights;

uniform struct Light {
   vec4 position;
   vec3 colors;
   float attenuation;
   float ambientCoefficient;
   float coneAngle;
   vec3 coneDirection;
   mat4 finalLightMatrix;
   sampler2D shadowMap;
   bool castShadows;
   mat4 shadowMatrix;
} allLights[MAX_LIGHTS];

in packet{
	
	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;
	flat int materialID;
} inputFragment;

float distanceToCam;

//Shadow Consts
const int pcfCount = 6;
const float totalTexels = (pcfCount * 2 + 1) * (pcfCount * 2 + 1);
uniform float mapSize;
uniform float shadowDistance;

float fadeDist;
float texelSize;


mat3 calcedTBN;
vec3 calcedNormal;

// output packet
layout (location = 0) out vec4 fragmentColour;

vec3 applyLight(Light thisLight, vec3 baseColor, vec3 surfacePos, vec3 surfaceToCamera)
{
	float attenuation = 1.0f;
	vec3 lightPos = (thisLight.finalLightMatrix*thisLight.position).xyz;
	vec3 surfaceToLight;

	vec3 coneDir = normalize(thisLight.coneDirection);

	if (thisLight.position.w == 0.0f)
	{
		//Directional Light (all rays same angle, use position as direction)
		surfaceToLight = normalize((thisLight.position).xyz);
		attenuation = 1.0f;
	}
	else
	{
		//Point light
		surfaceToLight =  normalize(lightPos - (surfacePos));


		float distanceToLight = length(lightPos -( surfacePos));
		attenuation = 1.0 / (1.0f + thisLight.attenuation * pow(distanceToLight, 2));

		//Work out the Cone restrictions
		float lightToSurfaceAngle = degrees(acos(dot(-surfaceToLight, normalize(coneDir))));
		
		if (lightToSurfaceAngle > thisLight.coneAngle)
		{
			attenuation = 0.0;
		}
	}
	if (thisLight.castShadows)
	{
		vec4 shadowCoords = thisLight.shadowMatrix * vec4(inputFragment.vert, 1.0f); //

		if (shadowCoords.x > 0.0f && shadowCoords.x < 1.0f && shadowCoords.y >0.0f && shadowCoords.y < 1.0f)
		{

			float bias = max(0.01 * (1.0 - dot(calcedNormal, surfaceToLight)), 0.005);
			//bias = 0.005f;

			float total = 0.0f;

			float distance = distanceToCam - ((shadowDistance* 1.3f) - fadeDist);
			distance /= fadeDist;

			shadowCoords.w = clamp(1.0 - distance, 0.0, 1.0);

			for (int x = -pcfCount; x <= pcfCount; x++)
			{
				for (int y = -pcfCount; y <= pcfCount; y++)
				{
					float objectNearestLight = texture2D(thisLight.shadowMap, shadowCoords.xy + vec2(x, y)* texelSize).r;
					if (shadowCoords.z - bias > objectNearestLight)
					{
						total++;
					}
				}
			}

			total /= totalTexels;

			attenuation -= (total*shadowCoords.w);
			attenuation = clamp(attenuation, 0.05, 1.0);
		}
	}
	//Ambient light calculation
	vec3 ambient = thisLight.ambientCoefficient * baseColor.rgb * thisLight.colors;

	//Diffuse Light Calculation
	float diffuseCoefficient = max(0.0, dot(calcedNormal,normalize(surfaceToLight)));
	vec3 diffuse = diffuseCoefficient * baseColor.rgb * thisLight.colors;

	//Specular Light Calculation
	float specularCoefficient = 0.0;
	if (diffuseCoefficient > 0.0)
	{
		specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight,calcedNormal))), materialShininess);
	}

	vec3 specular = specularCoefficient * texture2D(materials[(inputFragment.materialID)].spec, inputFragment.textureCoord).xyz * thisLight.colors;

	return ambient + attenuation * (diffuse+ specular); 
}

vec3 calculateNormal()
{
	//Sort the input so that the normal is between 1 and minus 1 instead of 0 and 1
	vec3 nInput = (texture2D(materials[(inputFragment.materialID)].norm, inputFragment.textureCoord)).xyz;

	vec3 readIn = normalize(nInput);

	readIn.x =  (2.0* nInput.r) - 1.0;
	readIn.y = invertY *((2.0* nInput.g) - 1.0);
	readIn.z =  (2.0*nInput.b-1.0) ;

	readIn = inputFragment.TBN *readIn;

	//Before we use this normal, it needs to be translated according to which way the surface is facing
	//We can do that using a TBN Matrix. This was calculated in the vertex shader
	return readIn;
}


void main(void) {
	texelSize = 1.0f / mapSize;

	fadeDist = shadowDistance * 0.12f;
	distanceToCam = length(inputFragment.vert - cameraPos_World);


	//get the base colour from the texture
	vec4 tempFragColor = texture(sampler2D(materials[(inputFragment.materialID)].diff), inputFragment.textureCoord).rgba;

	//Support for objects with and without a normal map
	if (useNormalMap == 1)
	{  
		calcedTBN = inputFragment.TBN;
		calcedNormal = calculateNormal();
	}
	else
	{
		calcedNormal = inputFragment.normal;
		calcedTBN = mat3(1);
	}

	vec3 surfaceToCameraWorld = normalize((cameraPos_World) - (inputFragment.vert));

	vec3 tempColour = vec3(0.0, 0.0, 0.0);

	for (int count = 0; count < numLights; count++)
	{
		tempColour += applyLight(allLights[count], tempFragColor.xyz, inputFragment.vert, surfaceToCameraWorld);
	}


	vec3 gamma = vec3(1.0 / 2.2);

	vec4 final = vec4(pow(tempColour, gamma), tempFragColor.a);

	fragmentColour = final;
	//fragmentColour = vec4(calcedNormal, 1);
	//fragmentColour = texture(sampler2D(textureImage), inputFragment.textureCoord).rgba;
}


