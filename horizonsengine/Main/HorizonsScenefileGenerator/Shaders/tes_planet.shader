#version 430 core

//layout(triangles, equal_spacing, ccw) in;
layout(triangles, fractional_even_spacing, ccw) in;


in packet{

	vec2 textureCoord;
	
	vec3 normal;
	vec3 vert;
	mat3 TBN;
	vec3 cameraPos;
	vec3 tangent;
	vec3 bitangent;

} inputVertex[];

out packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;
	vec3 cameraPos;
	vec3 debugTexture;

} outputVertex;


uniform mat4 camera; // camera matrix
uniform float radius;

vec3 vecToOrigin;
vec4 position;
//For normal sampling
float delta = 0.000001f;
vec3 modelXOffset;
vec3 modelYOffset;

#define M_PI 3.14159265358979323846

float rand(vec2 co) { return fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453); }
float rand(vec2 co, float l) { return rand(vec2(rand(co), l)); }
float rand(vec2 co, float l, float t) { return rand(vec2(rand(co, l), t)); }

float perlin(vec2 p, float dim, float time) {
	vec2 pos = floor(p * dim);
	vec2 posx = pos + vec2(1.0, 0.0);
	vec2 posy = pos + vec2(0.0, 1.0);
	vec2 posxy = pos + vec2(1.0);

	float c = rand(pos, dim, time);
	float cx = rand(posx, dim, time);
	float cy = rand(posy, dim, time);
	float cxy = rand(posxy, dim, time);

	vec2 d = fract(p * dim);
	d = -0.5 * cos(d * M_PI) + 0.5;

	float ccx = mix(c, cx, d.x);
	float cycxy = mix(cy, cxy, d.x);
	float center = mix(ccx, cycxy, d.y);

	return center * 2.0 - 1.0;
}

// p must be normalized!
float perlin(vec2 p, float dim) {

	/*vec2 pos = floor(p * dim);
	vec2 posx = pos + vec2(1.0, 0.0);
	vec2 posy = pos + vec2(0.0, 1.0);
	vec2 posxy = pos + vec2(1.0);

	// For exclusively black/white noise
	/*float c = step(rand(pos, dim), 0.5);
	float cx = step(rand(posx, dim), 0.5);
	float cy = step(rand(posy, dim), 0.5);
	float cxy = step(rand(posxy, dim), 0.5);*/

	/*float c = rand(pos, dim);
	float cx = rand(posx, dim);
	float cy = rand(posy, dim);
	float cxy = rand(posxy, dim);

	vec2 d = fract(p * dim);
	d = -0.5 * cos(d * M_PI) + 0.5;

	float ccx = mix(c, cx, d.x);
	float cycxy = mix(cy, cxy, d.x);
	float center = mix(ccx, cycxy, d.y);

	return center * 2.0 - 1.0;*/
	return perlin(p, dim, 0.0);

}

vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2)
{
	return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2)
{
	return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;
}

mat3 interpolateMat3(mat3 m0, mat3 m1, mat3 m2)
{
	return  m0 * gl_TessCoord.x + m1 * gl_TessCoord.y + m2 * gl_TessCoord.z;
}



float getNoiseAtPoint(vec2 tex)
{
	vec2 texCoord = vec2(0);
	texCoord.y = tex.y;

	//Angle of latitude in radians
	float angle = texCoord.x * M_PI;

	float circumfrence = (radius * 2 * M_PI);

	float lengthOfLongitudeLine = 360 * cos(angle) * circumfrence;

	//I think this is the right way round, will become more obvious as the terrain is more defined
	float portion = lengthOfLongitudeLine / circumfrence;

	texCoord.x = portion * tex.x;


	float baseHeight = perlin(texCoord, 200) ;

	baseHeight = pow(baseHeight, 1.0);

	float p = baseHeight * 10.0f;

	return p;
}

void calculateNormal()
{
	vec2 pointCoord = outputVertex.textureCoord;

	//Use the original TBN matrix to calculate this properly
	float heightL = getNoiseAtPoint(pointCoord + vec2(-delta, 0));
	float heightR = getNoiseAtPoint(pointCoord + vec2(delta, 0));
	float heightU = getNoiseAtPoint(pointCoord + vec2( 0, delta));
	float heightD = getNoiseAtPoint(pointCoord + vec2( 0, -delta));

	vec3 tangentNormal = normalize(vec3(heightL - heightR,  heightU - heightD,2.0f ));

	//vec3 tangentNormal = normalize(vec3(2*(heightL - heightR),  4.0f ,(2*( heightU - heightD))) * 0.25f);
	//tangentNormal.x = ((2.0 * tangentNormal.x) - 1.0);
	//tangentNormal.y = ((2.0 * tangentNormal.y) - 1.0);
	//tangentNormal.z = ((2.0 * tangentNormal.z) - 1.0);

	vec3 T = normalize(interpolate3D(inputVertex[0].tangent, inputVertex[1].tangent, inputVertex[2].tangent));
	vec3 B = normalize(interpolate3D(inputVertex[0].bitangent, inputVertex[1].bitangent, inputVertex[2].bitangent));

	mat3 TBN = mat3(T,  B, normalize(vecToOrigin));

	vec3 worldNormal = normalize( TBN * tangentNormal);

	outputVertex.normal = worldNormal;

	vec3 debugColour;

	debugColour.x = ((2.0 * worldNormal.x) - 1.0);
	debugColour.y = ((2.0 * worldNormal.y) - 1.0);
	debugColour.z = ((2.0 * worldNormal.z) - 1.0);

	outputVertex.debugTexture = tangentNormal;
}


void InterpolateAndPass()
{
	outputVertex.textureCoord = interpolate2D(inputVertex[0].textureCoord, inputVertex[1].textureCoord, inputVertex[2].textureCoord);
	vecToOrigin = normalize(interpolate3D(inputVertex[0].normal, inputVertex[1].normal, inputVertex[2].normal));
	outputVertex.vert = interpolate3D(inputVertex[0].vert, inputVertex[1].vert, inputVertex[3].vert);
	outputVertex.TBN = inputVertex[0].TBN;
	outputVertex.cameraPos = interpolate3D(inputVertex[0].cameraPos, inputVertex[1].cameraPos, inputVertex[2].cameraPos);
	outputVertex.vert = vec3(camera * vec4(interpolate3D(inputVertex[0].vert, inputVertex[1].vert, inputVertex[3].vert), 1));
}

void main(void) 
{
	//Pass Through
	InterpolateAndPass();

	//Interpolate the position
	position = (gl_TessCoord.x * gl_in[0].gl_Position + gl_TessCoord.y * gl_in[1].gl_Position + gl_TessCoord.z * gl_in[2].gl_Position);

	//Grab some perlin goodness
	float Displacement = getNoiseAtPoint(outputVertex.textureCoord);

	//Add on addtional height
	vec3 addition = vecToOrigin * Displacement;

	//apply this to point
	position.x += addition.x;
	position.y += addition.y;
	position.z += addition.z;

	//Calculate the normal 
	calculateNormal();

	position.w = 1.0f;

	//and calculate the world position
	gl_Position =  camera * position;
}

//Old normal calcs
	//float ogHeight = getNoiseAtPoint(outputVertex.textureCoord);
	//vec2 deltaXTexC = outputVertex.textureCoord + vec2(delta, 0.0);
	//vec2 deltaYTexC = outputVertex.textureCoord + vec2(0.0, delta);

	//vec3 ogMapPos;
	//ogMapPos.x = outputVertex.textureCoord.x;
	//ogMapPos.y = ogHeight;
	//ogMapPos.z = outputVertex.textureCoord.y;

	//vec3 deltaXPos = vec3(deltaXTexC.x, getNoiseAtPoint(deltaXTexC), deltaXTexC.y);
	//vec3 deltaYPos = vec3(deltaYTexC.x, getNoiseAtPoint(deltaYTexC), deltaYTexC.y);

	//vec3 XGrad = deltaXPos - ogMapPos;
	//vec3 YGrad = deltaYPos - ogMapPos;

	//vec3 tangentNormal = normalize(cross(XGrad, YGrad));