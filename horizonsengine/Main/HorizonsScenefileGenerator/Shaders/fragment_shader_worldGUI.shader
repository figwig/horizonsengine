#version 330

uniform sampler2D textureImage;



//Uniforms that have to be unique to each surface

in packet{

	vec2 textureCoord;


} inputFragment;

// output packet
layout (location = 0) out vec4 fragmentColour;



void main(void) {
	//get the base colour from the texture
	vec4 tempColour = texture2D(textureImage, inputFragment.textureCoord).rgba;

	vec3 gamma = vec3(1.0 / 2.2);

	vec4 final = vec4(pow(tempColour.xyz, gamma), tempColour.a);

	fragmentColour = final;
}


